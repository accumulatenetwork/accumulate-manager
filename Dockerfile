FROM mcr.microsoft.com/dotnet/sdk:7.0-jammy-amd64 AS builder

COPY . /AccumulateManager
WORKDIR /AccumulateManager

RUN mkdir -p /app
#RUN dotnet build AccumulateManagerCommon --configuration Release -p:GitHash=$(git rev-parse HEAD) -p:GitBranch=$(git rev-parse --abbrev-ref HEAD) -p:GitDateTime=$(git show -s --date='unix' --format=%cd HEAD) -p:GitTag=$(git describe --tags | sed 's/-g.*//') --output /app
RUN dotnet publish AccumulateManager.sln --configuration Release \
      -p:GitHash=$(git rev-parse HEAD) \
      -p:GitBranch=$(git rev-parse --abbrev-ref HEAD)  \
      -p:GitDateTime=$(git show -s --date='unix' --format=%cd HEAD)  \
      -p:GitTag=$(git describe --tags | sed 's/-g.*//') \
      -p:PublishDir=/app


FROM mcr.microsoft.com/dotnet/aspnet:7.0-jammy-amd64 AS run

COPY --from=builder /app /app

# Set the timezone.
ENV TZ=UTC
RUN ln -fs /usr/share/zoneinfo/UTC /etc/localtime

RUN apt update \
	&& apt -y install joe less ssh wget curl mtr-tiny bash

RUN apt clean
RUN rm -rf /var/lib/apt/lists/*
    
ENV AccumulateManager_DATA = "/data"
#ENV AccumulateManager_WWW = "/www"
ENV HOME = /data
 
EXPOSE 16666
WORKDIR /app
ENTRYPOINT ["/usr/bin/dotnet","AccumulateManager.dll"]
