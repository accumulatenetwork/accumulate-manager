using System.IO.Pipelines;
using System.Security.Cryptography;
using System.Text;
using AccumulateManager.TelnetConsole;
using ConsoleServer;
using ConsoleServerDocker;
using Docker.DotNet;
using Docker.DotNet.Models;
using ICSharpCode.SharpZipLib.Tar;
using Serilog;
using Spectre.Console;
using FileMode = System.IO.FileMode;

namespace AccumulateManager;

public class clsDockerAccumulate
{
    private AnsiTelnetConsole? ANSIConsole;
    public static readonly string _backupFile = "KeyBackup.tar";
    public static readonly string[] _backupFiles = new string[] { "priv_validator_key.json" , "register.json" };


    public clsDockerAccumulate(AnsiTelnetConsole? ansiConsole = null)
    {
        ANSIConsole = ansiConsole;
    }

    public async Task<bool> RunInit(AccNetworkInstalled network, CancellationToken token = default)
    {

        string backupFileInfoText = ReadBackupKeyInfo();
        var backupFile = Path.Combine(Program.DataPath,_backupFile);
        var backupExists = !String.IsNullOrEmpty(backupFileInfoText);
        if (backupExists)
        {
            if (!backupFileInfoText.Contains(network.NetworkName) || !backupFileInfoText.Contains(network.Subnet))
            {
                ANSIConsole.MarkupLine("[red]WARNING YOUR BACKUP DOES NOT MATCH YOU NETWORK SELECTION[/]");
                if (!YesNo("Continue without restoring backup?"))
                {
                    return false;
                }
                backupExists = false;
            }
        }

        clsDockerAttachToContainer container = null;
        try
        {
            CreateContainerResponse image;
            try
            {
                var entryPoint = new string[] { "/bin/bash" };
                image = new clsDockerImageAccumulate(ANSIConsole).Create(entryPoint, network, true);
                container = new clsDockerAttachToContainer(true,ANSIConsole);
                container.AttachToContainer(image.ID);
            }
            catch (DockerImageNotFoundException)
            {
                ANSIConsole?.WriteLine("Docker Image cant be Found");
                Log.Error("{name} Run Docker Image cant be Found", nameof(clsDockerAccumulate));
                return false;
            }
            catch (DockerApiException ex)
            {
                ANSIConsole?.WriteLine(ex.Message, "red");
                Log.Error("{name} RunInit {message} ", nameof(clsDockerAccumulate), ex.Message);
                return false;
            }
            catch (Exception ex)
            {
                ANSIConsole?.WriteException(ex);
                return false;
            }


            try
            {
                container.WriteCommand("").Wait();
                var consoleTask = container.AttachConsoleOutput(true, 10000);
                container.WriteCommand("wget https://gitlab.com/accumulatenetwork/accumulate-docker/-/raw/main/scripts/register-validator.sh").Wait();

                var matchSaved = new clsMatchItem("aved");
                container.WaitForMatch(TimeSpan.FromMilliseconds(8000), matchSaved);
                var sb = new StringBuilder();
                sb.Append("/bin/accumulated init dual ");
                sb.Append($" tcp://{network.Subnet.ToLower()}.{network.BaseURL}:16691");
                sb.Append($" -w /node");
                sb.Append($" --public {Program.PublicIPV4}");
                sb.Append($" && bash register-validator.sh -d /node/dnn -b /node/bvnn");
                container.WriteCommand(sb.ToString());
                container.WriteCommand("A='script';B='end';echo $A-$B");
                ANSIConsole.MarkupLine($"[yellow]Please wait.....[/]");
                var matchScriptEnd = new clsMatchItem("script-end");
                container.WaitForMatch(TimeSpan.FromMilliseconds(8000), matchScriptEnd);

                if (ANSIConsole != null)
                {
                    if (backupExists)
                    {
                        var rule = new Rule($"[white]Backup Found[/]");
                        rule.RuleStyle("green");
                        rule.LeftAligned();
                        ANSIConsole.Write(rule);

                        ANSIConsole.MarkupLine($"[yellow]{backupFileInfoText}[/]");

                        if (YesNo("Do you want to restore node Key?"))
                        {
                            for (int i = 1; i < 4; i++)
                            {
                                var textprompt = new TextPrompt<string>($"[green]{i} Enter Password[/]?").AllowEmpty();
                                textprompt.IsSecret = true;
                                var password = ANSIConsole.Prompt(textprompt);
                                if (String.IsNullOrEmpty(password)) continue;

                                if (clsDockerAccumulate.RestoreKey(image.ID, password.StripBraketedPaste()))
                                {
                                    ANSIConsole.MarkupLine("[green]Restore Complete[/]");
                                    break;
                                }
                                else
                                {
                                    ANSIConsole.MarkupLine("[red]Restore failed[/]");
                                }
                            }
                        }
                    }
                }

                container.WriteCommand("exit");
                container.WaitForExitSilence(10,20);

                var vol = await Program.DockerManager.GetVolumeResponse(network.DockerName);

                if (vol is not null)
                {
                    ANSIConsole?.MarkupLine($"Volume created: [green]{vol.CreatedAt}[/]");

                    //todo remove invalid URLs
                  //  var fullURL = $"{network.Subnet.ToLower()}.{network.BaseURL.ToLower()}";
                   // if (!Program.Settings.DomainNames.Contains(fullURL)) Program.Settings.DomainNames.Add(fullURL);
                   // if (!Program.Settings.DomainNames.Contains(network.BaseURL)) Program.Settings.DomainNames.Add(network.BaseURL);

                    return true;
                }
                else
                {
                    ANSIConsole?.MarkupLine($"[red]Volume not found: {vol.Name}[/]");
                    return false;
                }

            }
            catch (DockerApiException ex)
            {
                ANSIConsole?.WriteLine(ex.Message, "red");
                Log.Error("{name} RunInit {message} ", nameof(clsDockerAccumulate), ex.Message);
            }
            catch (Exception ex)
            {
                ANSIConsole?.WriteException(ex);
            }
        }
        catch (Exception ex)
        {
            ANSIConsole?.WriteException(ex);
        }
        finally
        {
            container?.Dispose();
        }

        return false;
    }


    public async Task<bool> Run(AccNetworkInstalled network, string[] entryPoint = null, CancellationToken token = default)
    {
        clsDockerAttachToContainer container = null;
        try
        {
            CreateContainerResponse image;
            try
            {
                if (entryPoint==null) {
                    entryPoint = new string[] { "/bin/accumulated","run-dual","/node/dnn","/node/bvnn","--truncate"};
                    if (Program.Settings.ExtraArguments?.Count > 0) {
                        entryPoint = entryPoint.Concat(Program.Settings.ExtraArguments).ToArray();
                    }
                }
                image = new clsDockerImageAccumulate(ANSIConsole).Create(entryPoint, network, false);
                container = new clsDockerAttachToContainer(true,ANSIConsole);
                container.AttachToContainer(image.ID);
            }
            catch (DockerImageNotFoundException)
            {
                ANSIConsole?.WriteLine("Docker Image cant be Found");
                Log.Error("{name} Run Docker Image cant be Found", nameof(clsDockerAccumulate));
                return false;
            }
            catch (DockerApiException ex)
            {
                ANSIConsole?.WriteLine(ex.Message, "red");
                Log.Error("{name} Run {message} ", nameof(clsDockerAccumulate), ex.Message);
                return false;
            }
            catch (Exception ex)
            {
                ANSIConsole?.WriteException(ex);
                return false;
            }

            try
            {
                var consoleTask = container.AttachConsoleOutput(true, 10000);  //Blocks until output (or timeout)
                container.WaitForExitSilence(5,8);

            }
            catch (DockerApiException ex)
            {
                ANSIConsole?.WriteLine(ex.Message, "red");
                Log.Error("{name} RunInit {message} ", nameof(clsDockerAccumulate), ex.Message);
            }
            catch (Exception ex)
            {
                ANSIConsole?.WriteException(ex);
            }
        }
        catch (Exception ex)
        {
            ANSIConsole?.WriteException(ex);
        }
        finally
        {
            container?.Dispose();
        }
        return false;
    }

    public async Task<bool> RunHttp(AccNetworkInstalled network, string[] entryPoint = null, CancellationToken token = default)
    {
        clsDockerAttachToContainer container = null;
        try
        {
            CreateContainerResponse image;
            try
            {
                if (entryPoint==null) {
                    entryPoint = new string[] { "/bin/accumulated-http",network.NetworkName,
                                                                  $"--key",network.Genisis,
                                                                  "--http-listen","/ip4/0.0.0.0/tcp/16695/http",
                                                                  "--p2p-listen","/ip4/0.0.0.0/tcp/16593",
                                                                  "--p2p-listen","/ip4/0.0.0.0/udp/16593/quic",
                                                                  "--peer-db","/data/peerdb.json",
                    };
                    if (Program.Settings.ExtraArguments?.Count > 0) {
                        entryPoint = entryPoint.Concat(Program.Settings.ExtraArguments).ToArray();
                    }
                }


                image = new clsDockerImageAccumulateHttp(ANSIConsole).Create(entryPoint, network, false);
                container = new clsDockerAttachToContainer(true,ANSIConsole);
                container.AttachToContainer(image.ID);
            }
            catch (DockerImageNotFoundException)
            {
                ANSIConsole?.WriteLine("Docker Image cant be Found");
                Log.Error("{name} Run Docker Image cant be Found", nameof(clsDockerAccumulate));
                return false;
            }
            catch (DockerApiException ex)
            {
                ANSIConsole?.WriteLine(ex.Message, "red");
                Log.Error("{name} Run {message} ", nameof(clsDockerAccumulate), ex.Message);
                return false;
            }
            catch (Exception ex)
            {
                ANSIConsole?.WriteException(ex);
                return false;
            }

            try
            {
                var matchListen = new clsMatchItem("Listening on");
                var matchFail = new clsMatchItem("key is wrong length");
                var consoleTask = container.AttachConsoleOutput(true, 10000,matchListen,matchFail);  //Blocks until output (or timeout)
                container.WaitForMatch(TimeSpan.FromSeconds(5));
                return matchListen.Count > 0 ;
            }
            catch (DockerApiException ex)
            {
                ANSIConsole?.WriteLine(ex.Message, "red");
                Log.Error("{name} RunHttp {message} ", nameof(clsDockerAccumulate), ex.Message);
            }
            catch (Exception ex)
            {
                ANSIConsole?.WriteException(ex);
            }
        }
        catch (Exception ex)
        {
            ANSIConsole?.WriteException(ex);
        }
        finally
        {
            container?.Dispose();
        }
        return true;
    }



    /// <summary>
    /// Start Accumulate container, but with BASH console instead.
    /// </summary>
    /// <param name="network"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    public async Task<CreateContainerResponse> RunBashConsole(AccNetworkInstalled network, string[] commands = null, string imageName = "registry.gitlab.com/accumulatenetwork/accumulate", string? tag = null, CancellationToken token = default)
    {
        clsDockerAttachToContainer container = null;
        CreateContainerResponse image;
        try
        {
            var entryPoint = new string[] { "/bin/bash" };
            image = new clsDockerImageAccumulate(ANSIConsole, imageName, tag).Create(entryPoint, network, true);
            container = new clsDockerAttachToContainer(true,ANSIConsole);
            container.AttachToContainer(image.ID);
            if (commands?.Length > 0)
            {
                foreach (var command in commands)
                {
                    container.WriteCommand(command).Wait();
                }
            }
            else
            {
                container.WriteCommand("").Wait();
            }
            container.AttachConsoleInOut(false).Wait();

        }
        catch (DockerImageNotFoundException)
        {
            ANSIConsole?.WriteLine("Docker Image cant be Found");
            Log.Error("{name} Run Docker Image cant be Found", nameof(clsDockerAccumulate));
            return null;
        }
        catch (DockerApiException ex)
        {
            ANSIConsole?.WriteLine(ex.Message, "red");
            Log.Error("{name} RunInit {message} ", nameof(clsDockerAccumulate), ex.Message);
            return null;
        }
        catch (Exception ex)
        {
            ANSIConsole?.WriteException(ex);
            return null;
        }
        finally
        {
            container?.Dispose();
        }

        return image;
    }

    public static bool BackupKey(string password)
    {
        var sb = new StringBuilder();
        sb.AppendLine($"Network: {clsAccNetworkMap.NetworkInstalled.NetworkName}.{clsAccNetworkMap.NetworkInstalled.Subnet}");
        sb.AppendLine($"Backup: {DateTime.Now:yyyy-MM-dd HH:mm zzz}");

        using (var fileStream = File.Create(Path.Combine(Program.DataPath, _backupFile)))
        {
            using (var tarWrite = new ICSharpCode.SharpZipLib.Tar.TarOutputStream(fileStream))
            {
                using (var rijndael = clsEncypt.InitSymmetric(Rijndael.Create(), password, 256))
                {
                    foreach (var file in _backupFiles)
                    {
                        var response = Program.DockerManager.GetClient().Containers.GetArchiveFromContainerAsync(clsAccNetworkMap.NetworkInstalled.DockerName, new GetArchiveFromContainerParameters()
                        {
                            Path = $"/node/{file}"
                        }, false).Result;

                        using (var tarRead = new ICSharpCode.SharpZipLib.Tar.TarInputStream(response.Stream))
                        {
                            TarEntry tarReadEntry;
                            while ((tarReadEntry = tarRead.GetNextEntry()) != null)
                            {
                                using (var unencryptedFile = new MemoryStream())
                                {
                                    using (var encryptedFile = new MemoryStream())
                                    {
                                        using (var cryptoStream = new CryptoStream(encryptedFile, rijndael.CreateEncryptor(), CryptoStreamMode.Write))
                                        {
                                            //Read tar contents
                                            tarRead.CopyEntryContents(unencryptedFile);
                                            unencryptedFile.Position = 0;
                                            unencryptedFile.CopyTo(cryptoStream); //Copy to decrypt

                                            var testTxt = Encoding.UTF8.GetString(unencryptedFile.ToArray());

                                            cryptoStream.FlushFinalBlock();
                                            tarReadEntry.Size = encryptedFile.Length + 8 + 32; //Add space for Len & hash
                                            tarReadEntry.Name = $"{tarReadEntry.Name}_enc";

                                            byte[] hash;
                                            using (HashAlgorithm algorithm = SHA256.Create())
                                            {
                                                hash = algorithm.ComputeHash(unencryptedFile.ToArray());
                                            }

                                            encryptedFile.Position = 0;
                                            tarWrite.PutNextEntry(tarReadEntry);
                                            tarWrite.Write(BitConverter.GetBytes(unencryptedFile.Length));
                                            tarWrite.Write(hash);
                                            encryptedFile.CopyTo(tarWrite);
                                            tarWrite.CloseEntry();
                                        }
                                    }
                                }
                            }
                        }
                        response.Stream.Dispose();
                    }
                }

                var infoBytes = Encoding.UTF8.GetBytes(sb.ToString());
                var tarEntryInto = TarEntry.CreateTarEntry("backup.info");
                tarEntryInto.ModTime = DateTime.UtcNow;
                tarEntryInto.Size = infoBytes.Length;
                tarWrite.PutNextEntry(tarEntryInto);
                tarWrite.Write(infoBytes);
                tarWrite.CloseEntry();
            }
        }

        return true;
    }

    public static bool RestoreKey(string containerId, string password)
    {
        try
        {
            //Open input tar file
            using (var fileStream = File.Open(Path.Combine(Program.DataPath, _backupFile), FileMode.Open))
            {
                using (var rijndael = clsEncypt.InitSymmetric(Rijndael.Create(), password, 256))
                {
                    //Read the tar stream.
                    using (var tarRead = new ICSharpCode.SharpZipLib.Tar.TarInputStream(fileStream))
                    {
                        //Pipe new tar to container
                        var outputPipe = new Pipe();
                        using (var tarWrite = new ICSharpCode.SharpZipLib.Tar.TarOutputStream(outputPipe.Writer.AsStream()))
                        {
                            //Read each tar entry from input tar
                            TarEntry tarReadEntry;
                            while ((tarReadEntry = tarRead.GetNextEntry()) != null)
                            {
                                if (tarReadEntry.Name.EndsWith("_enc"))  //If filename ends in _enc, it's encrypted
                                {
                                    using (var encryptedData = new MemoryStream())
                                    {
                                        using (var decryptedStream = new CryptoStream(encryptedData, rijndael.CreateDecryptor(), CryptoStreamMode.Read))
                                        {
                                            //Read input tar entry. and pass for decryption
                                            tarRead.CopyEntryContents(encryptedData);
                                            encryptedData.Position = 0; //Reset position, so that if can be read.
                                            //Read unencrypted data len
                                            var origLength = new byte[8];
                                            encryptedData.Read(origLength, 0, origLength.Length);
                                            //Read Hash
                                            byte[] hash = new byte[32];
                                            encryptedData.Read(hash, 0, hash.Length);

                                            using (var decrypt = new MemoryStream())
                                            {
                                                decryptedStream.CopyTo(decrypt);  //Decrypt

                                                tarReadEntry.Size = BitConverter.ToInt64(origLength,0);
                                                tarReadEntry.Name = tarReadEntry.Name.Substring(0, tarReadEntry.Name.Length - 4);  //Remove _enc
                                                tarWrite.PutNextEntry(tarReadEntry);

                                                decrypt.Position = 0;  //Reset read position
                                                decrypt.SetLength(tarReadEntry.Size);  //Remove hash

                                                byte[] testHash = new byte[32];
                                                using (HashAlgorithm algorithm = SHA256.Create())
                                                {
                                                    testHash=algorithm.ComputeHash(decrypt.ToArray());
                                                }
                                                if (!testHash.SequenceEqual(hash)) return false;  //Decrypt failed (probably bad password).
                                                decrypt.CopyTo(tarWrite);
                                                tarWrite.CloseEntry();
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    tarWrite.PutNextEntry(tarReadEntry);
                                    tarRead.CopyEntryContents(tarWrite);
                                    tarWrite.CloseEntry();
                                }
                            }

                            tarWrite.Close();

                            Program.DockerManager.GetClient().Containers.ExtractArchiveToContainerAsync(containerId, new ContainerPathStatParameters()
                            {
                                Path = $"/node"
                            }, outputPipe.Reader.AsStream()).Wait();
                        }
                        outputPipe.Reader.AsStream().Dispose();
                    }
                }
            }

        }catch (Exception ex)
        {
            return false;
        }

        return true;
    }


    public static string ReadBackupKeyInfo()
    {
        try
        {
            var backupFile = Path.Combine(Program.DataPath, _backupFile);
            if (File.Exists(backupFile))
            {
                using (var fileStream = File.Open(backupFile, FileMode.Open))
                {
                    using (var tarRead = new ICSharpCode.SharpZipLib.Tar.TarInputStream(fileStream))
                    {
                        TarEntry tarReadEntry;
                        while ((tarReadEntry = tarRead.GetNextEntry()) != null)
                        {
                            if (tarReadEntry.Name.Contains("backup.info"))
                            {
                                using (var contentStream = new MemoryStream())
                                {
                                    tarRead.CopyEntryContents(contentStream);
                                    return Encoding.UTF8.GetString(contentStream.GetBuffer(), 0, (int)contentStream.Length);
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
        }

        return null;
    }

    public static bool WriteFile(string containerId, string outputPath, ref MemoryStream data)
    {
        try
        {
            //Pipe new tar to container
            var outputPipe = new Pipe();
            using (var tarWrite = new ICSharpCode.SharpZipLib.Tar.TarOutputStream(outputPipe.Writer.AsStream()))
            {
                TarHeader header = new TarHeader();

                var tarEntry = new TarEntry(header);
                tarEntry.Name = Path.GetFileName(outputPath);
                tarEntry.Size = data.Length;

                tarWrite.PutNextEntry(tarEntry);

                data.CopyTo(tarWrite);
                tarWrite.CloseEntry();

                tarWrite.Close();

                Program.DockerManager.GetClient().Containers.ExtractArchiveToContainerAsync(containerId, new ContainerPathStatParameters()
                {
                    Path = outputPath
                }, outputPipe.Reader.AsStream()).Wait();
            }
        }catch (Exception ex)
        {
            return false;
        }

        return true;
    }



    static public async Task<VolumeResponse[]?> GetAccumulateVolumes()
    {
        return await Program.DockerManager.GetVolumes("ccumulate");
    }



    async Task DeleteVolume(string name)
    {
      //  await Program.DockerManager._client.Volumes.RemoveAsync(name,true);
    }

    public bool RunNewNetwork(clsNewNetworkJSON.Node node)
    {


        return true;
    }

    bool YesNo(string prompt = "Select")
    {
        var list = new List<SelectionFunction<bool>>();
        list.Add(new SelectionFunction<bool>("No",() => false));
        list.Add(new SelectionFunction<bool>("Yes",() => true));

        var option = ANSIConsole.Prompt(
            new SelectionPrompt<SelectionFunction<bool>>()
                .Title(prompt)
                .PageSize(5)
                .AddChoices(list));

        return option.MenuAction();
    }

}