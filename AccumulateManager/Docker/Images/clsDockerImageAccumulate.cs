using System.Runtime.CompilerServices;
using ConsoleServer;
using Docker.DotNet;
using Docker.DotNet.Models;
using ConsoleServerDocker;
using Serilog;

namespace AccumulateManager;


public class clsDockerImageAccumulate : IDockerImage
{
    private AnsiTelnetConsole ANSIConsole;

    ImagesCreateParameters AccumulateImage;

    public clsDockerImageAccumulate(AnsiTelnetConsole console, string image = "registry.gitlab.com/accumulatenetwork/accumulate", string? tag = null)
    {
        ANSIConsole = console;
        AccumulateImage = new ImagesCreateParameters
        {
            FromImage = image,
            Tag = tag,
        };
    }

    public CreateContainerResponse Create(string[] entrypoint, AccNetworkInstalled network, bool attachAndExit = false)
    {
        if (AccumulateImage.Tag == null) {
            AccumulateImage.Tag = network.Tag;
        }
        AccumulateImage.FromImage = $"{AccumulateImage.FromImage}:{AccumulateImage.Tag}";

        IProgress<JSONMessage> progress = (ANSIConsole != null) ? new clsProgressOutput(ANSIConsole) : new Progress();

        //Pull Image
        Program.DockerManager.CreateImageAsync(AccumulateImage, new AuthConfig(), progress).Wait();

        //Set container Run Parameters
        var runParams = new CreateContainerParameters
        {
            Name = network.DockerName,
            Env = null,
            Cmd = null,
            ArgsEscaped = false,
            Image = AccumulateImage.FromImage,
            Entrypoint = entrypoint,
            Labels = null,
            AttachStderr = attachAndExit,
            AttachStdin = attachAndExit,
            AttachStdout = true,
            OpenStdin = attachAndExit,
            StdinOnce = false,
            Tty = attachAndExit,
            HostConfig = new HostConfig
            {
                Binds = new List<string>()
                {
                    $"{network.DockerName}:/node",
                },
                Privileged = false,
                AutoRemove = attachAndExit,
                ReadonlyRootfs = false,
                NetworkMode = "host",
                RestartPolicy = new RestartPolicy
                {
                    Name = attachAndExit ? RestartPolicyKind.No : RestartPolicyKind.UnlessStopped ,
                    MaximumRetryCount = 0
                }
            }
        };


        if (ANSIConsole != null)
        {
            runParams.Env = new List<string>() { $"COLUMNS={ANSIConsole.Profile.Width}", $"LINES={ANSIConsole.Profile.Height}" };
        }

        if (!String.IsNullOrEmpty(runParams.Name)) Program.DockerManager.DeleteContainerIfExist(runParams.Name).Wait();

        //Create container
        var result = Program.DockerManager.CreateContainerAsync(runParams).Result;

        Log.Logger.Information("{clsDockerImageNsenter}: {result}",nameof(clsDockerImageAccumulate),result);
        foreach (var warn in result.Warnings)
        {
            Log.Logger.Warning("{image} {warn}",AccumulateImage.Repo,warn);
        }

        return result;
    }
}

