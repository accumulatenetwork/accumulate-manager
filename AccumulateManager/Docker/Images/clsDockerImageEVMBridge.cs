using ConsoleServer;
using Docker.DotNet;
using Docker.DotNet.Models;
using ConsoleServerDocker;
using Serilog;

namespace AccumulateManager;


public class clsDockerImageEVMBridge : IDockerImage
{
    private AnsiTelnetConsole ANSIConsole;
    
    ImagesCreateParameters DockerImage = new ImagesCreateParameters
    {
        FromImage = "registry.gitlab.com/accumulatenetwork/evm-bridge:main",
    };

    public clsDockerImageEVMBridge(AnsiTelnetConsole console)
    {
        ANSIConsole = console;
    }
   
    public CreateContainerResponse Create(string[] entrypoint, bool attachAndExit = false)
    {

        IProgress<JSONMessage> progress = (ANSIConsole != null) ? new clsProgressOutput(ANSIConsole) : new Progress();
       
        //Pull Image
        Program.DockerManager.CreateImageAsync(DockerImage, new AuthConfig(), progress).Wait();

        //Set container Run Parameters
        var runParams = new CreateContainerParameters
        {
            Name = "evm_bridge",
            Env = null,
            Cmd = null,
            ArgsEscaped = false,
            Image = DockerImage.FromImage,
            Entrypoint = entrypoint,
            Labels = null,
            AttachStderr = attachAndExit,
            AttachStdin = attachAndExit,
            AttachStdout = true,
            OpenStdin = attachAndExit,
            StdinOnce = false,
            Tty = attachAndExit,
            HostConfig = new HostConfig
            {
                Binds = new List<string>()
                {
                    $"/root/.accumulatebridge:/home/app/values",
                },
                Privileged = false,
             //   AutoRemove = attachAndExit,
                ReadonlyRootfs = false,
                NetworkMode = "host",
                RestartPolicy = new RestartPolicy
                {
                    Name = RestartPolicyKind.UnlessStopped,
                    MaximumRetryCount = 0,
                }

            }
        };


        if (ANSIConsole != null)
        {
            runParams.Env = new List<string>() { $"COLUMNS={ANSIConsole.Profile.Width}", $"LINES={ANSIConsole.Profile.Height}" };
        }
        
        Program.DockerManager.DeleteContainerIfExist(runParams.Name).Wait();
        
        //Create container
        var result = Program.DockerManager.CreateContainerAsync(runParams).Result;
        
        Log.Logger.Information("{clsDockerImageEVMBridge}: {result}",nameof(clsDockerImageEVMBridge),result);
        foreach (var warn in result.Warnings)
        {
            Log.Logger.Warning("{image} {warn}",DockerImage.Repo,warn);
        }
        
        return result;
    }
}

