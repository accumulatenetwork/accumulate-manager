using ConsoleServer;
using Docker.DotNet;
using Docker.DotNet.Models;
using ConsoleServerDocker;
using Serilog;

namespace AccumulateManager;


public class clsDockerImageAccBackup : IDockerImage
{



    private AnsiTelnetConsole ANSIConsole;
    
    ImagesCreateParameters DockerImage = new ImagesCreateParameters
    {
        FromImage = "registry.gitlab.com/accumulatenetwork/utils/accbackup",
    };

    public clsDockerImageAccBackup(AnsiTelnetConsole console)
    {
        ANSIConsole = console;
    }
   
    public CreateContainerResponse Create(string[] entrypoint,string volume, bool attachAndExit = false)
    {

        IProgress<JSONMessage> progress = (ANSIConsole != null) ? new clsProgressOutput(ANSIConsole) : new Progress();
       
        //Pull Image
        Program.DockerManager.CreateImageAsync(DockerImage, new AuthConfig(), progress).Wait();

        //Set container Run Parameters
        var runParams = new CreateContainerParameters
        {
            Name = "accbackup",
            Env = new List<string>(){$"AWS_ACCESS_KEY_ID={AWS_ACCESS_KEY_ID}",$"AWS_SECRET_ACCESS_KEY={AWS_SECRET_ACCESS_KEY}",$"S3_BUCKET_NAME={volume}"},
            Cmd = null,
            ArgsEscaped = false,
            Image = DockerImage.FromImage,
            Entrypoint = entrypoint,
            Labels = null,
            AttachStderr = attachAndExit,
            AttachStdin = attachAndExit,
            AttachStdout = true,
            OpenStdin = attachAndExit,
            StdinOnce = false,
            Tty = attachAndExit,
            HostConfig = new HostConfig
            {
                Binds = new List<string>()
                {
                    $"{volume}:/data",
                },
                Privileged = false,
                AutoRemove = attachAndExit,
                ReadonlyRootfs = false,
               // NetworkMode = "host",
                RestartPolicy = new RestartPolicy
                {
                    Name = RestartPolicyKind.No,
                    MaximumRetryCount = 0,
                }
            }
        };


        if (ANSIConsole != null)
        {
            runParams.Env.Add($"COLUMNS={ANSIConsole.Profile.Width}");
            runParams.Env.Add( $"LINES={ANSIConsole.Profile.Height}");
        }
        
        Program.DockerManager.DeleteContainerIfExist(runParams.Name).Wait();
        
        //Create container
        var result = Program.DockerManager.CreateContainerAsync(runParams).Result;
        
        Log.Logger.Information("{clsDockerImageEVMBridge}: {result}",nameof(clsDockerImageEVMBridge),result);
        foreach (var warn in result.Warnings)
        {
            Log.Logger.Warning("{image} {warn}",DockerImage.Repo,warn);
        }
        
        return result;
    }


    private static string? aws_access_key_id;
    public static string? AWS_ACCESS_KEY_ID
    {
        get
        {
            if (aws_access_key_id != null) return aws_access_key_id;
            return aws_access_key_id = Environment.GetEnvironmentVariable("AWS_ACCESS_KEY_ID", EnvironmentVariableTarget.Process) ?? "";
        }
    }

    private static string? aws_secret_access_key;
    public static string? AWS_SECRET_ACCESS_KEY
    {
        get
        {
            if (aws_secret_access_key != null) return aws_secret_access_key;
            return aws_secret_access_key = Environment.GetEnvironmentVariable("AWS_SECRET_ACCESS_KEY", EnvironmentVariableTarget.Process) ?? "";
        }
    }



    public static bool isAWSKeysSet()
    {
        return !string.IsNullOrEmpty(AWS_ACCESS_KEY_ID) && !string.IsNullOrEmpty(AWS_SECRET_ACCESS_KEY);
    }


    public static void Start()
    {
        if (isAWSKeysSet())
        {
            var volumeResponse = clsAccNetworkMap.NetworkInstalled?.GetVolumeResponse().Result;
            var docker = new clsDockerImageAccBackup(null);
            var dockerContainer = docker.Create(new[] { "/start.sh","backup","shutdown" }, volumeResponse.Name);
            Program.DockerManager.GetClient().Containers.StartContainerAsync(dockerContainer.ID, new ContainerStartParameters()).Wait();
        }
    }

    public static bool IsRunning()
    {
        var container = Program.DockerManager.GetContainer("accbackup").Result;
        if (container == null) return false;
        if (container.State.Contains("unning")) return true;
        return false;
    }

}

