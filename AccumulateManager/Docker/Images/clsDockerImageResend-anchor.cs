using ConsoleServer;
using Docker.DotNet;
using Docker.DotNet.Models;
using Serilog;
using ILogger = Serilog.ILogger;

namespace AccumulateManager;

public class clsDockerImageResendAnchor : IDockerImage
{

    ImagesCreateParameters ResendAnchorImage = new ImagesCreateParameters
    {
        FromImage = "registry.gitlab.com/accumulatenetwork/accumulate/resend-anchor",
        Tag = "latest",
    };

    public clsDockerImageResendAnchor()
    {

    }
    

    public CreateContainerResponse Create(string dockerID)
    {

       // IProgress<JSONMessage> progress = (console != null) ? new clsProgressOutput(console) : new Progress();

        //Pull Image
        Program.DockerManager.CreateImageAsync(ResendAnchorImage, new AuthConfig(), new Progress());
            
        //Set Watchtower container Run Parameters
        var runParams = new CreateContainerParameters
        {
            Name = dockerID,
            Env = null,
            Cmd = null,
            ArgsEscaped = false,
            Image = ResendAnchorImage.FromImage,
            Entrypoint = null,
            Labels = null,
           // AttachStdout = null,
           // Tty = (console != null),
            HostConfig = new HostConfig()
            {
                RestartPolicy = new RestartPolicy
                {
                    Name = RestartPolicyKind.UnlessStopped,
                    MaximumRetryCount = 0,
                },
               // AutoRemove = true
            }
        };

        Program.DockerManager.DeleteContainerIfExist(runParams.Name).Wait();
        
        //Create container
        var result = Program.DockerManager.CreateContainerAsync(runParams).Result;
        Log.Information("CreateContainerAsync: {result}",result);
        var IsRunning = Program.DockerManager.GetClient().Containers.InspectContainerAsync(dockerID).Result;
        if (!IsRunning.State.Running)
        {
            var result2 = Program.DockerManager.GetClient().Containers.StartContainerAsync(dockerID, new ContainerStartParameters()).Result;
            Log.Information("StartContainerAsync: {result2}",result2);
        }

        return result;

    }

}