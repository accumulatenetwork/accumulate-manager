using Google.Protobuf;
using Proto.API;

namespace AccumulateManager;
public interface IManagerTable<TProto,TProtoList> : IDisposable where TProto:Google.Protobuf.IMessage where TProtoList:Google.Protobuf.IMessage
{
    public TProtoList ProtoWrapper { get; }
  //  public void Load(Func<TProtoList> defaultData);
    
    public Action<TProto, TProto> MapFields { get; init; }
    
   // public MsgReply Update(TProto proto);
   // public MsgReply Add(TProto proto);
   // public MsgReply Delete(UInt32 ID);
}