using System.Security.Cryptography.X509Certificates;
using Docker.DotNet.Models;
using Google.Protobuf;
using Google.Protobuf.Collections;
using Proto.API;
using Proto.Settings;
using Serilog;
using SharpYaml;
using SharpYaml.Serialization;

namespace AccumulateManager;

static public class clsSettings
{
    static public string PathSettings { get; private set; }

    static clsSettings()
    {
        PathSettings = Path.Combine(Program.DataPath, "config.yaml");
    }

    static public Proto.Settings.Settings Load(bool waitForConfig = false)
    {
        do
        {
            if (File.Exists(PathSettings))
            {
                //Read from file
                try
                {
                    Log.Information("Loading {path}",PathSettings);
                    using (var readFile = new StreamReader(PathSettings))
                    {
                        var serializer = new Serializer();
                        return serializer.Deserialize<Proto.Settings.Settings>(readFile);
                    }
                }
                catch (Exception e)
                {
                    Log.Error(e,"Error");
                    if (!waitForConfig) throw;
                }
            }
            
            if (!waitForConfig) break;
            
            Log.Information("{settings} Waiting for config file.",nameof(Settings));
            Thread.Sleep(5000);

        } while (waitForConfig);

        Log.Information("{settings} No settings found. Using defaults for {nodetype}.",nameof(Settings),Enum.GetName(typeof(AccManNode.Types.NodeT), Program.ThisAccManNode.NodeType));
        
        var setings = new Settings
        {
            P2PPort = 16666,
            ConsolePort = 3333,
            Accumulate = new Proto.Settings.Accumulate
            {
            },
            Proxy = new Proto.Settings.Proxy
            {
            }
        };

        var localNode = new Accumulate.Types.AccumulateNetwork
        {
            Name = "AccumulateAPI",
        };
        localNode.UrlJsonAPI.Add("http://localhost:16695");
        setings.Accumulate.Network.Add(localNode);

        setings.Proxy.Network.Add(new Proxy.Types.ProxyNetwork()
        {
            Name = "AccumulateAPI",
            UrlJsonAPI = 443,
        });
        setings.Proxy.Network.Add(new Proxy.Types.ProxyNetwork()
        {
            Name = "AccumulateAPI",
            UrlJsonAPI = 6695,
        });

        return setings;
    }

    static public void LoadEnvironmentVariables()
    {
        var domains = Environment.GetEnvironmentVariable("ACCU_DOMAIN", EnvironmentVariableTarget.Process);
        if (!string.IsNullOrEmpty(domains))
        {
            foreach (var domain in domains.Split(new char[]{',',' ',';'}))
            {
                if (!Program.Settings.DomainNames?.Contains(domain) ?? true)
                {
                    Program.Settings.DomainNames.Add(domain);
                    Log.Logger.Information("ACCU_DOMAIN={cor}",domain);
                }
            }
        }
        
        var cors = Environment.GetEnvironmentVariable("ACCU_CORS", EnvironmentVariableTarget.Process);
        if (!string.IsNullOrEmpty(cors))
        {
            foreach (var cor in cors.Split(new char[]{',',' ',';'}))
            {
                if (!Program.Settings.CorsURL?.Contains(cor) ?? true)
                {
                    Program.Settings.CorsURL.Add(cor);
                    Log.Logger.Information("ACCU_CORS={cor}",cor);
                }
            }
        }
        

        var domainEmail = Environment.GetEnvironmentVariable("DOMAIN_EMAIL", EnvironmentVariableTarget.Process);
        if (!String.IsNullOrEmpty(domainEmail))
        {
            if (string.IsNullOrEmpty(Program.Settings.DomainEmail))
            {
                Program.Settings.DomainEmail=domainEmail;
                Log.Logger.Information("ACCU_DOMAIN_EMAIL={network}",domainEmail);
            }
            else
            {
                Log.Logger.Information("Ignoring Env ACCU_DOMAIN_EMAIL (dupe)");
            }
        }

        var certs = Environment.GetEnvironmentVariable("CERTS_DIR", EnvironmentVariableTarget.Process);
        if (!String.IsNullOrEmpty(certs))
        {
           Program.DataPathCerts=certs;
           Log.Logger.Information("CERTS_DIR={certs}",certs);
        }
    }


    static public void Update(Proto.Settings.Settings settings)
    {
        
    }

    static public void Save()
    {
        //File.WriteAllBytes(PathSettings, Program.Settings.ToByteArray());
        
        using (var writefile = new StreamWriter(PathSettings))
        {
            var serializer = new Serializer();
            serializer.Serialize(writefile, Program.Settings, typeof(Proto.Settings.Settings));
        }
    }
}