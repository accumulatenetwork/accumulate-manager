using AccumulateManager.TaskManager;

namespace AccumulateManager;

public class clsLoadCertificateTask : ITaskManagerTask
{
    public bool RunOnStartup { get; } = false;
    public bool AllowDuplicates { get; } = false;
    public ITaskManagerTask.TaskStatus Status { get; }
    public DateTime? StartAfter { get; set; } = DateTime.UtcNow.AddSeconds(5);
    public DateTime? DestroyAfter { get; }
    public TimeSpan? Interval { get; }


    public async Task Run()
    {
        clsCertificates.LoadCerts();
    }
}