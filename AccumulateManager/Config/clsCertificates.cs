using System.Security.Cryptography.X509Certificates;
using AccumulateManager.TaskManager;
using Serilog;

namespace AccumulateManager;

public class clsCertificates
{

    static public FileSystemWatcher WatchCertsDir()
    {
        if (Directory.Exists(Program.DataPathCerts))
        {
            Log.Information("{name} Monitoring {file}",nameof(WatchCertsDir),Program.DataPathCerts);
            var watcher = new FileSystemWatcher(Program.DataPathCerts);

            watcher.NotifyFilter = NotifyFilters.CreationTime
                                   | NotifyFilters.FileName
                                   | NotifyFilters.LastWrite;

            watcher.Changed += WatcherOnChanged;
            watcher.Created += WatcherOnChanged;
            watcher.Disposed += (sender, args) =>
            {
                Log.Information("{name} Monitoring Disposed", nameof(WatchCertsDir));
            };

            watcher.IncludeSubdirectories = false;
            watcher.EnableRaisingEvents = true;

            return watcher;
        }
        return null;
    }

    private static void WatcherOnChanged(object sender, FileSystemEventArgs e)
    {
        Log.Information("{name} cert file changed {file} {type}",nameof(WatcherOnChanged),e.Name,e.ChangeType);
        if (new[] { ".crt", ".key", }.Any(c => e.Name.Contains(c)))
        {
            var certTask = new clsLoadCertificateTask();
            clsTaskManager.AddTask<clsLoadCertificateTask>(certTask);
        }
    }

    static public bool LoadCerts()
    {
        if (Directory.Exists(Program.DataPathCerts))
        {
            foreach (var file in Directory.GetFiles(Program.DataPathCerts,"*.crt"))
            {
                try
                {
                    var keyfile = Path.ChangeExtension(file, "key");
                    if (File.Exists(keyfile))
                    {
                        var filename = Path.GetFileName(file);
                        var cert = X509Certificate2.CreateFromPemFile(file, keyfile);
                        if (cert.NotAfter < DateTime.UtcNow)
                        {
                            Log.Error("{func} cert: {cert} has expired", nameof(LoadCerts), filename);
                            return false;
                        }

                        if (Program.SSLCert != null)
                        {
                            var oldcert = Program.SSLCert;
                            Program.SSLCert = cert;
                            oldcert.Dispose();
                        }
                        else
                        {
                            Program.SSLCert = cert;
                        }

                        Log.Information("{func} Loaded cert: {cert}", nameof(LoadCerts), filename);
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Loading cert {file}",file);
                }
            }
        }
        else
        {
            Log.Information("{func} {dir} not found",nameof(LoadCerts),Program.DataPathCerts);
        }
        return false;
    }


}