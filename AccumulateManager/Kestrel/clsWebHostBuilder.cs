using System.Net;
using App.Metrics.AspNetCore.Endpoints;
//using App.Metrics.Formatters.Prometheus;
using Microsoft.AspNetCore.Connections;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Https;
using Serilog;

namespace AccumulateManager;

public class clsWebHostBuilder :  HostBuilder
{

    public clsWebHostBuilder()
    {

        this.UseSerilog();
        /*this.UseMetricsWebTracking()
            .UseMetricsEndpoints(options =>
            {
                options.MetricsEndpointOutputFormatter = new MetricsPrometheusProtobufOutputFormatter();
            });*/

        this.ConfigureWebHost(webHost =>
        {
            Log.Information($"Configuring Kestrel");            
            webHost.UseKestrel(k =>
            {

                k.ConfigureHttpsDefaults(h =>
                {
                    h.ClientCertificateMode = ClientCertificateMode.NoCertificate;
                    h.ClientCertificateValidation = (certificate, chain, errors) =>
                    {
                        return true; //certificate.Issuer == serverCert.Issuer;
                    };
                });

                var ssCert = SelfSignedCertificate.GetSelfSignedCertificate();

                //P2P port
                Log.Information($"Adding Port: {Program.Settings.P2PPort} P2P");
                k.Listen(IPAddress.Any, (int)Program.Settings.P2PPort, options =>
                {
                    options.UseHttps(ssCert, options =>
                    {
                        options.AllowAnyClientCertificate();
                    });

                });

                try
                {
                    //Proxy ports
                    foreach (var port in Program.ProxyConfigProvider.ListenPortList)
                    {
                        k.Listen(IPAddress.Any, (int)port, options =>
                        {
                            if ((Program.Settings.DomainNames?.Count ?? 0) == 0)
                            {
                                Log.Information($"Adding HTTPS Port: {port} (Self Cert)");

                                if (Program.SSLCert != null) options.UseHttps(Program.SSLCert);
                                else options.UseHttps(ssCert);
                            }
                            else
                            {
                                Log.Information($"Adding HTTPS Port: {port} (Certified)");
                                options.UseHttps(o =>
                                {
                                    o.ClientCertificateMode = ClientCertificateMode.NoCertificate;  //.AllowCertificate;
                                 //   o.AllowAnyClientCertificate();

                                    if (Program.SSLCert != null) o.ServerCertificateSelector = (context, dnsName) => Program.SSLCert;  //Load dynamically

#if DEBUG
                                    else o.ServerCertificate = ssCert;
#else
                                    else o.UseLettuceEncrypt(k.ApplicationServices);
#endif
                                });
                            }
                        });
                    }

                }
                catch (Exception e)
                {
                    Log.Error(e,"Proxy Port Setup");
                    throw;
                }

                try
                {
                    if (Program.Settings.WWWPort > 0)
                    {
                        k.Listen(IPAddress.Any, (int)Program.Settings.WWWPort, options =>
                        {
                            if ((Program.Settings.DomainNames?.Count ?? 0) == 0)
                            {
                                Log.Information("Adding Port: {port} (Self Cert)", Program.Settings.WWWPort);

                                if (Program.SSLCert != null) options.UseHttps(Program.SSLCert);
#if !DEBUG
                                else options.UseHttps(ssCert);
#endif
                            }
                            else
                            {
                                options.UseHttps(o =>
                                {
                                    Log.Information("Adding Port: {port} (Certified)", Program.Settings.WWWPort);

                                    if (Program.SSLCert != null) o.ServerCertificateSelector = (context, dnsName) => Program.SSLCert;  //Load dynamically
#if !DEBUG
                                    else o.UseLettuceEncrypt(k.ApplicationServices);
#endif
                                });
                            }
                        });
                    }
                }
                catch (Exception e)
                {
                    Log.Error(e,"Setup WWW");
                }
                
            });
            webHost.UseStartup<Startup>();
        });
    }
}