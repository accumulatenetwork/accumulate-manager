using System.Collections;
using Microsoft.Extensions.Primitives;
using Serilog;
using Yarp.ReverseProxy.Configuration;
using Yarp.ReverseProxy.LoadBalancing;

namespace AccumulateManager;

public class clsProxyConfigProvider : IProxyConfigProvider
{
    private volatile CustomMemoryConfig _config;
  //  private Dictionary<string, ClusterConfig> ClusterConfigList = new Dictionary<string, ClusterConfig>();
  //  private Dictionary<string, RouteConfig> RouteConfigList = new Dictionary<string, RouteConfig>();
    public List<uint> ListenPortList = new List<uint>();

    public IProxyConfig GetConfig() => _config;
    
    public clsProxyConfigProvider()
    {
    }

    public clsProxyConfigProvider Build()
    {
        Log.Information("clsProxyConfigProvider build");
        
        var Routes = new List<RouteConfig>();
        var Clusters = new List<ClusterConfig>();

        foreach (var proxyNetwork in Program.Settings.Proxy.Network)
        {
            var route = new RouteConfig
            {
                RouteId = $"{proxyNetwork.Name}-{proxyNetwork.UrlJsonAPI}",
                ClusterId = proxyNetwork.Name,
                CorsPolicy = Program.CORSpolicy,  //https://microsoft.github.io/reverse-proxy/articles/cors.html
                Match = new RouteMatch
                {
                    Hosts = new string[]{ $"*:{proxyNetwork.UrlJsonAPI}"}
                }
            };
            Routes.Add(route);
            if (!ListenPortList.Contains(proxyNetwork.UrlJsonAPI)) ListenPortList.Add(proxyNetwork.UrlJsonAPI);
            Log.Information("Proxy: Adding route: {id} {match}",route.RouteId, route.Match.Hosts);
        }

        foreach (var proxyNetwork in Program.Settings.Accumulate.Network)
        {
            var destination = new Dictionary<string, DestinationConfig>();
            foreach (var url in proxyNetwork.UrlJsonAPI)
            {
                destination.Add($"{proxyNetwork.Name}-{destination.Count}", new DestinationConfig() { Address = url });
                Log.Information("Proxy: Adding destination: {name} {url}",proxyNetwork.Name, url);
            }
            
            var cluster = new ClusterConfig()
            {
                ClusterId = proxyNetwork.Name,
                LoadBalancingPolicy = LoadBalancingPolicies.RoundRobin,
                Destinations = destination,
            };
            Clusters.Add(cluster);
        }
        _config = new CustomMemoryConfig(Routes, Clusters);
        return this;
    }


    /*public void AddMatchConfig(string networkName, params int[] ports)
    {
        if (RouteConfigList.ContainsKey(networkName)) throw new Exception("Duplicate Network Name");

        string[] urls = new string[ports.Length];
        for (int i = 0; i < ports.Length; i++)
        {
            urls[i] = $"*:{ports[i]}";
            if (!ListenPortList.Contains(ports[i])) ListenPortList.Add(ports[i]);
        }
        
        var routeConfig = new RouteConfig
        {
            RouteId = $"{networkName}-{ClusterConfigList.Count}",
            ClusterId = networkName,
            //CorsPolicy = "policy_name",  //https://microsoft.github.io/reverse-proxy/articles/cors.html
            Match = new RouteMatch
            {
                Hosts = urls
            }
        };
        RouteConfigList.Add(networkName, routeConfig);
    }

    public void AddClusterConfig(string networkName, string[] Urls)
    {
        if (ClusterConfigList.ContainsKey(networkName)) throw new Exception("Duplicate Network Name");

        var destination = new Dictionary<string, DestinationConfig>();
        foreach (var url in Urls)
        {
            destination.Add($"{networkName}-{destination.Count}", new DestinationConfig() { Address = url });
        }

        var clusterConfig = new ClusterConfig
        {
            ClusterId = networkName,
            LoadBalancingPolicy = LoadBalancingPolicies.RoundRobin,
            Destinations = destination,
        };

        ClusterConfigList.Add(networkName, clusterConfig);
    }*/
    
    
    /// <summary>
    /// By calling this method from the source we can dynamically adjust the proxy configuration.
    /// Since our provider is registered in DI mechanism it can be injected via constructors anywhere.
    /// </summary>
    public void Update(IReadOnlyList<RouteConfig> routes, IReadOnlyList<ClusterConfig> clusters)
    {
        var oldConfig = _config;
        _config = new CustomMemoryConfig(routes, clusters);
        oldConfig.SignalChange();
    }

    private class CustomMemoryConfig : IProxyConfig
    {
        private readonly CancellationTokenSource _cts = new CancellationTokenSource();

        public CustomMemoryConfig(IReadOnlyList<RouteConfig> routes, IReadOnlyList<ClusterConfig> clusters)
        {
            Routes = routes;
            Clusters = clusters;
            ChangeToken = new CancellationChangeToken(_cts.Token);
        }

        public IReadOnlyList<RouteConfig> Routes { get; }

        public IReadOnlyList<ClusterConfig> Clusters { get; }

        public IChangeToken ChangeToken { get; }

        internal void SignalChange()
        {
            _cts.Cancel();
        }
    }
}