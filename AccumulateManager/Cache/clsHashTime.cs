namespace AccumulateManager.Cache;

public class clsHashTime
{
    public DateTime LastAccess;
    private List<clsQueryTxJSON.Receipt> _hashReceipt;

    public clsHashTime(List<clsQueryTxJSON.Receipt> hashReceipt)
    {
        _hashReceipt = hashReceipt;
        LastAccess = DateTime.UtcNow;
    }

    public List<clsQueryTxJSON.Receipt> HashReceipt
    {
        get
        {
            LastAccess = DateTime.UtcNow;
            return _hashReceipt;
        }
    }

}