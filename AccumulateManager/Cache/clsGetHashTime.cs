namespace AccumulateManager.Cache;

static public class clsGetHashTime
{

    public static Dictionary<string, clsHashTime> HashTimeCache = new Dictionary<string, clsHashTime>();

    static public List<clsQueryTxJSON.Receipt> GetHashReceipt(string hash)
    {
        clsHashTime hashtime = null;
        if (HashTimeCache.TryGetValue(hash, out hashtime))
            return hashtime.HashReceipt;
        else
            return null;
    }


    public static void SetHash(string hash,List<clsQueryTxJSON.Receipt> receipts)
    {
        HashTimeCache.TryAdd(hash,new clsHashTime(receipts));
    }
}