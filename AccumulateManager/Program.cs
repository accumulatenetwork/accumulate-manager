using System.Net;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using AccumulateManager.IPTables;
using AccumulateManager.TaskManager;
using AccumulateManager.TelnetConsole;
using Serilog;
using AccumulateManagerCommon;
using ConsoleServer;
using Docker.DotNet;
using ConsoleServerDocker;
using Google.Protobuf;
using Google.Protobuf.Collections;
using Grpc.Net.Client;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Mono.Unix;
using Mono.Unix.Native;
using Proto.API;
//using ProtoBuf.Meta;
using Serilog.Events;
using SharpYaml.Serialization;
using Spectre.Console;
using IPAddress = Org.BouncyCastle.Utilities.Net.IPAddress;

namespace AccumulateManager;

public static class Program
{

    static public Proto.Settings.Settings Settings { get; private set; }
    
    static public string DataPath { get; private set; } = "data";
    static public string? DataPathWWW { get; private set; }
    static public string? DataPathCerts { get; set; } = "/certs";
    static public clsDistributedProxyList DistributedProxyList  = new clsDistributedProxyList();
    static public AccManNode ThisAccManNode { get; private set; }
  //  static public bool EnableIPV6 { get; private set; }
    
  //  static public int HttpsP2PPort { get; set; } = 16666;

    static public CancellationTokenSource CancelTokenSource = new CancellationTokenSource();
    static public GrpcChannelOptions SelfCertOptions;
    static public clsProxyConfigProvider ProxyConfigProvider = new clsProxyConfigProvider();
    static public readonly string CORSpolicy = "CorsPolicy";
    static public clsDockerManager DockerManager;
    static public string PublicIPV4  { get; private set; }
    static public string PublicIPV6  { get; private set; }
    static public bool SaveChanges = false;
    static public bool RestartRequired = false;
    static public bool FirewallActive = false;
    static public bool DevMode = false;
    static public X509Certificate2? SSLCert = null;
    //static public string VersionTag { get; private set; }
    static public bool NewVersion { get; set; }
    static public FileSystemWatcher WatchCertsDirectory { get; set; }

    static public AccumulateAPI_Status NodeStatus1;
    static public AccumulateAPI2_Status NodeStatus2;
    static public List<string> OriginList = new List<string>();
    static public bool SyncOnlyMode;

    public enum EnumRuntimeMode
    {
        Normal = 0,
        SyncWaitToStart,
        SyncWaitForComplete,
        SyncComplete,
        BackupStarted,
        BackupStopped,
        Shutdown
    }

    static public EnumRuntimeMode RunTimeMode { get; set; }

    public static async Task Main(string[] args)
    {
        Log.Logger = new LoggerConfiguration()
            .MinimumLevel.Debug()
            .Enrich.FromLogContext()
#if DEBUG
            .WriteTo.Console(restrictedToMinimumLevel: LogEventLevel.Debug)
#else
            .WriteTo.Console(restrictedToMinimumLevel: LogEventLevel.Warning)
#endif
            //  .WriteTo.File("logfile.log", rollingInterval: RollingInterval.Day)
            .WriteTo.ConsoleTelnetServer()
            .CreateLogger();

        ThisAccManNode = new AccManNode()
        {
            ID = AccumulateManagerCommon.Extentions.GetRandomID(clsDistributedProxyList.RandomIDMin, clsDistributedProxyList.RandomIDMax),
        };


        var GitHash = AssemblyGitCommit.GetValue();
        if (!String.IsNullOrEmpty(GitHash))
        {
            ThisAccManNode.AccManTag = AssemblyGitCommitTag.GetValue();
            Log.Information("Git Hash: {GitHash}",GitHash);
            Log.Information("Git Branch: {Branch}  Tag: {Tag} {date:yyyy-MM-dd HH:mm:ss}",AssemblyGitCommitBranch.GetValue(),ThisAccManNode.AccManTag,AssemblyGitCommitDateTime.GetValue());
        }

        DataPath = Environment.GetEnvironmentVariable("AccMan_DATA", EnvironmentVariableTarget.Process) ?? "/data";
        if (!Directory.Exists(DataPath))
        {
#if !DEBUG            
            Log.Fatal("Data path not found: {path}",DataPath);
            return;
#else
            DataPath = "/home/stuart/Projects/AccumulateManager/AccumulateManager/Config";
            DataPathCerts = DataPath;
#endif
        }

        Boolean.TryParse(Environment.GetEnvironmentVariable("DEVMODE", EnvironmentVariableTarget.Process), out DevMode);


        DataPathWWW = Environment.GetEnvironmentVariable("AccMan_WWW", EnvironmentVariableTarget.Process) ?? Path.Combine(DataPath,"www");

        var nodeType = Environment.GetEnvironmentVariable("ACCU_NODETYPE", EnvironmentVariableTarget.Process);
        if (Enum.TryParse<AccManNode.Types.NodeT>(nodeType, true, out AccManNode.Types.NodeT nodeT))
        {
            ThisAccManNode.NodeType = nodeT;
        }
       
        if ((Settings = clsSettings.Load(args.Any(x=>x.EndsWith("wait")))) == null) return;
        clsSettings.LoadEnvironmentVariables();
        clsCertificates.LoadCerts();
        WatchCertsDirectory = clsCertificates.WatchCertsDir();
        if (Directory.Exists(DataPathWWW) && Path.IsPathRooted(DataPathWWW))
        {
            Log.Information("Data Dir: {dir}",DataPathWWW);
        }
        else
        {
            Log.Information("Data Dir: {dir}. Not found.",DataPathWWW);
            DataPathWWW = null;
        }
        

        PublicIPV4 = await Extentions.GetPublicIpAddress();
        Log.Information("Public IPV4 = {ip}",PublicIPV4);
        PublicIPV6 = await Extentions.GetPublicIp6Address();
        Log.Information("Public IPV6 = {ip}",PublicIPV6);

        if (ThisAccManNode.UrlP2P.Count == 0)
        {
            var publicurl = $"https://{PublicIPV4}:16666";;
            if (!string.IsNullOrEmpty(publicurl))
            {
                ThisAccManNode.UrlP2P.Add(publicurl);
                Log.Information("Local IP: {ip}", publicurl);
            }
            else
            {
                Log.Fatal("Cant get local IP");
                return;
            }
        }

        DockerManager = new clsDockerManager("accumulate_manager","registry.gitlab.com/accumulatenetwork/accumulate-manager:latest",CancelTokenSource);

        var telnetServer = new ConsoleServer.TelnetServer((int)Settings.ConsolePort);
        Log.Information("Starting Telnet Server");
        telnetServer.StartListen( new Action<TelnetSession>((session) => 
        {
           var telnetAppSession = new clsTelnetAppSession(session, Log.Logger);
           telnetAppSession.Run();
        }));

        SelfCertOptions = new GrpcChannelOptions()
        {
            HttpHandler = new HttpClientHandler()
            {
                ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator
            }
        };

        ProxyConfigProvider.Build();
        if (!Settings.DisableFirewall) await clsIPTables.UpdateIPTables(true);

        Boolean.TryParse(Environment.GetEnvironmentVariable("SYNCONLY", EnvironmentVariableTarget.Process), out SyncOnlyMode);

        if (SyncOnlyMode)
        {
            Program.RunTimeMode = EnumRuntimeMode.SyncWaitToStart;
            Log.Information("SYNCONLY mode");
        }

        Log.Information($"Start clsWebHostBuilder");
        var hostBuilder = new clsWebHostBuilder();
        
        var hostTask = hostBuilder.Build().RunAsync(CancelTokenSource.Token);
        clsTaskManager.Run();
        clsSeedList.BuildSeedList(args);

        UnixSignal[] signals = new UnixSignal[] {
            new UnixSignal(Signum.SIGQUIT),
            new UnixSignal(Signum.SIGTERM),
        };

        do
        {
            int id = UnixSignal.WaitAny(signals,10000);
            if (id >= 0 && id < signals.Length)
            {
                if (signals[id].IsSet)
                {
                    Log.Fatal($"UNIX Signal {signals[id]}");
                    CancelTokenSource.Cancel();
                    break;
                }
            }
            clsTaskManager.Run();
        } while (!CancelTokenSource.IsCancellationRequested);

        clsTaskManager.RemoveAllTasks();
        WatchCertsDirectory.Dispose();
        SSLCert?.Dispose();
        DockerManager.Dispose();
        telnetServer.Dispose();
        Log.CloseAndFlush();
    }
}