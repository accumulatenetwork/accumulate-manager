using ConsoleServerDocker;
using Serilog;

namespace AccumulateManager.TaskManager.StartupTasks;

public class CheckCert : ITaskManagerTask
{
    public bool RunOnStartup { get; } = true;
    public bool AllowDuplicates { get; } = false;
    public ITaskManagerTask.TaskStatus Status { get; }
    public DateTime? StartAfter { get; set; }
    public DateTime? DestroyAfter { get; }
    public TimeSpan? Interval { get; }

    public CheckCert()
    {
        StartAfter = DateTime.UtcNow;
        Interval = TimeSpan.FromHours(12);
    }

    public async Task Run()
    {

        if (Program.SSLCert?.NotAfter is not null && File.Exists(Program.DataPathCerts))
        {
            if (Program.SSLCert.NotAfter > DateTime.UtcNow.AddDays(-7))
            {
                var runScript = Path.Combine(Program.DataPathCerts, "run");
                try
                {
                    if (File.Exists(runScript))
                    {
                        Log.Information($"Try updating cert {runScript}");
                        System.Diagnostics.Process.Start("bash",runScript);
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex,$"Try updating cert {runScript}");
                }
            }
        }

    }

}