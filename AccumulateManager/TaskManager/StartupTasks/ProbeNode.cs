using Serilog;

namespace AccumulateManager.TaskManager.StartupTasks;

public class ProbeNode : ITaskManagerTask
{
    public bool RunOnStartup { get; } = true;
    public bool AllowDuplicates { get; } = false;
    public ITaskManagerTask.TaskStatus Status { get; }
    public DateTime? StartAfter { get; set; }
    public DateTime? DestroyAfter { get; }
    public TimeSpan? Interval { get; }


    public ProbeNode()
    {
        StartAfter = DateTime.UtcNow;
        Interval = TimeSpan.FromSeconds(30);
    }

    public async Task Run()
    {
        try
        {

            using (var httpClient = new HttpClient())
            {
                Program.NodeStatus1 = httpClient.GetFromJsonAsync<AccumulateAPI_Status>($"http://localhost:16695/status").Result;
            }

            using (var httpClient = new HttpClient())
            {
                Program.NodeStatus2 = httpClient.GetFromJsonAsync<AccumulateAPI2_Status>($"http://localhost:16692/status").Result;
            }

            if (Program.RunTimeMode == Program.EnumRuntimeMode.SyncWaitForComplete && ! (Program.NodeStatus2?.result.sync_info.catching_up ?? true))
            {
                Log.Information($"BVN Height: {Program.NodeStatus1?.bvnHeight}");
                Program.RunTimeMode = Program.EnumRuntimeMode.SyncComplete;
            }
        }
        catch (Exception ex)
        {
            Log.Information("Failed to connect to Accumulate");
        }
    }


}