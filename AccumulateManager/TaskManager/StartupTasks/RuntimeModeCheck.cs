using ConsoleServerDocker;
using Docker.DotNet.Models;
using Serilog;

namespace AccumulateManager.TaskManager.StartupTasks;

public class RuntimeModeCheck : ITaskManagerTask
{
    public bool RunOnStartup { get; } = true;
    public bool AllowDuplicates { get; } = false;
    public ITaskManagerTask.TaskStatus Status { get; }
    public DateTime? StartAfter { get; set; }
    public DateTime? DestroyAfter { get; }
    public TimeSpan? Interval { get; }

    private uint ProjectID = 37886342;
    private uint ContainerRepoID = 3246287;

    public RuntimeModeCheck()
    {
        StartAfter = DateTime.UtcNow.AddSeconds(60);
        Interval = TimeSpan.FromSeconds(60);
    }

    public async Task Run()
    {

        if (Program.RunTimeMode == Program.EnumRuntimeMode.Normal) return;

        var preRuntime = Program.RunTimeMode;
        var status = clsAccNetworkMap.GetStatus().Result;
        var container = Program.DockerManager.GetContainer(clsAccNetworkMap.NetworkInstalled.DockerName).Result;

        try
        {
            switch (Program.RunTimeMode)
            {
                case Program.EnumRuntimeMode.Normal:
                    break; //Do nothing
                case Program.EnumRuntimeMode.SyncWaitToStart:

                    if (status == clsAccNetworkMap.Status.Stopped)
                    {
                        //Start accumulate
                        if (container != null) Program.DockerManager.GetClient().Containers.StartContainerAsync(container.ID, new ContainerStartParameters());
                    }
                    else if (status == clsAccNetworkMap.Status.Running)
                    {
                        Program.RunTimeMode = Program.EnumRuntimeMode.SyncWaitForComplete;
                    }

                    break;
                case Program.EnumRuntimeMode.SyncWaitForComplete:
                    break; // See ProbeNode
                case Program.EnumRuntimeMode.SyncComplete:

                    if (status == clsAccNetworkMap.Status.Running && container != null) Program.DockerManager.StopContainer(container.ID);

                    if (clsDockerImageAccBackup.isAWSKeysSet())
                    {
                        clsDockerImageAccBackup.Start();
                        Program.RunTimeMode = Program.EnumRuntimeMode.BackupStarted;
                    }
                    else
                    {
                        Program.RunTimeMode = Program.EnumRuntimeMode.Shutdown;
                    }

                    break;
                case Program.EnumRuntimeMode.BackupStarted:
                    if (!clsDockerImageAccBackup.IsRunning()) Program.RunTimeMode = Program.EnumRuntimeMode.BackupStopped;
                    break;
                case Program.EnumRuntimeMode.BackupStopped:
                    if (!clsDockerImageAccBackup.IsRunning())
                    {
                        Program.RunTimeMode = Program.EnumRuntimeMode.Shutdown;
                        using (var run = Program.DockerManager.CreateRunOnHostInstance())
                        {
                            if (run.AttatchToHost("/bin/sh")) run.WriteCommand("shutdown -h now");
                        }
                    }
                    break;
                case Program.EnumRuntimeMode.Shutdown:
                    break;
            }

            if (preRuntime != Program.RunTimeMode)
            {
                Log.Information($"RunTimeMode change: {preRuntime}->{Program.RunTimeMode}");
            }

        }
        catch (Exception e)
        {
            Log.Error(e,"RuntimeModeCheck");
            throw;
        }
    }

}