using Serilog;

namespace AccumulateManager.TaskManager.StartupTasks;

public class ScanForNodes : ITaskManagerTask
{
    public bool RunOnStartup { get; } = true;
    public bool AllowDuplicates { get; } = false;
    public ITaskManagerTask.TaskStatus Status { get; }
    public DateTime? StartAfter { get; set; }
    public DateTime? DestroyAfter { get; }
    public TimeSpan? Interval { get; set; }


    public ScanForNodes()
    {
#if !DEBUG
        if (!Program.SyncOnlyMode)
        {
            StartAfter = DateTime.UtcNow.AddSeconds(5);
            Interval = TimeSpan.FromSeconds(30);
        }
#endif
    }

    public async Task Run()
    {
        if (Program.DistributedProxyList.NodeList.Count == 0)
        {
            if (Interval.Value.Minutes < 2)  Interval = Interval.Value.Add(TimeSpan.FromSeconds(10));
            if (clsSeedList.SeedList.Count > 0)
            {
                await clsSeedList.TryToConnect(Program.CancelTokenSource.Token);
            }
            else
            {
                //Try to get network base url (if selected)
                clsSeedList.GetSeedsFromNetworkURL();
            }
        }
        else
        {
            Program.DistributedProxyList.ScanAccManNodes(Program.CancelTokenSource.Token);
        }
    }


    public void ClockTick()
    {

    }


}