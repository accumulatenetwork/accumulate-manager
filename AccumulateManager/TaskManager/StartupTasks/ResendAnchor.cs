using Serilog;

namespace AccumulateManager.TaskManager.StartupTasks;

public class ResendAnchor : ITaskManagerTask
{
    public bool RunOnStartup { get; } = true;
    public bool AllowDuplicates { get; } = false;
    public ITaskManagerTask.TaskStatus Status { get; }
    public DateTime? StartAfter { get; set; }
    public DateTime? DestroyAfter { get; }
    public TimeSpan? Interval { get; }


    public ResendAnchor()
    {
        StartAfter = DateTime.UtcNow.AddMinutes(1);
        Interval = TimeSpan.FromHours(1);
    }

    public async Task Run()
    {
        var containerName = "resend_anchor";
        var container = Program.DockerManager.GetContainer(containerName).Result;

        if (container == null || container.State.Contains("xited"))
        {
            try
            {
                var image = new clsDockerImageResendAnchor().Create(containerName);
            }
            catch (Exception ex)
            {
                Log.Error(ex,containerName);
            }
        }

    }

}