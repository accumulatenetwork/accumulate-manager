using ConsoleServerDocker;
using Serilog;

namespace AccumulateManager.TaskManager.StartupTasks;

public class CheckVersion : ITaskManagerTask
{
    public bool RunOnStartup { get; } = true;
    public bool AllowDuplicates { get; } = false;
    public ITaskManagerTask.TaskStatus Status { get; }
    public DateTime? StartAfter { get; set; }
    public DateTime? DestroyAfter { get; }
    public TimeSpan? Interval { get; }

    private uint ProjectID = 37886342;
    private uint ContainerRepoID = 3246287;

    public CheckVersion()
    {
        StartAfter = DateTime.UtcNow;
        Interval = TimeSpan.FromHours(1);
    }

    public async Task Run()
    {
        if (!String.IsNullOrEmpty(Program.ThisAccManNode.AccManTag))
        {
            try
            {
                var gitLab = new clsGitLab(ProjectID, ContainerRepoID);
                var images = await gitLab.GetCommits();
                if (images != null && images.Count > 0)
                {
                    if (!Program.ThisAccManNode.AccManTag.Contains(images[0].name))
                    {
                        Log.Information($"Checking Version: New Version found {images[0].name}");
                        Program.NewVersion = true;
                    }
                    else
                    {
                        Log.Information($"Checking Version: {Program.ThisAccManNode.AccManTag} -> {images[0].name}");
                    }
                }
                else
                {
                    Log.Information($"Checking Version: No new version");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"Checking Version");
            }
        }
        else
        {
            Log.Information("Version check failed, as no Git tag found.");
        }
    }

}