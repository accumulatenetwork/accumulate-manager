using Docker.DotNet.Models;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Grpc.Net.Client;
using Org.BouncyCastle.Utilities.Net;
using Proto.API;
using Proto.Authentication;
using ProtoHelper;
using Serilog;

namespace AccumulateManager;

public class clsAccManNode : IProtoShadowClass<UInt64, AccManNode>
{
    public UInt64 ID => ProtoMessage.ID;
    public AccManNode ProtoMessage { get; init; }

    public string AccessToken { get; set; }
    public DateTime AccessTokenExpiry { get; set; }
    public DateTime ConnectionFromTime { get; set; }
    public DateTime LastSeedRequestTime { get; set; }
    
    public int AccessTokenTimeout
    {
        set { AccessTokenExpiry = DateTime.UtcNow.AddMinutes(value); }
    }

    public bool Authenticated => (!String.IsNullOrEmpty(AccessToken) && AccessTokenExpiry > DateTime.UtcNow);

    private Uri? _uri;
    public Uri? uri
    {
        get
        {
            if (_uri == null)
            {
                foreach (var ip in ProtoMessage.UrlP2P)
                {
                    _uri = new Uri(ip);
                    if (_uri.IsAbsoluteUri || !_uri.Host.Contains(":")) return _uri;
                }

                _uri = null;
            }

            return _uri;
        }
    }

    public Metadata Headers
    {
        get
        {
            var Headers = new Metadata();
            Headers.Add("Authorization", $"Bearer {AccessToken}");
            return Headers;
        }
    }

    public clsAccManNode(AccManNode node)
    {
        ProtoMessage = node;
    }

    public void Update(AccManNode newClass)
    {
        if (this.ProtoMessage.UrlP2P.Count > 0)
        {
            this.ProtoMessage.UrlP2P.Clear();
            this.ProtoMessage.UrlP2P.AddRange(newClass.UrlP2P);
        }

        _uri = null;
    }

    public async Task<bool> NodeLogin(Uri? seedUri = null)
    {
        try
        {
            Log.Information($"NodeLogin: Trying {uri}");

            //Try to login.
            var Channel = GrpcChannel.ForAddress(seedUri ?? uri, Program.SelfCertOptions);

            if (!Authenticated)
            {
                try
                {
                    var AuthAPI = new AccManAuthentication.AccManAuthenticationClient(Channel);
                    var AuthRequest = new NodeAuthenticationRequest() { ID = Program.ThisAccManNode.ID };
                    AuthRequest.URL.AddRange(Program.ThisAccManNode.UrlP2P);
                    var nodeReply = await AuthAPI.NodeAuthenticateAsync(AuthRequest);
                    AccessToken = nodeReply.AccessToken;
                    AccessTokenTimeout = nodeReply.ExpiresIn;
                    Log.Information($"NodeLogin: {uri} returned {nodeReply.AccessToken}");
                }
                catch (Exception ex)
                {
                    Log.Information($"NodeLogin: {uri} failed {ex.GetType().Name}");
                    return false;
                }
            }

            //Get ID & Info
            var API = new AccManAPI.AccManAPIClient(Channel);
            var remoteID = await API.GetAccManNodeAsync(new Empty(), Headers);

            if (ID == 0) ProtoMessage.ID = remoteID.ID;
                else if (ID != remoteID.ID) throw new Exception("ID's dont match!");
            
            
            ProtoMessage.LastSeen = DateTime.UtcNow.ToTimestamp();
            ProtoMessage.Network = remoteID.Network;
            ProtoMessage.NodeTag = remoteID.NodeTag;
            ProtoMessage.AccManTag = remoteID.AccManTag;
            if (remoteID.UrlP2P.Count > 0)
            {
                ProtoMessage.UrlP2P.Clear(); //Todo optimise
                ProtoMessage.UrlP2P.AddRange(remoteID.UrlP2P);
            }

            return true;
        }
        catch (Exception ex)
        {
            Log.Information($"NodeLogin: {uri} failed {ex.GetType().Name}");
        }
        return false;
    }

    public async Task<AccManNodeList> RefreshNodeList()
    {
        if (!Authenticated) await NodeLogin();
       
        var Channel = GrpcChannel.ForAddress(uri ?? throw new InvalidOperationException(), Program.SelfCertOptions);
        var API = new AccManAPI.AccManAPIClient(Channel);
        ProtoMessage.LastSeen = DateTime.UtcNow.ToTimestamp();
        var reply = await API.GetIPListAsync(new requestCount() { Count = 5 }, Headers);
        LastSeedRequestTime = DateTime.UtcNow;
        return  reply;
    }
    
}