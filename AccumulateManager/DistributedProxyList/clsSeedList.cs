using DnsClient;
using Google.Protobuf.WellKnownTypes;
using Grpc.Net.Client;
using Proto.API;
using Proto.Authentication;
using Serilog;

namespace AccumulateManager;

static public class clsSeedList
{
    
    static public List<Uri> SeedList = new List<Uri>();

    
    //Read any seeds provided on startup args & call GetSeedsFromNetworkURL()
    static public void BuildSeedList(string[] urls)
    {
        foreach (var argItem in urls)
        {
            foreach (var arg in argItem.Split(new[] { ' ', ',', ';' },
                         StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries))
            {
                try
                {
                    Uri? uri;
                    if (Uri.TryCreate((!arg.StartsWith("http")) ? $"https://{arg}" : arg, UriKind.Absolute, out uri))
                    {
                        var builder = new UriBuilder(uri);
                        builder.Port = (int)Program.Settings.P2PPort;
                        SeedList.Add(builder.Uri);
                    }
                    else
                    {
                        Log.Error($"Unknown seed URL:{arg}");
                    }

                }
                catch (Exception e)
                {
                    Log.Error($"Seed URL Error: {arg} {e.Message}");
                    throw;
                }
            }
        }

        if (SeedList.Count == 0) GetSeedsFromNetworkURL();
    }

    //Query Network DNS A Records for base URL
    static public void GetSeedsFromNetworkURL()
    {
        var lookup = new LookupClient();
        Uri? uri;
        var baseURL = clsAccNetworkMap.NetworkInstalled?.BaseURL;
        if (!String.IsNullOrEmpty(baseURL))
        {
            if (Uri.TryCreate((!baseURL.StartsWith("http")) ? $"https://{baseURL}:{Program.Settings.P2PPort}" : baseURL, UriKind.Absolute, out uri))
            {
                if (uri.HostNameType == UriHostNameType.Dns)
                {
                    var result = lookup.QueryAsync(uri.DnsSafeHost, QueryType.A).Result;
                    foreach (var records in result.Answers.ARecords().Where(x=>x.Address.ToString() != Program.PublicIPV4))
                    {
                        if (Uri.TryCreate($"https://{records.Address}:16666", UriKind.Absolute, out uri))
                        {
                            SeedList.Add(uri);
                        }
                    }
                }
                else
                {
                    SeedList.Add(uri);
                }
            }
        }
    }

    
    static public async Task TryToConnect(CancellationToken CancelToken)
    {
        try
        {
            var options = new ParallelOptions { MaxDegreeOfParallelism = 3, CancellationToken = CancelToken };
            await Parallel.ForEachAsync(SeedList, options, async (seedUri, token) =>
            {
                try
                {
                    var newNode = new AccManNode {};
                    var newProxNode = new clsAccManNode(newNode);

                    if (await newProxNode.NodeLogin(seedUri))
                    {
                        Program.DistributedProxyList.Add(newProxNode);
                    }
                }
                catch (Exception ex)
                {
                    Log.Error($"TryToConnect: {seedUri.Host} error {ex.GetType()} ");
                }
            });
        }
        catch (Exception ex)
        {
            Log.Error($"TryToConnect: error {ex.Message} ");
        }
    }

}
