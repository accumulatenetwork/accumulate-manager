using System.Net.Sockets;
using AccumulateManagerCommon;
using System.Linq;
using Google.Protobuf;
using Google.Protobuf.Collections;
using Grpc.Net.Client;
using Microsoft.Extensions.Options;
using Proto.API;
using Serilog;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Proto.Authentication;
using ProtoHelper;

namespace AccumulateManager;

public class clsDistributedProxyList
{
    public clsProtoShadowTableIndexed<clsAccManNode, AccManNode, UInt64> NodeList { get; init; }
    public AccManNodeList ProtoNodeList = new AccManNodeList();
  //  private Action<AccManNode, UInt64> IndexSelectorWrite;
  //  private Func<AccManNode, IComparable<UInt64>> IndexSelector;

    public const UInt64 RandomIDMin = 10000;
    public const UInt64 RandomIDMax = UInt64.MaxValue;


    private readonly object _lock = new object();

    public Action<AccManNode, AccManNode>? MapFields { get; init; } = null;

    public clsDistributedProxyList()
    {
        var indexSelector = new Func<AccManNode, IComparable<UInt64>>(x => x.ID); //Index field of our proto message
        var indexSelectorWrite = new Action<AccManNode, UInt64>((x, y) => x.ID = y); //Write action for index field.
        //var repeatedFieldSelector = new Func<AccManNodeList, RepeatedField<AccManNode>>(x => x.IPs);

        NodeList = new clsProtoShadowTableIndexed<clsAccManNode, AccManNode, UInt64>(ProtoNodeList.IPs, indexSelector, indexSelectorWrite);
    }


    public AccManNodeList GetRandomProxies(uint count)
    {
        var proxies = new AccManNodeList();

        var rnd = new Random();
        lock (_lock)
        {
            proxies.IPs.AddRange(ProtoNodeList.IPs.Where(x => x.ID != Program.ThisAccManNode.ID).OrderBy(x => rnd.Next())
                .Take((int)count));
        }

        return proxies;
    }

    public List<clsAccManNode> GetRandomProxiesShadow(uint count, DateTime beforeTime)
    {
        var proxies = new List<clsAccManNode>();
        var rnd = new Random();
        lock (_lock)
        {
            proxies.AddRange(NodeList.Values.Where(x => x.LastSeedRequestTime < beforeTime && x.ID != Program.ThisAccManNode.ID).OrderBy(x => rnd.Next())
                .Take((int)count));
        }

        return proxies;
    }


    public async Task ScanAccManNodes(CancellationToken token)
    {
        try
        {
            if (NodeList.Count == 0)
            {
                Log.Information($"ScanProxy: nothing to scan!");
                return;
            }

            var proxies = GetRandomProxiesShadow(5, DateTime.UtcNow.AddMinutes(-10));

            var options = new ParallelOptions { MaxDegreeOfParallelism = 3, CancellationToken = token };
            Parallel.ForEachAsync(proxies, options, async (proxy, token) =>
            {
                try
                {
                    Log.Information($"ScanProxy: Trying {proxy}");

                    if (!proxy.Authenticated)
                    {
                        if (!await proxy.NodeLogin()) //Failed
                        {
                            if (NodeList.Values.Count(x => x.Authenticated) > 5)
                                NodeList.Remove(proxy.ID); //If we have sufficient authenticated nodes, then delete.
                        }
                    }

                    if (proxy.uri != null)
                    {
                        var refreshList = await proxy.RefreshNodeList();
                        AddSeeds(refreshList);
                        Log.Information($"ScanProxy: {proxy} returned {refreshList.IPs.Count} items");
                    }
                    else
                    {
                        Log.Information($"ScanProxy: Remote proxy has no connections. {proxy}");
                    }
                }
                catch (Exception ex)
                {
                    Log.Information($"ScanProxy: {proxy} failed {ex.GetType().Name}");
                }
            });
        }
        catch (Exception ex)
        {
            Log.Error($"ScanAccManNodes: {ex.Message}");
        }
    }

    private void AddSeeds(AccManNodeList nodeList)
    {
        lock (_lock)
        {
            foreach (var node in nodeList.IPs)
            {
                clsAccManNode? existingnode;
                if (NodeList.TryGetValue(node.ID, out existingnode))
                {
                    //ToDo update?
                }
                else
                {
                 //   node.LastSeen = null;  //Delete, as we want to now track our own
                    NodeList.Add(node, new clsAccManNode(node));
                }
            }
        }
    }
    
    public void Add(clsAccManNode newNode)
    {
        lock (_lock)
        {
            NodeList.Add(newNode.ProtoMessage, newNode);
            RemoveDupeIP(newNode.ProtoMessage);
        }
    }

    private void RemoveDupeIP(AccManNode newNode)
    {
        foreach (var ip in newNode.UrlP2P)
        {
            var dups = ProtoNodeList.IPs.Where(x => x != newNode && x.UrlP2P.Contains(ip));

            foreach (var node in dups)
            {
                Log.Information($"RemoveDupeIP: {node.ID}");
                NodeList.Remove(node.ID);
            }
        }
    }

    

    /*public void Add(string urls)
    {
        Proto.API.AccManNodeList list = new AccManNodeList();
        foreach (var url in urls.Split(new[] { ' ', ',', ';' }, StringSplitOptions.TrimEntries|StringSplitOptions.RemoveEmptyEntries))
        {
            list.IPs.Add(new AccManNode()
            {
                IPV4 = url,
            });
        }
        Add(list);
    }*/

    public void Dispose()
    {
    }

    public void ConnectFrom(NodeAuthenticationRequest request)
    {
        lock (_lock)
        {
            clsAccManNode? shadowClass;
            if (NodeList.TryGetValue(request.ID,out shadowClass))
            {
                if (shadowClass.ProtoMessage.UrlP2P.Count == 0)  //todo 
                    shadowClass.ProtoMessage.UrlP2P.AddRange(request.URL);
                shadowClass.ConnectionFromTime = DateTime.UtcNow;
                shadowClass.ProtoMessage.LastSeen = DateTime.UtcNow.ToTimestamp();
            }
            else
            {
                var node = new AccManNode { ID = request.ID, LastSeen = DateTime.UtcNow.ToTimestamp() };
                node.UrlP2P.AddRange(request.URL);
                shadowClass = new clsAccManNode(node)
                {
                    ConnectionFromTime = DateTime.UtcNow
                };
                
                NodeList.Add(node, shadowClass);
                RemoveDupeIP(node);
            }
        }
    }

    public IEnumerable<AccumulateNode> GetRandomAccumulateNodes(string NetworkID, int count)
    {
        var nodeList = new List<AccumulateNode>();

        var rnd = new Random();
        lock (_lock)
        {
            return ProtoNodeList.IPs.Where(x => x.ID != Program.ThisAccManNode.ID && x.AccumulateNodes.Any())
                .SelectMany(node => node.AccumulateNodes).Where(x => x.NetworkName == NetworkID)
                .OrderBy(x => rnd.Next())
                .Take((int)count);
        }
    }
}