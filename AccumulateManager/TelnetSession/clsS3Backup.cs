using System.IO.Compression;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using ConsoleServer;
using ConsoleServerDocker;
using Docker.DotNet.Models;
using ICSharpCode.SharpZipLib.Tar;
using Spectre.Console;

namespace AccumulateManager.TelnetConsole;

public class clsS3Backup
{

    private AnsiTelnetConsole AnsiConsole;
    private clsTelnetAppSession Root;


    public clsS3Backup(clsTelnetAppSession root)
    {
        Root = root;
        AnsiConsole = root.AnsiConsole;
    }

    public void S3Backup()
    {

        var backupFile1 = Path.Combine(Program.DataPath, clsDockerAccumulate._backupFile);
        bool exit = false;

        do
        {
            var status = clsAccNetworkMap.GetStatus().Result;
            var menulist = new List<SelectionFunction<SelectEnum>>();

            menulist.Add(new SelectionFunction<SelectEnum>(Root.MenuActiveMarkup("Backup", status == clsAccNetworkMap.Status.Stopped && clsDockerImageAccBackup.isAWSKeysSet()), () =>
            {
                try
                {
                    if (status != clsAccNetworkMap.Status.Stopped || !clsDockerImageAccBackup.isAWSKeysSet()) return SelectEnum.Selection;

                    var volumeResponse = clsAccNetworkMap.NetworkInstalled?.GetVolumeResponse().Result;
                    var docker = new clsDockerImageAccBackup(AnsiConsole);
                    var dockerContainer = docker.Create(new[] { "/start.sh","backup"}, volumeResponse.Name);

                    Thread.Sleep(1000);
                    Program.DockerManager.GetClient().Containers.StartContainerAsync(dockerContainer.ID, new ContainerStartParameters()).Wait();
                    Program.DockerManager.AttatchToLogOutput(dockerContainer.ID, AnsiConsole);
                }
                catch (Exception e)
                {
                    AnsiConsole.WriteException(e);
                }

                Root.End();

                return SelectEnum.Selection;
            }));

            menulist.Add(new SelectionFunction<SelectEnum>(Root.MenuActiveMarkup("Restore", status == clsAccNetworkMap.Status.Stopped && clsDockerImageAccBackup.isAWSKeysSet()), () =>
            {

                var versionID = AnsiConsole.Prompt(
                    new TextPrompt<string>(" [green]Enter optional version ID[/]")
                        .AllowEmpty() );

                try
                {
                    if (status != clsAccNetworkMap.Status.Stopped || !clsDockerImageAccBackup.isAWSKeysSet()) return SelectEnum.Selection;

                    var volumeResponse = clsAccNetworkMap.NetworkInstalled?.GetVolumeResponse().Result;
                    var docker = new clsDockerImageAccBackup(AnsiConsole);
                    var dockerContainer = docker.Create(new[] { "/start.sh","restore",versionID}, volumeResponse.Name);

                    Thread.Sleep(1000);
                    Program.DockerManager.GetClient().Containers.StartContainerAsync(dockerContainer.ID, new ContainerStartParameters()).Wait();
                    Program.DockerManager.AttatchToLogOutput(dockerContainer.ID, AnsiConsole);
                }
                catch (Exception e)
                {
                    AnsiConsole.WriteException(e);
                }

                Root.End();

                return SelectEnum.Selection;
            }));

            menulist.Add(new SelectionFunction<SelectEnum>("<- Back", () => SelectEnum.Exit));

            Root.ShowScreenHeader();

            if (!clsDockerImageAccBackup.isAWSKeysSet())
            {
                AnsiConsole.MarkupLine("[yellow]AWS_ACCESS_KEY_ID & AWS_SECRET_ACCESS_KEY environment variables not set[/]");
            }


            var option = AnsiConsole.Prompt(
                new SelectionPrompt<SelectionFunction<SelectEnum>>()
                    .Title("Key Management")
                    .PageSize(10)
                    .MoreChoicesText("[grey](Move up and down to reveal more options)[/]")
                    .AddChoices(menulist));

            switch (option.MenuAction())
            {
                case SelectEnum.Exit:
                    exit = true;
                    break;
            }
        } while (!exit);
    }


}