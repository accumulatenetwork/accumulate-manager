using System.Text.RegularExpressions;
using ConsoleServer;
using ConsoleServerDocker;
using Spectre.Console;

namespace AccumulateManager.TelnetConsole;

public class clsGovernance
{

    private AnsiTelnetConsole AnsiConsole;
    private clsTelnetAppSession Root;
    private Regex regEx_base64 = new Regex("^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/][AQgw]==|[A-Za-z0-9+/]{2}[AEIMQUYcgkosw048]=)?$");
    private Regex regEx_txid = new Regex("[A-Fa-f0-9]{50}");

    public clsGovernance(clsTelnetAppSession root)
    {
        Root = root;
        AnsiConsole = root.AnsiConsole;
    }


    public void Governance()
    {
        bool exit = false;

        do
        {

            var menulist = new List<SelectionFunction<SelectEnum>>();

            menulist.Add(new SelectionFunction<SelectEnum>($"Register validator", () =>
            {
                GovernanceRegister();
                return SelectEnum.Selection;
            }));
            menulist.Add(new SelectionFunction<SelectEnum>("<- Back", () => SelectEnum.Exit));


            Root.ShowScreenHeader();

            var option = AnsiConsole.Prompt(
                new SelectionPrompt<SelectionFunction<SelectEnum>>()
                    .Title("Domain Names")
                    .PageSize(10)
                    .MoreChoicesText("[grey](Move up and down to reveal more options)[/]")
                    .AddChoices(menulist));

            switch (option.MenuAction())
            {
                case SelectEnum.Exit:
                    exit = true;
                    break;
            }
        } while (!exit);
    }

    public void GovernanceRegister()
    {
        bool exit = false;

        do
        {
            var menulist = new List<SelectionFunction<SelectEnum>>();

            menulist.Add(new SelectionFunction<SelectEnum>($"Submit Request", () =>
            {
                var rule = new Rule("[white]Submit Request[/]");
                rule.RuleStyle("blue");
                rule.LeftAligned();
                AnsiConsole.Write(rule);

                string key;
                int bytesWritten;
                do
                {
                    key = AnsiConsole.Prompt(
                        new TextPrompt<string>(" [green]Enter Public Validator Key[/]?")
                            .AllowEmpty());
                    if (String.IsNullOrEmpty(key)) return SelectEnum.Selection;
                    if (key.Contains("exit")) return SelectEnum.Selection;

                } while (!regEx_base64.IsMatch(key));

                var container = Program.DockerManager.GetContainer(clsAccNetworkMap.NetworkInstalled.DockerName).Result;
                if (container != null)
                {
                    using (var conr = new clsDockerAttachToContainer(false,AnsiConsole))
                    {
                        if (conr.AttachToContainer(container.ID, new string[] { "/bin/bash" }))
                        {
                            var task = conr.AttachConsoleOutput(true,3000);
                            conr.AttachConsoleInOut(false);
                            conr.WriteCommand($"accumulate -j operator add dn.acme/operators /node/priv_validator_key.json {key} && accumulate -j validator add dn.acme/validators /node/priv_validator_key.json {key}").Wait();
                            task.Wait();
                        }
                    }
                }
                else
                {
                    AnsiConsole.WriteLine("Not Found");
                }
                Root.End();

                return SelectEnum.Selection;
            }));
            menulist.Add(new SelectionFunction<SelectEnum>($"Approve Request", () =>
            {


                var rule = new Rule("[white]Approve Request[/]");
                rule.RuleStyle("blue");
                rule.LeftAligned();
                AnsiConsole.Write(rule);

                string txid;
                Span<byte> txidBytes = Span<byte>.Empty;
                do
                {
                    txid = AnsiConsole.Prompt(
                        new TextPrompt<string>(" [green]Enter Transaction ID[/]?")
                            .AllowEmpty());
                    if (String.IsNullOrEmpty(txid)) return SelectEnum.Selection;

                } while (!regEx_txid.IsMatch(txid));

                var container = Program.DockerManager.GetContainer(clsAccNetworkMap.NetworkInstalled.DockerName).Result;
                if (container != null)
                {
                    using (var conr = new clsDockerAttachToContainer(false,AnsiConsole))
                    {
                        if (conr.AttachToContainer(container.ID, new string[] { "/bin/bash" }))
                        {
                            var task = conr.AttachConsoleInOut(true,3000);
                            conr.WriteCommand($"accumulate -j tx sign dn.acme/operators /node/priv_validator_key.json {txid} && accumulate tx get {txid} -j").Wait();
                            task.Wait();
                        }
                    }
                }
                else
                {
                    AnsiConsole.WriteLine("Not Found");
                }
                Root.End();

                return SelectEnum.Selection;
            }));

            menulist.Add(new SelectionFunction<SelectEnum>("< Back", () => SelectEnum.Exit));

            Root.ShowScreenHeader();


            var option = AnsiConsole.Prompt(
                new SelectionPrompt<SelectionFunction<SelectEnum>>()
                    .Title("Governance")
                    .PageSize(10)
                    .MoreChoicesText("[grey](Move up and down to reveal more options)[/]")
                    .AddChoices(menulist));

            switch (option.MenuAction())
            {
                case SelectEnum.Exit:
                    exit = true;
                    break;
            }
        } while (!exit);
    }
}