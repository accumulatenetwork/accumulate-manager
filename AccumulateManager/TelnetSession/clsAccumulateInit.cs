
using System.Net;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;
using ConsoleServer;
using Docker.DotNet;
using ProtoHelper;
using SharpYaml.Tokens;
using Spectre.Console;

namespace AccumulateManager.TelnetConsole;

public class clsAccumulateInit
{
    private AnsiTelnetConsole AnsiConsole;
    private AccNetwork _selectedAccNetwork;
    private clsTelnetAppSession Root;
    private Regex DomainRegEx = new Regex(@"(?:[A-Za-z0-9][A-Za-z0-9\-]{0,61}[A-Za-z0-9]|[A-Za-z0-9])");
    
    public clsAccumulateInit(clsTelnetAppSession root)
    {
        Root = root;
        AnsiConsole = root.AnsiConsole;
    }
    
    public void Run()
    {
        clsAccNetworkMap.UpdateNetworkMap().Wait();
        
        //AnsiConsole.WriteLine("");
        
        var list = new List<SelectionFunction<SelectEnum>>();
        list.Add(new SelectionFunction<SelectEnum>("Join Accumulate Network", () =>
        {
            var select = new clsAccumulateSelect(AnsiConsole);
            if (select.SelectNetwork().Result)
            {
                var node = new AccNetworkInstalled
                {
                    NetworkName = select.SelectedAccNetwork.NetworkName,
                    Tag = select.SelectedTag,
                    Subnet = select.SelectedSubnet?.Subnet ?? "",
                    Genisis = select.SelectedSubnet?.genisis ?? "",
                    BaseURL = select.SelectedAccNetwork.BaseURL
                };

                var volume = Program.DockerManager.GetVolumeResponse(node.DockerName).Result;
                if (volume is not null)
                {
                    AnsiConsole.WriteLine();
                    var option2 = AnsiConsole.Prompt(new SelectionPrompt<string>()
                        .Title("[red]Volume already exists[/]")
                        .HighlightStyle(Style.Parse("yellow"))
                        .PageSize(3)
                        .AddChoices(new[] {
                            "Delete",
                            "Exit"
                        }));
                    if (option2 == "Exit") return SelectEnum.Exit;
                    Program.DockerManager.DeleteContainerIfExist(node.DockerName).Wait();
                    Program.DockerManager.DeleteVolume(node.DockerName).Wait();
                }

                clsAccNetworkMap.NetworkInstalled = node;
                var acc = new clsDockerAccumulate(AnsiConsole);

                if (node.Tag == "http")
                {
                    var key = AnsiConsole.Ask<string>("Paste in your Key seed:");

                    if (!String.IsNullOrEmpty(key))
                    {
                        node.Genisis = key;
                        if (acc.RunHttp(node).Result)
                        {
                            clsAccNetworkMap.UpdateNetworkMap();
                        }
                        else
                        {
                            clsAccNetworkMap.NetworkInstalled = null;
                        }
                    }
                }
                else if ( acc.RunInit(node).Result)
                {
                    clsAccNetworkMap.UpdateNetworkMap();
                }
                Thread.Sleep(500);
                End();
                var acc2 = new clsAccumulate(Root);
                acc2.Run(false);
            }

            return SelectEnum.Selection;
        }));


        /*
        var networkJsonFile = Path.Combine(Program.DataPath, "network.json");

        list.Add(new SelectionFunction<SelectEnum>("Create new network", () =>
        {
            if (File.Exists(networkJsonFile))
            {
                AnsiConsole.MarkupLine("network.json not found.  Copy to the ~/accman directory");
                Root.End();
            }

            var newNetwork = JsonSerializer.Deserialize<clsNewNetworkJSON>(File.ReadAllText(networkJsonFile));

            var nomatch = new List<string>();
            clsNewNetworkJSON.Node selectednode = null;
            foreach (var newNetworkBvn in newNetwork.bvns)
            {
                foreach (var node in newNetworkBvn.nodes)
                {
                    var peerAddress = Dns.GetHostAddresses(node.peerAddress);
                    if (!nomatch.Contains(node.peerAddress))
                    {
                        if (peerAddress.Any(x => x.ToString() == Program.PublicIPV4))
                        {
                            selectednode = node;
                            break;
                        }
                        else
                        {
                            nomatch.Add(node.peerAddress);
                        }
                    }
                }
            }

            if (selectednode != null)
            {

                AnsiConsole.MarkupLine($"Node found:");
                AnsiConsole.Write(selectednode.ToClassTree());
                if (!Root.YesNo("Continue?")) return SelectEnum.Selection;

            }
            else
            {
                AnsiConsole.MarkupLine($"[red]Domains don't match:[/]");
                foreach (var nodePeer in nomatch)
                {
                    AnsiConsole.MarkupLine($"[yellow]{nodePeer}:[/]");
                }
                Root.End();
            }

            var acc = new clsDockerAccumulate(AnsiConsole);
            acc.RunNewNetwork(selectednode);



            return SelectEnum.Selection;
        }));
        */



        /*
        list.Add(new SelectionFunction<SelectEnum>("Create new network", () =>
        {

            string baseURL = "";
            bool valid;
            do
            {
                var rule = new Rule("[white]Create new network[/]");
                rule.RuleStyle("blue");
                rule.LeftAligned();
                AnsiConsole.Write(rule);

                baseURL = AnsiConsole.Prompt(
                    new TextPrompt<string>(" [green]Enter base network domain name (myaccumulate.com):[/]")
                        .AllowEmpty() );
                if (String.IsNullOrEmpty(baseURL)) return SelectEnum.Exit;

                valid = DomainRegEx.Match(baseURL).Success;

            } while (!valid);


            var bvns= GetBVNs(baseURL);
            if (bvns.Count == 0)
            {
                AnsiConsole.MarkupLine($"[red]BVN domains not found.  Expecting:[/]");
                AnsiConsole.MarkupLine($"  [white]bvn0.{baseURL}[/]");
                AnsiConsole.MarkupLine($"  [white]bvn0-seed.{baseURL}[/]");
                AnsiConsole.MarkupLine($"  [white]bvn1.{baseURL}[/]");
                AnsiConsole.MarkupLine($"  [white]bvn1-seed.{baseURL}[/]");
                AnsiConsole.MarkupLine($"  [white]etc....[/]");
                Root.End();
                return SelectEnum.Exit;

            }

            int bvnFound = -1;
            var jsonFile = new NewNetworkJSON() { id = baseURL, };
            jsonFile.bvns = new List<NewNetworkJSON.Bvn>();

            for (int i =0 ; i < bvns.Count;i++)
            {
                AnsiConsole.Markup($"[white]bvn{i}.{baseURL}[/]   ");
                if (bvns[i].Any(x => x.ToString() == Program.PublicIPV4))
                {
                    AnsiConsole.Markup($"   [white]IPV4[/]");
                    bvnFound = i;
                }
                if (bvns[i].Any(x => x.ToString() == Program.PublicIPV6))
                {
                    AnsiConsole.Markup($"   [white]IPV6[/]");
                    bvnFound = i;

                }
                AnsiConsole.MarkupLine("");

                jsonFile.bvns.Add(new NewNetworkJSON.Bvn()
                {
                    id = $"BVN{i}",
                    nodes = new List<NewNetworkJSON.Node>()
                    {
                        new NewNetworkJSON.Node()
                        {
                            hostName = $"bvn{i}.{baseURL}",
                            basePort = 16591,
                            dnnType = "validator",
                            bvnnType = "validator"
                        }
                    }
                });

            }

            if (bvnFound == -1)
            {
                AnsiConsole.MarkupLine($"[red]No matching IP[/]");
                Root.End();
                return SelectEnum.Exit;
            }



            var node = new AccNetworkInstalled
            {
                NetworkName = baseURL,
                Tag = "develop",
                Subnet = $"bvns{bvnFound}.{baseURL}",
                BaseURL = baseURL
            };

            var volume = Program.DockerManager.GetVolumeResponse(node.DockerName).Result;
            if (volume is not null)
            {
                AnsiConsole.WriteLine();
                var option2 = AnsiConsole.Prompt(new SelectionPrompt<string>()
                    .Title("[red]Volume already exists[/]")
                    .HighlightStyle(Style.Parse("yellow"))
                    .PageSize(3)
                    .AddChoices(new[] {
                        "Delete",
                        "Exit"
                    }));
                if (option2 == "Exit") return SelectEnum.Exit;
                Program.DockerManager.DeleteContainerIfExist(node.DockerName).Wait();
                Program.DockerManager.DeleteVolume(node.DockerName).Wait();
            }

            var acc = new clsDockerAccumulate(AnsiConsole);
            if (acc.RunNewNetwork(node).Result)
            {
                clsAccNetworkMap.NetworkInstalled = node;
                clsAccNetworkMap.UpdateNetworkMap();
            }
            End();
            var acc2 = new clsAccumulate(Root);
            acc2.Run(false);

            return SelectEnum.Selection;
        }));
        */

        list.Add(new SelectionFunction<SelectEnum>("<cancel>", () =>
        {
            return SelectEnum.Exit;
        }));

        var option = AnsiConsole.Prompt(
            new SelectionPrompt<SelectionFunction<SelectEnum>>()
                //.Title("Select network you wish to join.")
                .PageSize(20)
                .MoreChoicesText("[grey](Move up and down to reveal more options)[/]")
                .AddChoices(list));

        switch (option.MenuAction())
        {
            case SelectEnum.Selection:
                

                break;
            case SelectEnum.Exit:
                return;

        }
    }

    List<IPAddress[]> GetBVNs(string domain)
    {
        var list = new List<IPAddress[]>();
        for (int i = 0; i < 10; i++)
        {
            var bvns = GetIPs($"bvn{i}.{domain}");
            if (bvns.Length == 0) break;
            var bvnseeds = GetIPs($"bvn{i}-seed.{domain}");
            if (bvnseeds.Length == 0) break;
            list.Add(bvnseeds);
        }

        return list;
    }

    IPAddress[] GetIPs(string domain)
    {
        try
        {
            return Dns.GetHostAddresses(domain);
        }
        catch (Exception e)
        {
            return Array.Empty<IPAddress>();
        }
    }


    void End()
    {
        var rule = new Rule($"[white]end[/]");
        rule.RuleStyle("blue");
        rule.LeftAligned();
        AnsiConsole.Write(rule);
        
        AnsiConsole.Prompt(
            new SelectionPrompt<string>()
                .Title("")
                .HighlightStyle(Style.Parse("yellow"))
                .PageSize(3)
                .AddChoices(new[] {
                    "Press Return"
                }));
    }



}