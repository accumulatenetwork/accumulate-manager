
using ConsoleServer;
using Spectre.Console;

namespace AccumulateManager.TelnetConsole;

public class clsAccumulateSelect
{
    private AnsiTelnetConsole AnsiConsole;
    public AccNetwork SelectedAccNetwork { get; private set; }
    public AccNetwork.SubnetInfo SelectedSubnet { get; private set; }
    public string SelectedTag { get; private set; }
    
    
    public clsAccumulateSelect(AnsiTelnetConsole ansiTelnetConsole)
    {
        AnsiConsole = ansiTelnetConsole;
        clsAccNetworkMap.UpdateNetworkMap();
    }
    
    public async Task<bool> SelectNetwork()
    {
        if (clsAccNetworkMap.Networks == null || clsAccNetworkMap.Networks?.Count == 0 )
        {
            AnsiConsole.WriteLine("Not Found");
            Thread.Sleep(2000);
            return false;
        }
        
        var list = new List<SelectionItemT<AccNetwork>>();
        foreach (var accNetwork in clsAccNetworkMap.Networks)
        {
            list.Add(new SelectionItemT<AccNetwork>(accNetwork.NetworkName,SelectEnum.Selection,accNetwork));
        }

        list.Add(new SelectionItemT<AccNetwork>("<cancel>", SelectEnum.Exit,null));

        var backupInfo = clsDockerAccumulate.ReadBackupKeyInfo();
        if (!String.IsNullOrEmpty(backupInfo))
        {
            AnsiConsole.MarkupLine($"[white]Backup found[/]");
            AnsiConsole.MarkupLine($"[yellow]{backupInfo}[/]");
        }

        var option = AnsiConsole.Prompt(
            new SelectionPrompt<SelectionItemT<AccNetwork>>()
                .Title("[cyan]Select network you wish to join.[/]")
                .PageSize(10)
                .MoreChoicesText("[grey](Move up and down to reveal more options)[/]")
                .AddChoices(list));

        switch (option.MenuAction)
        {
            case SelectEnum.Selection:
                SelectedAccNetwork = option.Item;
                return await SelectSubnet();
            case SelectEnum.Exit:
                return false;
        }
        return false;
    }

    private async Task<bool> SelectSubnet()
    {

        if (SelectedAccNetwork == null)
        {
            AnsiConsole.MarkupLine("[red]Unable to find network map[/]");
            Thread.Sleep(2000);
            return false;
        }
        
        var list = new List<SelectionItemT<AccNetwork.SubnetInfo>>();

        list.Add(new SelectionItemT<AccNetwork.SubnetInfo>("[yellow]Auto <- Please select Auto (unless you have a good reason not to).[/]", SelectEnum.Auto,null));
        foreach (var accSubNet in SelectedAccNetwork.SubNets.Where(x=>x.Subnet!="DN"))
        {
            list.Add(new SelectionItemT<AccNetwork.SubnetInfo>(accSubNet.Subnet,SelectEnum.Selection,accSubNet));
        }
        list.Add(new SelectionItemT<AccNetwork.SubnetInfo>("API Node", SelectEnum.API,null));
        list.Add(new SelectionItemT<AccNetwork.SubnetInfo>("<cancel>", SelectEnum.Exit,null));

        var option = AnsiConsole.Prompt(
            new SelectionPrompt<SelectionItemT<AccNetwork.SubnetInfo>>()
                .Title("[cyan]Select network subnet you wish to join.[/]")
                .PageSize(10)
                .MoreChoicesText("[grey](Move up and down to reveal more options)[/]")
                .AddChoices(list));

        switch (option.MenuAction)
        {
            case SelectEnum.Selection:
                SelectedTag = await SelectVersionTag(SelectedAccNetwork);
                SelectedSubnet = option.Item;
                break;
            case SelectEnum.API:
                SelectedTag = "http";
                SelectedSubnet = new AccNetwork.SubnetInfo("http", "");
                break;
            case SelectEnum.Auto:
                var rand = new Random();
                var subbvn = SelectedAccNetwork.SubNets.Where(x=>x.Subnet!="DN").ToArray();
                var sub = subbvn[rand.Next(subbvn.Length)];
                SelectedTag = await SelectVersionTag(SelectedAccNetwork);
                SelectedSubnet = sub;
                break;
            case SelectEnum.Exit:
                return false;
        }

        return (SelectedTag is not null);
    }

    
    public async Task<string?> SelectVersionTag(AccNetwork accNetwork)
    {
        var list = new List<SelectionItemT<string>>();
        var tags = accNetwork.Tags.Split(new[] { ' ', ',', ';' }, StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries).ToList();

        if (Program.DevMode && !String.IsNullOrEmpty(accNetwork.DevTags))
        {
            var devtags = accNetwork.DevTags.Split(new[] { ' ', ',', ';' }, StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries).ToList();
            tags.AddRange(devtags);
        }

            foreach (var tag in tags.Distinct())
            {
                if (tag.Contains('*') || tag.Contains('?'))
                {
                    var gitList = await clsAccNetworkMap.GetDockerVersions(tag);
                    foreach (var version in gitList)
                    {
                        list.Add(new SelectionItemT<string>(version, SelectEnum.Selection, version));
                    }
                }
                else
                {
                    list.Add(new SelectionItemT<string>(tag, SelectEnum.Selection, tag));    
                }
            }

            list.Add(new SelectionItemT<string>("<- back", SelectEnum.Exit, null));

            do
            {
                var option = AnsiConsole.Prompt(
                    new SelectionPrompt<SelectionItemT<string>>()
                        .Title("[cyan]Select version.[/]")
                        .PageSize(20)
                        .MoreChoicesText("[grey](Move up and down to reveal more options)[/]")
                        .AddChoices(list.Distinct()));

                switch (option.MenuAction)
                {
                    case SelectEnum.Selection:
                        return option.Item;
                    case SelectEnum.Exit:
                        return null;
                }
            } while (true);
            
    }


    /*private async Task<string?> SelectGitLabVersion(string? wildcard = null)
    {
        var versions = await clsAccNetworkMap.GetDockerVersions(wildcard);

        var list = new List<SelectionItemT<string>>();
        foreach (var tag in versions)
        {
            list.Add(new SelectionItemT<string>(tag.name,SelectEnum.Selection,tag.name));
        }

        list.Add(new SelectionItemT<string>("<- back", SelectEnum.Exit,null));

        var option = AnsiConsole.Prompt(
            new SelectionPrompt<SelectionItemT<string>>()
                .Title("Select network you wish to join.")
                .PageSize(30)
                .MoreChoicesText("[grey](Move up and down to reveal more options)[/]")
                .AddChoices(list));

        return option.MenuAction == SelectEnum.Selection ? option.Item : null;
    }*/
}