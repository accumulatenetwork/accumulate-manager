using System.IO.Compression;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using ConsoleServer;
using Docker.DotNet.Models;
using Spectre.Console;

namespace AccumulateManager.TelnetConsole;

public class clsKeyManagement
{

    private AnsiTelnetConsole AnsiConsole;
    private clsTelnetAppSession Root;

    //private Regex regEx_base64 = new Regex("^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/][AQgw]==|[A-Za-z0-9+/]{2}[AEIMQUYcgkosw048]=)?$");
    //private Regex regEx_txid = new Regex("[A-Fa-f0-9]{50}");

    public clsKeyManagement(clsTelnetAppSession root)
    {
        Root = root;
        AnsiConsole = root.AnsiConsole;
    }

    public void KeyManagement()
    {

        var backupFile1 = Path.Combine(Program.DataPath, clsDockerAccumulate._backupFile);
        bool exit = false;

        do
        {
            String backupInfo = null;
            var backupExists = File.Exists(backupFile1);
            if (backupExists) backupInfo = clsDockerAccumulate.ReadBackupKeyInfo();
            var status = clsAccNetworkMap.GetStatus().Result;

            var menulist = new List<SelectionFunction<SelectEnum>>();

            menulist.Add(new SelectionFunction<SelectEnum>(Root.MenuActiveMarkup("Backup Key to AccMan",!backupExists && status == clsAccNetworkMap.Status.Running), () =>
            {
                if (backupExists || status != clsAccNetworkMap.Status.Running) return SelectEnum.Selection;
                AnsiConsole.Clear();
                AnsiConsole.MarkupLine("[yellow]Encrypt backup[/]");
                var textprompt = new TextPrompt<string>("[green]Enter new password[/]:").AllowEmpty();
                textprompt.IsSecret = true;
                var pw1 = AnsiConsole.Prompt(textprompt);
                if (String.IsNullOrEmpty(pw1)) return SelectEnum.Selection;

                textprompt = new TextPrompt<string>("[green]Re-enter Password[/]:").AllowEmpty();
                textprompt.IsSecret = true;
                var pw2 = AnsiConsole.Prompt(textprompt);

                if (pw1 != pw2)
                {
                    AnsiConsole.MarkupLine("[red]Passwords dont match[/]");
                    Root.End();
                    return SelectEnum.Selection;
                }

                if (clsDockerAccumulate.BackupKey(pw1))
                {
                    AnsiConsole.MarkupLine("[green]Backup Complete[/]");
                }
                else
                {
                    AnsiConsole.MarkupLine("[red]Backup failed[/]");
                }
                Root.End();

                return SelectEnum.Selection;
            }));


            menulist.Add(new SelectionFunction<SelectEnum>(Root.MenuActiveMarkup("Delete Backup Key",backupExists), () =>
            {
                if (!backupExists) return SelectEnum.Selection;

                if (Root.AreYouSure("Delete Backup Key?"))
                {
                    try
                    {
                        File.Delete(Path.Combine(Program.DataPath, clsDockerAccumulate._backupFile));
                    }
                    catch (Exception e)
                    {
                        AnsiConsole.MarkupLine($"[red]{e.Message}[/]");
                        Root.End();
                    }
                }

                return SelectEnum.Selection;
            }));


            menulist.Add(new SelectionFunction<SelectEnum>("<- Back", () => SelectEnum.Exit));

            Root.ShowScreenHeader();

            if (backupExists)
            {
                    AnsiConsole.MarkupLine($"[green]Backup Found:[/]");
                    AnsiConsole.MarkupLine($"[yellow]{backupInfo ?? ""}[/]");
            }
            else
            {
                AnsiConsole.MarkupLine($"[red]No backup found:[/]");
            }

            var option = AnsiConsole.Prompt(
                new SelectionPrompt<SelectionFunction<SelectEnum>>()
                    .Title("Key Management")
                    .PageSize(10)
                    .MoreChoicesText("[grey](Move up and down to reveal more options)[/]")
                    .AddChoices(menulist));

            switch (option.MenuAction())
            {
                case SelectEnum.Exit:
                    exit = true;
                    break;
            }
        } while (!exit);
    }


}