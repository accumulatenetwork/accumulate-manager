
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Text.Json;
using System.Web;
using ConsoleServer;
using Docker.DotNet;
using Docker.DotNet.Models;
using ConsoleServerDocker;
using ProtoHelper;
using Serilog;
using Spectre.Console;

namespace AccumulateManager.TelnetConsole;

public class clsAccumulate
{
    private AnsiTelnetConsole AnsiConsole;
    private AccNetwork _selectedAccNetwork;
    private clsTelnetAppSession Root;

    static string GitLabApiBaseUrl = "https://gitlab.com/api/v4";
    private static string GitLabScriptPath = "scripts/ops/fix";
    //private static string GitLabScriptPath = "scripts/ci";



    public clsAccumulate(clsTelnetAppSession root)
    {
        Root = root;
        AnsiConsole = root.AnsiConsole;
    }

    public void Run(bool ForceSelectVertion)
    {
        do
        {
            var status = clsAccNetworkMap.GetStatus().Result;

            if (status == clsAccNetworkMap.Status.NotSet) return;

            var list = new List<SelectionFunction<bool>>();

            if (status == clsAccNetworkMap.Status.Stopped || status == clsAccNetworkMap.Status.Initialised)
            {
                list.Add(new SelectionFunction<bool>(ForceSelectVertion ? "Start":"Start / Update", () =>
                {
                    var status = clsAccNetworkMap.GetStatus().Result;
                    if (status == clsAccNetworkMap.Status.Running) return false;
                    if (status == clsAccNetworkMap.Status.Stopped)
                    {
                        var container = Program.DockerManager.GetContainer(clsAccNetworkMap.NetworkInstalled.DockerName).Result;
                        if (container!=null)Program.DockerManager.Delete(container).Wait();
                    }

                    string? verstionTag;
                    if (clsAccNetworkMap.NetworkInstalled?.Tag == null || (ForceSelectVertion && clsAccNetworkMap.NetworkInstalled?.Tag != "http"))
                    {
                        var select = new clsAccumulateSelect(AnsiConsole);
                        verstionTag = select.SelectVersionTag(clsAccNetworkMap.NetworkInstalled.GetAccNetwork()).Result;
                    }
                    else
                    {
                        verstionTag = clsAccNetworkMap.NetworkInstalled.Tag;
                    }

                    if (verstionTag is not null)
                    {
                        bool changed = false;
                        if (clsAccNetworkMap.NetworkInstalled.Tag != verstionTag)
                        {
                            changed = true;
                            clsAccNetworkMap.NetworkInstalled.Tag = verstionTag;
                        }

                        try
                        {
                            var acc = new clsDockerAccumulate(AnsiConsole);
                            if (clsAccNetworkMap.NetworkInstalled.Tag == "http")
                            {
                                acc.RunHttp(clsAccNetworkMap.NetworkInstalled).Wait();
                            }
                            else
                            {
                                acc.Run(clsAccNetworkMap.NetworkInstalled).Wait();
                            }
                        }
                        catch (Exception ex)
                        {
                            AnsiConsole.WriteException(ex);
                        }


                        if (changed)
                        {
                            clsAccNetworkMap.UpdateNetworkMap();
                            clsAccNetworkMap.SaveNetworkInstalled();
                        }
                        End();
                    }

                    return false;
                }));
            }
            else
            {
                list.Add(new SelectionFunction<bool>("Stop", () =>
                {
                    var status = clsAccNetworkMap.GetStatus().Result;
                    if (status == clsAccNetworkMap.Status.Running)
                    {
                        if (Root.AreYouSure("Stop Node"))
                        {
                            AnsiConsole.MarkupLine("[yellow]Stopping node, please wait....[/]");
                            var container = Program.DockerManager.GetContainer(clsAccNetworkMap.NetworkInstalled.DockerName).Result;
                            if (container != null)
                            {
                                Program.DockerManager.StopContainer(container.ID);
                                Thread.Sleep(2000);
                            }
                        }
                    }
                    return false;
                }));
            }

            list.Add(new SelectionFunction<bool>("Display Status", () =>
            {
                AccumulateAPI_Status status1;
                AccumulateAPI2_Status status2;

                using (var httpClient = new HttpClient())
                {
                    status1 = httpClient.GetFromJsonAsync<AccumulateAPI_Status>($"http://localhost:16695/status").Result;
                }
                using (var httpClient = new HttpClient())
                {
                    status2 = httpClient.GetFromJsonAsync<AccumulateAPI2_Status>($"http://localhost:16692/status").Result;
                }

                if (status1!=null) AnsiConsole.Write(status1.ToClassTree());
                if (status2 != null)
                {
                    AnsiConsole.Write(status2.result.node_info.ToClassTree());
                    AnsiConsole.Write(status2.result.sync_info.ToClassTree());
                    AnsiConsole.Write(status2.result.validator_info.ToClassTree());
                }

                End();
                return false;
            }));

            list.Add(new SelectionFunction<bool>("Attach to Log Output", () =>
            {
                var parameters = new ContainerLogsParameters
                {
                    ShowStdout = true,
                    ShowStderr = true,
                    Since = null,
                    Timestamps = true,
                    Follow = true,
                    Tail = "300",
                };
                var container = Program.DockerManager.GetContainer(clsAccNetworkMap.NetworkInstalled.DockerName).Result;
                if (container != null)
                {
                    if (status == clsAccNetworkMap.Status.Running) AnsiConsole.MarkupLine("[yellow]Press ESCAPE to exit[/]");
                    byte[] buffer = new byte [1024];
                    using (var logStream = Program.DockerManager.GetClient()?.Containers?.GetContainerLogsAsync(container.ID, parameters).Result)
                    {
                        using (var keypress = new clsKeyPressed(AnsiConsole,new Action<ConsoleKeyInfoExt>(c =>
                        {
                                c.Cancel = true;
                                if (c.Key == ConsoleKey.Escape ||
                                c.Shift ||
                                c.Control)
                            {
                                logStream.Close();
                            }
                        })))
                        {
                            try
                            {
                                if (logStream != null)
                                {
                                    do
                                    {
                                        var count = logStream.Read(buffer, 0, buffer.Length);
                                        if (count <= 0) break;
                                        AnsiConsole.TelnetSession.SendFixCR(buffer, 0, count);
                                    } while (true);
                                }
                            }
                            catch (Exception e)
                            {
                            }
                        }
                    }
                }
                else
                {
                    AnsiConsole.WriteLine("Not Found");
                }
                if (status != clsAccNetworkMap.Status.Running) End();
                return false;
            }));

            list.Add(new SelectionFunction<bool>("Attach BASH Console", () =>
            {
                var status = clsAccNetworkMap.GetStatus().Result;

                if (status == clsAccNetworkMap.Status.Stopped || status == clsAccNetworkMap.Status.Initialised)
                {
                    var acc = new clsDockerAccumulate(AnsiConsole);
                    acc.RunBashConsole(clsAccNetworkMap.NetworkInstalled).Wait();
                    End();
                }
                else if (status == clsAccNetworkMap.Status.Running)
                {
                    var container = Program.DockerManager.GetContainer(clsAccNetworkMap.NetworkInstalled.DockerName).Result;
                    if (container != null)
                    {
                        using (var conr = new clsDockerAttachToContainer(false,AnsiConsole))
                        {
                            conr.AttachToContainer(container.ID, new[] { "/bin/bash" });
                            conr.AttachConsoleInOut(false).Wait();
                        }
                        End();
                    }
                }
                return false;
            }));

            list.Add(new SelectionFunction<bool>(status == clsAccNetworkMap.Status.Running ? "[grey]Execute Maintenance Script[/]":"Execute Maintenance Script", () =>
            {
                var status = clsAccNetworkMap.GetStatus().Result;

                if (status == clsAccNetworkMap.Status.Stopped || status == clsAccNetworkMap.Status.Initialised)
                {
                    var scripts = GetGitLabFiles().Result;
                    var listFile = new List<SelectionFunction<bool>>();

                    foreach (var scriptItem in scripts)
                    {
                        listFile.Add(new SelectionFunction<bool>(scriptItem, () =>
                        {

                            var encodedFilePath = HttpUtility.UrlEncode($"{GitLabScriptPath}/{scriptItem}");
                            var fileUrl = $"{GitLabApiBaseUrl}/projects/29762666/repository/files/{encodedFilePath}/raw";

                            // Parse the image line
                            string? image = null, tag = null;
                            using (var client = new HttpClient())
                            using (var stream = client.GetStreamAsync(fileUrl).Result)
                            using (var reader = new StreamReader(stream))
                            {
                                var line1 = reader.ReadLine();
                                var line2 = reader.ReadLine();
                                if (line1 != null && line2 != null && line1.StartsWith("#!") && line2.StartsWith("# image: ")) {
                                    var parts = line2.Substring(8).Trim().Split(":");
                                    switch (parts.Length) {
                                        case 2:
                                            tag = parts[1];
                                            goto case 1; // fallthrough
                                        case 1:
                                            image = parts[0];
                                            break;
                                    }
                                }
                            }

                            var acc = new clsDockerAccumulate(AnsiConsole);
                            acc.RunBashConsole(clsAccNetworkMap.NetworkInstalled,new []{"cd /node",$"wget --output-document {scriptItem} {fileUrl}",$"chmod +x {scriptItem}",$"./{scriptItem}"}, image, tag).Wait();

                            End();
                            return false;
                        }));
                    }
                    listFile.Add(new SelectionFunction<bool>("<- Back", () => { return false; }));

                    Root.ShowScreenHeader(status);
                    var option = AnsiConsole.Prompt(
                        new SelectionPrompt<SelectionFunction<bool>>()
                            .Title("[cyan]Maintenance Scripts:[/]")
                            .PageSize(20)
                            .MoreChoicesText("[grey](Move up and down to reveal more options)[/]")
                            .AddChoices(listFile));
                    try
                    {
                        if (option.MenuAction.Invoke()) return true;
                    }
                    catch (Exception ex)
                    {
                        AnsiConsole.WriteException(ex);
                        End();
                    }

                }
                /*else if (status == clsAccNetworkMap.Status.Running)
                {
                    var container = Program.DockerManager.GetContainer(clsAccNetworkMap.NetworkInstalled.DockerName).Result;
                    if (container != null)
                    {
                        using (var conr = new clsDockerAttachToContainer(false,AnsiConsole))
                        {
                            conr.AttachToContainer(container.ID, new[] { "/bin/bash" });
                            conr.AttachConsoleInOut(false).Wait();
                        }
                        End();
                    }
                }*/
                return false;
            }));


            list.Add(new SelectionFunction<bool>("Display Registration Info", () =>
            {
                var status = clsAccNetworkMap.GetStatus().Result;

                if (status == clsAccNetworkMap.Status.Stopped || status == clsAccNetworkMap.Status.Initialised)
                {
                    var acc = new clsDockerAccumulate(AnsiConsole);
                    acc.RunBashConsole(clsAccNetworkMap.NetworkInstalled, new String[] { "cat /node/register.json", "exit" });
                    End();
                }
                else if (status == clsAccNetworkMap.Status.Running)
                {
                    var container = Program.DockerManager.GetContainer(clsAccNetworkMap.NetworkInstalled.DockerName).Result;
                    if (container != null)
                    {
                        using (var conr = new clsDockerAttachToContainer(false, AnsiConsole))
                        {
                            if (conr.AttachToContainer(container.ID, new string[] { "/bin/bash" }))
                            {
                                var readTask = conr.AttachConsoleOutput(true, 2000);
                                conr.WriteCommand("cat /node/register.json").Wait();
                                conr.WriteCommand("exit").Wait();

                                readTask.Wait(50000);
                            }
                        }
                    }
                    else
                    {
                        AnsiConsole.WriteLine("Not Found");
                    }

                    End();
                }
                    return false;
                }));

            list.Add(new SelectionFunction<bool>("Key Management", () =>
            {
                new clsKeyManagement(Root).KeyManagement();
                return false;
            }));

            /*list.Add(new SelectionFunction<bool>(Root.MenuActiveMarkup("S3 Backup", status == clsAccNetworkMap.Status.Running), () =>
            {
                new clsS3Backup(Root).S3Backup();
                return false;
            }));*/

            list.Add(new SelectionFunction<bool>("View Disk Usage", () =>
            {
                using (var container = new clsDockerRunOnHost(AnsiConsole,Program.DockerManager.GetClient()))
                {
                    if (container.AttatchToHost(new string[] { "/bin/bash" }))
                    {
                        var startReaderTask = container.StartReaderTask();
                        container.WriteCommand("docker system df");
                        container.WriteCommand("df -h --total -x tmpfs");
                        container.WriteCommand("exit");

                        startReaderTask.Wait(TimeSpan.FromSeconds(10));
                    }
                }
                End();
                return false;
            }));

            if (status != clsAccNetworkMap.Status.Running)
            {

                list.Add(new SelectionFunction<bool>("S3 Backup", () =>
                {
                    new clsS3Backup(Root).S3Backup();
                    return true;
                }));


                list.Add(new SelectionFunction<bool>("Clean/Delete", () =>
                {
                    AnsiConsole.MarkupLine("Type [yellow]CLEAN[/] to delete just the data set");
                    AnsiConsole.MarkupLine("Type [red]DELETE[/] to delete node & it's configuration");

                    var reply = AnsiConsole.Ask<string>("Type in CLEAN or DELETE or anything else to exit");
                    try
                    {
                        if (reply == "DELETE")
                        {
                            Program.DockerManager.DeleteContainerIfExist(clsAccNetworkMap.NetworkInstalled.DockerName).Wait();
                            Program.DockerManager.DeleteVolume(clsAccNetworkMap.NetworkInstalled.DockerName).Wait();
                            clsAccNetworkMap.NetworkInstalled = null;
                        }
                        else if (reply == "CLEAN")
                        {
                            var acc = new clsDockerAccumulate(AnsiConsole);
                            acc.RunBashConsole(clsAccNetworkMap.NetworkInstalled,new String[] { "rm -rf /node/{d,bv}nn/data/{*.db,cs.wal}", "exit" }).Wait();
                            End();
                        }
                        else
                        {
                            AnsiConsole.MarkupLine("aborted");
                            End();
                        }
                        //   var fullURL = $"{clsAccNetworkMap.NetworkInstalled.Subnet.ToLower()}.{clsAccNetworkMap.NetworkInstalled.BaseURL.ToLower()}";
                     //   if (Program.Settings.DomainNames.Contains(fullURL)) Program.Settings.DomainNames.Remove(fullURL);
                      //  if (Program.Settings.DomainNames.Contains(clsAccNetworkMap.NetworkInstalled.BaseURL)) Program.Settings.DomainNames.Remove(clsAccNetworkMap.NetworkInstalled.BaseURL);

                    }
                    catch (Exception e)
                    {
                        AnsiConsole.WriteException(e);
                        End();
                    }

                    return true;
                }));
            }

            list.Add(new SelectionFunction<bool>("<- Back", () => { return true; }));

            Root.ShowScreenHeader(status);
            var option = AnsiConsole.Prompt(
                new SelectionPrompt<SelectionFunction<bool>>()
                    .Title("[cyan]Accumulate Node options:[/]")
                    .PageSize(20)
                    .MoreChoicesText("[grey](Move up and down to reveal more options)[/]")
                    .AddChoices(list));

            try
            {
                if (option.MenuAction.Invoke()) return;
            }
            catch (Exception ex)
            {
                AnsiConsole.WriteException(ex);
                End();
            }


        } while (true);
    }

    static async Task<List<string>> GetGitLabFiles()
    {
        var list = new List<string>();
        try
        {
            using (var httpClient = new HttpClient())
            {
                var response = await httpClient.GetStringAsync($"{GitLabApiBaseUrl}/projects/29762666/repository/tree?path={GitLabScriptPath}");

                using var jsonDocument = JsonDocument.Parse(response);
                var root = jsonDocument.RootElement;

                if (root.ValueKind == JsonValueKind.Array)
                {
                    foreach (var item in root.EnumerateArray())
                    {
                        var name = item.GetProperty("name").GetString();
                        list.Add(name);
                    }
                }
            }
        }
        catch (Exception e)
        {
            Log.Error(e,"GetGitLabFiles");
        }

        return list;
    }


    void End()
    {
        var rule = new Rule($"[white]end[/]");
        rule.RuleStyle("blue");
        rule.LeftAligned();
        AnsiConsole.Write(rule);

        AnsiConsole.Prompt(
            new SelectionPrompt<string>()
                .Title("")
                .HighlightStyle(Style.Parse("yellow"))
                .PageSize(3)
                .AddChoices(new[] {
                    "Press Return"
                }));
    }

}