using System.Net;
using System.Reflection.Metadata.Ecma335;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using Spectre.Console;

namespace AccumulateManager.TelnetConsole;

public partial class clsTelnetAppSession
{
    private Regex DomainRegEx = new Regex(@"(?:[A-Za-z0-9][A-Za-z0-9\-]{0,61}[A-Za-z0-9]|[A-Za-z0-9])");
    private Regex EmailRegEx = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
    
    void Domain(clsTelnetAppSession root)
    {
        bool exit = false;
        
        do
        {
            var menulist = new List<SelectionFunction<SelectEnum>>();

            if (Program.SSLCert != null)
            {
                menulist.Add(new SelectionFunction<SelectEnum>($"View Certificate", () =>
                {
                    var table = new Table();
                    table.AddColumn("SSL");
                    table.AddColumn("");

                    table.AddRow("[green]Version[/]", $"{Program.SSLCert.Version}");
                    table.AddRow("[green]Name[/]", Program.SSLCert.SubjectName?.Name);
                    table.AddRow("[green]IssuerName[/]", Program.SSLCert.IssuerName?.Name);

                    table.AddRow("[green]NotBefore[/]", $"{Program.SSLCert.NotBefore:yyyy-MM-dd HH:mm} UTC");
                    table.AddRow("[green]NotAfter[/]", $"{Program.SSLCert.NotAfter:yyyy-MM-dd HH:mm} UTC");

                    table.AddRow("[green]PublicKey[/]", Program.SSLCert.PublicKey.Oid.Value);
                    table.AddRow("[green]SerialNumber[/]", Program.SSLCert.SerialNumber);
                    table.AddRow("[green]HasPrivateKey[/]", $"{Program.SSLCert.HasPrivateKey}");
                    table.AddRow("[green]Info[/]", Program.SSLCert.GetNameInfo(X509NameType.DnsName, true) ?? "");

                    table.AddRow("[green]Verify[/]", $"{Program.SSLCert.Verify()}");

                    AnsiConsole.Write(table);
                    root.End();

                    return SelectEnum.Selection;
                }));
            }

            menulist.Add(new SelectionFunction<SelectEnum>($"Domains ({Program.Settings.DomainNames.Count})", () =>
            {
                Domains();
                return SelectEnum.SubMenu;
            }));


            menulist.Add(new SelectionFunction<SelectEnum>($"CORS ({Program.Settings.CorsURL.Count})", () =>
            {
                CORS();
                return SelectEnum.SubMenu;
            }));

            menulist.Add(new SelectionFunction<SelectEnum>($"LetsEncrypt e-mail ({(String.IsNullOrEmpty(Program.Settings.DomainEmail) ? "not set" : Program.Settings.DomainEmail)})", () =>
            {
                DomainEmail();
                return SelectEnum.SubMenu;
            }));
            menulist.Add(new SelectionFunction<SelectEnum>("<- Back", () => SelectEnum.Exit));

            ShowScreenHeader();


            var option = AnsiConsole.Prompt(
                new SelectionPrompt<SelectionFunction<SelectEnum>>()
                    .Title("Domain Names")
                    .PageSize(10)
                    .MoreChoicesText("[grey](Move up and down to reveal more options)[/]")
                    .AddChoices(menulist));

            switch (option.MenuAction())
            {
                case SelectEnum.Exit:
                    exit = true;
                    break;
            }
        } while (!exit);
    }

    void Domains()
    {
       bool exit = false;

        do
        {
            var menulist = new List<SelectionFunction<SelectEnum>>();

            foreach (var domain in Program.Settings.DomainNames)
            {
                menulist.Add(new SelectionFunction<SelectEnum>(domain, () => SelectEnum.Selection));
            }

            menulist.Add(new SelectionFunction<SelectEnum>("Add New Domain",() => SelectEnum.New));
            menulist.Add(new SelectionFunction<SelectEnum>("<- Back", () => SelectEnum.Exit));

            var option = AnsiConsole.Prompt(
                new SelectionPrompt<SelectionFunction<SelectEnum>>()
                    .Title("Domain Names")
                    .PageSize(10)
                    .MoreChoicesText("[grey](Move up and down to reveal more options)[/]")
                    .AddChoices(menulist));

            switch (option.MenuAction())
            {
                case SelectEnum.Selection:
                    var delete = MenuDelete(option.Prompt);
                    if (delete)
                    {
                        Program.Settings.DomainNames.Remove(option.Prompt);
                        Program.SaveChanges = true;
                        Program.RestartRequired = true;
                    }
                    break;
                case SelectEnum.New:
                    DomainNew();
                    break;
                case SelectEnum.Exit:
                    exit = true;
                    break;
            }
        } while (!exit);

    }

    void DomainNew()
    {
        string newDomain = "";
        bool valid;
        do
        {
            var rule = new Rule("[white]Domain name[/]");
            rule.RuleStyle("blue");
            rule.LeftAligned();
            AnsiConsole.Write(rule);

            AnsiConsole.MarkupLine($"[cyan]Please make sure the domain resolves to[/] {Program.PublicIPV4 ?? "Unknown"} {Program.PublicIPV6 ?? ""}");

            if (String.IsNullOrEmpty(Program.Settings.DomainEmail) && Program.SSLCert == null)
            {
                AnsiConsole.MarkupLine("[cyan]Remember to set your e-mail address if you want to use LetsEncrypt[/]");
            }

            newDomain = AnsiConsole.Prompt(
                new TextPrompt<string>(" [green]Enter Domain Name[/]?")
                    .AllowEmpty() );
            if (String.IsNullOrEmpty(newDomain)) return;
            
            valid = DomainRegEx.Match(newDomain).Success;

        } while (!valid);

        try
        {
            var ips = Dns.GetHostAddresses(newDomain);
            if (ips?.Length == 0)
            {
                if (!AnsiConsole.Confirm("No IP's found for this domain. Want to continue?"))
                {
                    return;
                }
            }
            AnsiConsole.WriteLine($"IP = {ips[0]}");
        }
        catch (Exception e)
        {
            AnsiConsole.WriteLine("Error looking up address");
        }
        Program.Settings.DomainNames.Add(newDomain);
        Program.SaveChanges = true;
        Program.RestartRequired = true;
    }

    bool MenuDelete(string prompt)
    {
        var list = new List<SelectionFunction<bool>>();
        list.Add(new SelectionFunction<bool>("Keep",() => false));
        list.Add(new SelectionFunction<bool>("Delete",() => true));
        
        var option = AnsiConsole.Prompt(
            new SelectionPrompt<SelectionFunction<bool>>()
                .Title(prompt)
                .PageSize(5)
                .AddChoices(list));

        return option.MenuAction();
    }


    bool DomainEmail()
    {
        string newEmail = "";
        bool valid;
        do
        {
            var rule = new Rule("[white]Domain e-mail contact[/]");
            rule.RuleStyle("blue");
            rule.LeftAligned();
            AnsiConsole.Write(rule);
            
            newEmail = AnsiConsole.Prompt(
                new TextPrompt<string>(" [green]Enter e-mail address[/]?")
                    .AllowEmpty() );
            if (String.IsNullOrEmpty(newEmail)) return false;
            valid = EmailRegEx.Match(newEmail).Success;
        } while (!valid);

        Program.Settings.DomainEmail = newEmail;
        Program.SaveChanges = true;
        Program.RestartRequired = true;
        return true;
    }
    
    
}