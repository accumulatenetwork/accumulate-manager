using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;
using AccumulateManager.IPTables;
using ConsoleServer;
using Docker.DotNet;
using ConsoleServerDocker;
using ProtoHelper;
using Serilog;
using Serilog.Events;
using Spectre.Console;

namespace AccumulateManager.TelnetConsole;

public class clsDebug
{
    private clsTelnetAppSession Root;
    private AnsiTelnetConsole AnsiConsole;
    private List<string> IPCache = new List<string>();

    public clsDebug(clsTelnetAppSession root)
    {
        Root = root;
        AnsiConsole = root.AnsiConsole;
    }


    public void Debug()
    {
        bool exit = false;
        
        do
        {
            Root.ShowScreenHeader();
            var menulist = new List<SelectionFunction<SelectEnum>>();
            menulist.Add(new SelectionFunction<SelectEnum>("AccMan logging", () =>
            {
                Root.SetLogLevel();
                return SelectEnum.Selection;
            }));

            menulist.Add(new SelectionFunction<SelectEnum>("This", () =>
            {
                AnsiConsole.Write(Program.ThisAccManNode.ToProtoTree());
                Root.End();
                return SelectEnum.Selection;
            }));

            menulist.Add(new SelectionFunction<SelectEnum>("Seeds", () =>
            {
                SelectProxyNetwork();
                return SelectEnum.Selection;
            }));

            menulist.Add(new SelectionFunction<SelectEnum>("Test Remote Node", () =>
            {
                string host;
                do
                {
                    var tp = new TextPrompt<string>(" [green]Enter ip address address[/]")
                    {
                        AllowEmpty = true,
                    };
                    host = AnsiConsole.Prompt(tp);

                    if (String.IsNullOrEmpty(host)) return SelectEnum.Selection;
                    if (IPAddress.TryParse(host, out _)) break;
                    AnsiConsole.MarkupLine("[red]Invalid IP address[/]");
                } while (exit);

                if (!IPCache.Contains(host)) IPCache.Add(host);


                var token = new CancellationToken();
                var options = new ParallelOptions { MaxDegreeOfParallelism = 3, CancellationToken = token };
                var task = Parallel.ForEach(clsIPTables.FixedRPCPortsTCP, options, async (fixedPort, token) =>
                {
                    IsPortOpen(host, (int)fixedPort.port, TimeSpan.FromSeconds(3));
                });

                Root.End();
                return SelectEnum.Selection;
            }));


            menulist.Add(new SelectionFunction<SelectEnum>("Settings", () =>
            {
                AnsiConsole.Write(Program.Settings.ToProtoTree());
                Root.End();
                return SelectEnum.Selection;;
            }));
            
            menulist.Add(new SelectionFunction<SelectEnum>("Ports Listening", () =>
            {
                using (var container = new clsDockerRunOnHost(AnsiConsole,Program.DockerManager.GetClient()))
                {
                    if (container.AttatchToHost(new string[] { "/bin/bash" }))
                    {
                        var readerTask = container.StartReaderTask();
                        container.WriteCommand("netstat -tulpn | grep LISTEN");
                        container.WriteCommand("exit");

                        readerTask.Wait();
                    }
                }
                Root.End();
                return SelectEnum.Selection;;
            }));

            
            menulist.Add(new SelectionFunction<SelectEnum>("List Containers", () =>
            {
                var list = Program.DockerManager.ListContainers(1000).Result;
                
                foreach (var item in list)
                {
                    AnsiConsole.WriteLine($"{item.Names[0]} {item.State}");
                }
                Root.End();
                return SelectEnum.Selection;
            }));

            menulist.Add(new SelectionFunction<SelectEnum>("List Telnet Users", () =>
            {
                foreach (var ipEndPoint in Root.TelnetSession.ConnectionList())
                {
                    AnsiConsole.WriteLine($"{ipEndPoint.Address}:{ipEndPoint.Port}");
                }
                Root.End();
                return SelectEnum.Selection;
            }));

            menulist.Add(new SelectionFunction<SelectEnum>("Origin List", () =>
            {
                foreach (var origin in Program.OriginList)
                {
                    AnsiConsole.WriteLine(origin);
                }

                Root.End();
                return SelectEnum.Selection;
            }));


            menulist.Add(new SelectionFunction<SelectEnum>("<- Back", () =>
            {
                return SelectEnum.Exit;
            }));
            
            var option = AnsiConsole.Prompt(
                new SelectionPrompt<SelectionFunction<SelectEnum>>()
                    .Title("Domain Names")
                    .PageSize(20)
                    .MoreChoicesText("[grey](Move up and down to reveal more options)[/]")
                    .AddChoices(menulist));

            switch (option.MenuAction())
            {
                case SelectEnum.Selection:
                    break;
                case SelectEnum.Exit:
                    exit = true;
                    break;
            }
        } while (!exit);
    }
    

    bool IsPortOpen(string host, int port, TimeSpan timeout)
    {
      //  return Task.Run(bool() =>
      //  {
            var sw = new Stopwatch();
            try
            {
                using (var client = new TcpClient())
                {
                    sw.Start();
                    var result = client.BeginConnect(host, port, null, null);
                    var success = result.AsyncWaitHandle.WaitOne(timeout);
                    sw.Stop();
                    client.EndConnect(result);
                    AnsiConsole.MarkupLine($"{host} {port} [green]Conneced[/] {sw.ElapsedMilliseconds}ms");
                    return success;
                }
            }
            catch (SocketException ex)
            {
                AnsiConsole.MarkupLine($"{host} {port} [red]Failed[/] {ex.Message}");
                return false;
            }
            catch
            {
                AnsiConsole.MarkupLine($"{host} {port} [red]Failed[/]");
                return false;
            }
     //   });
    }


    void SelectProxyNetwork()
    {
        do
        {
            var netlist = new List<SelectionItemT<string>>();
            var accManNodes = Program.DistributedProxyList.NodeList.Values.Select(x => x.ProtoMessage.Network);
            var networks = accManNodes.Distinct();

            foreach (var network in networks.OrderBy(s => s))
            {
                var count = accManNodes.Where(x => x == network).Count();
                netlist.Add(new SelectionItemT<string>(String.IsNullOrEmpty(network) ? $"unknown ({count})" : $"{network} ({count})", SelectEnum.Selection, network));
            }

            netlist.Add(new SelectionItemT<string>("<- Back", SelectEnum.Exit, null));

            var option = AnsiConsole.Prompt(
                new SelectionPrompt<SelectionItemT<string>>()
                    .Title("Network")
                    .PageSize(10)
                    .MoreChoicesText("[grey](Move up and down to reveal more options)[/]")
                    .AddChoices(netlist));

                switch (option.MenuAction)
                {
                    case SelectEnum.Selection:
                        SelectProxyNetworkItems(option.Item);
                        break;
                    case SelectEnum.Exit:
                        return;
                }
        } while (true);
    }


    void SelectProxyNetworkItems( string network)
    {
        var netlist = new List<SelectionItemT<clsAccManNode>>();

        IEnumerable<clsAccManNode> net;
        if (String.IsNullOrEmpty(network) || network=="unknown")
        {
            net = Program.DistributedProxyList.NodeList.Values.Where(x => String.IsNullOrEmpty(x.ProtoMessage.Network));
        }
        else
        {
            net = Program.DistributedProxyList.NodeList.Values.Where(x => x.ProtoMessage.Network == network);
        }

        foreach (var accManNodes in net)
        {
            if (accManNodes.ConnectionFromTime != null && accManNodes.ConnectionFromTime.Ticks != 0)
            {
                var time = (DateTime.UtcNow - accManNodes.ConnectionFromTime).TotalSeconds;
                netlist.Add(new SelectionItemT<clsAccManNode>($"{accManNodes.uri.Host} {accManNodes.ProtoMessage.NodeTag} {time:0}s", SelectEnum.Selection, accManNodes));
            }
            else
            {
                netlist.Add(new SelectionItemT<clsAccManNode>($"{accManNodes.uri.Host}", SelectEnum.Selection, accManNodes));
            }
        }


        netlist.Add(new SelectionItemT<clsAccManNode>("<- Back", SelectEnum.Exit));
        do
        {
            AnsiConsole.Clear();

            var option = AnsiConsole.Prompt(
                new SelectionPrompt<SelectionItemT<clsAccManNode>>()
                    .Title("Network")
                    .PageSize(AnsiConsole.Profile.Height - 5)
                    .MoreChoicesText("[grey](Move up and down to reveal more options)[/]")
                    .AddChoices(netlist));

            switch (option.MenuAction)
            {
                case SelectEnum.Selection:
                    AnsiConsole.MarkupLine($"[cyan]ConnectionFromTime:[/]  {option.Item.ConnectionFromTime}");
                    AnsiConsole.MarkupLine($"[cyan]LastSeedRequestTime:[/] {option.Item.LastSeedRequestTime}");
                    AnsiConsole.Write(option.Item.ProtoMessage.ToProtoTree());
                    Root.End();
                    break;
                case SelectEnum.Exit:
                    return;
            }
        } while (true);
    }

}
