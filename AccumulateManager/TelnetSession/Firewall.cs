using AccumulateManager.IPTables;
using Proto.Settings;
using Serilog.Events;
using Spectre.Console;

namespace AccumulateManager.TelnetConsole;

public partial class clsTelnetAppSession
{
    void Firewall(clsTelnetAppSession root)
    {
        bool exit = false;
        
        do
        {
            root.ShowScreenHeader();
            AnsiConsole.MarkupLine($"[grey]IPTables script:[/] {clsIPTables.iptables_path}");
            var list = new List<SelectionFunction<SelectEnum>>();

            if (Program.Settings.DisableFirewall)
            {
                list.Add(new SelectionFunction<SelectEnum>("Enable Firewall",() => SelectEnum.Enable));
            }
            else
            {
                list.Add(new SelectionFunction<SelectEnum>("Disable Firewall",() => SelectEnum.Disable));
                list.Add(new SelectionFunction<SelectEnum>("Restart Firewall",() => SelectEnum.Reset));
            }

            /*list.Add(new SelectionFunction<SelectEnum>("Port Numbers",() =>
            {
                return SelectEnum.Reset;
            }));*/

            list.Add(new SelectionFunction<SelectEnum>("<- Back", () => SelectEnum.Exit));

            var option = AnsiConsole.Prompt(
                new SelectionPrompt<SelectionFunction<SelectEnum>>()
                    .Title("Firewall")
                    .PageSize(10)
                    .MoreChoicesText("[grey](Move up and down to reveal more options)[/]")
                    .AddChoices(list));

            switch (option.MenuAction())
            {
                case SelectEnum.Enable:
                    if (AreYouSure("Enable Firewall"))
                    {
                        if (Program.Settings.DisableFirewall)
                        {
                            Program.Settings.DisableFirewall = false;
                            clsSettings.Save();
                            clsIPTables.UpdateIPTables(true,AnsiConsole).Wait();
                            root.End();
                        }
                    }
                    break;
                case SelectEnum.Disable:
                    if (AreYouSure("Disable Firewall"))
                    {
                        if (!Program.Settings.DisableFirewall)
                        {
                            Program.Settings.DisableFirewall = true;
                            clsSettings.Save();
                            clsIPTables.Clear(AnsiConsole).Wait();
                            root.End();
                        }
                    }
                    break;
                case SelectEnum.Reset:
                    clsIPTables.UpdateIPTables(true,AnsiConsole).Wait();
                    root.End();
                    break;
                case SelectEnum.Exit:
                    exit = true;
                    break;
            }
        } while (!exit);
    }


    void FirewallPorts(clsTelnetAppSession root)
    {
        bool exit = false;

        do
        {
            root.ShowScreenHeader();
            AnsiConsole.MarkupLine($"[grey]IPTables script:[/] {clsIPTables.iptables_path}");
            var list = new List<SelectionFunction<SelectEnum>>();


            list.Add(new SelectionFunction<SelectEnum>("Enable Firewall", () => SelectEnum.Enable));

            list.Add(new SelectionFunction<SelectEnum>("Add New", () => { return SelectEnum.New; }));

            list.Add(new SelectionFunction<SelectEnum>("<- Back", () => SelectEnum.Exit));

            var option = AnsiConsole.Prompt(
                new SelectionPrompt<SelectionFunction<SelectEnum>>()
                    .Title("Firewall")
                    .PageSize(10)
                    .MoreChoicesText("[grey](Move up and down to reveal more options)[/]")
                    .AddChoices(list));

            switch (option.MenuAction())
            {
                case SelectEnum.Enable:
                    if (AreYouSure("Enable Firewall"))
                    {
                        if (Program.Settings.DisableFirewall)
                        {
                            Program.Settings.DisableFirewall = false;
                            clsSettings.Save();
                            clsIPTables.UpdateIPTables(true, AnsiConsole).Wait();
                            root.End();
                        }
                    }

                    break;
                case SelectEnum.Disable:
                    if (AreYouSure("Disable Firewall"))
                    {
                        if (!Program.Settings.DisableFirewall)
                        {
                            Program.Settings.DisableFirewall = true;
                            clsSettings.Save();
                            clsIPTables.Clear(AnsiConsole).Wait();
                            root.End();
                        }
                    }

                    break;
                case SelectEnum.Reset:
                    clsIPTables.UpdateIPTables(true, AnsiConsole).Wait();
                    root.End();
                    break;
                case SelectEnum.Exit:
                    exit = true;
                    break;
            }
        } while (!exit);
    }

}