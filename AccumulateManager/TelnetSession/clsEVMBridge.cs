using System.Text.RegularExpressions;
using ConsoleServer;
using Docker.DotNet;
using Docker.DotNet.Models;
using ConsoleServerDocker;
using Serilog;
using Spectre.Console;

namespace AccumulateManager.TelnetConsole;

public class clsEVMBridge
{

    private AnsiTelnetConsole ANSIConsole;
    private clsTelnetAppSession Root;
    private string _dockername = "evm_bridge";


    public clsEVMBridge(clsTelnetAppSession root)
    {
        Root = root;
        ANSIConsole = root.AnsiConsole;
    }


    public void EVMBridge()
    {
        bool exit = false;

        do
        {
            var evmStatus = GetEVMStatus();
            
            var list = new List<SelectionFunction<bool>>();

            if (evmStatus == EVMStatus.Stopped)
            {
                list.Add(new SelectionFunction<bool>("Start EVM Bridge", () =>
                {
                    clsDockerAttachToContainer container = null;
                    try
                    {
                        CreateContainerResponse image;
                        try
                        {
                            image = new clsDockerImageEVMBridge(ANSIConsole).Create(null, true);
                            container = new clsDockerAttachToContainer(true, ANSIConsole);
                            container.AttachToContainer(image.ID);
                        }
                        catch (DockerImageNotFoundException)
                        {
                            ANSIConsole?.WriteLine("Docker Image cant be Found");
                            Log.Error("{name} Run Docker Image cant be Found", nameof(clsDockerAccumulate));
                        }
                        catch (DockerApiException ex)
                        {
                            ANSIConsole?.WriteLine(ex.Message, "red");
                            Log.Error("{name} Run {message} ", nameof(clsDockerAccumulate), ex.Message);
                        }
                        catch (Exception ex)
                        {
                            ANSIConsole?.WriteException(ex);
                        }

                        try
                        {
                            var consoleTask = container.AttachConsoleOutput(true, 10000); //Blocks until output (or timeout)
                            container.WaitForExitSilence(3,10);
                        }
                        catch (DockerApiException ex)
                        {
                            ANSIConsole?.WriteLine(ex.Message, "red");
                            Log.Error("{name} RunInit {message} ", nameof(clsDockerAccumulate), ex.Message);
                        }
                        catch (Exception ex)
                        {
                            ANSIConsole?.WriteException(ex);
                        }
                    }
                    catch (Exception ex)
                    {
                        ANSIConsole?.WriteException(ex);
                    }

                    return false;
                }));
            }
            else if (evmStatus != EVMStatus.Uninitialised)
            {
                list.Add(new SelectionFunction<bool>("Stop EVM Bridge", () =>
                {
                    if (GetEVMStatus() != EVMStatus.Stopped)
                    {
                        if (Root.AreYouSure("Stop EVM Bridge"))
                        {
                            AnsiConsole.MarkupLine("[yellow]Stopping EVM Bridge, please wait....[/]");
                            var container = Program.DockerManager.GetContainer(_dockername).Result;
                            if (container != null)
                            {
                                Program.DockerManager.StopContainer(container.ID);
                            }
                        }
                    }
                    return false;
                }
                ));

                list.Add(new SelectionFunction<bool>( Root.MenuActiveMarkup("Attach BASH Console",evmStatus== EVMStatus.Running), () =>
                {
                    if (GetEVMStatus() == EVMStatus.Running)
                    {
                        var container = Program.DockerManager.GetContainer(_dockername).Result;
                        if (container != null)
                        {
                            using (var conr = new clsDockerAttachToContainer(false,ANSIConsole))
                            {
                                conr.AttachToContainer(container.ID, new[] { "/bin/bash" });
                                conr.AttachConsoleInOut(false).Wait();
                            }
                            Root.End();
                        }
                    }
                    return false;
                }));

                list.Add(new SelectionFunction<bool>("Attach to Log Output", () =>
                {
                    Program.DockerManager.AttatchToLogOutput("evm_bridge", ANSIConsole);
                    Root.End();
                    return false;
                }));
            }


            list.Add(new SelectionFunction<bool>("Edit EVM Bridge config", () =>
            {
                CreateContainerResponse image;
                try
                {
                    image = new clsDockerImageNano(ANSIConsole).Create("evm_nano",_dockername, "/data/config.yaml");
                    using (var container = new clsDockerAttachToContainer(true, ANSIConsole))
                    {
                        container.AttachToContainer(image.ID,new string[]{"sh"});
                        var task = container.AttachConsoleInOut(true,10000);
                        container.WriteCommand("apk add nano");
                        container.WriteCommand("[ ! -f /data/config.yaml ] && wget -O /data/config.yaml https://raw.githubusercontent.com/AccumulateNetwork/bridge/master/config.yaml.EXAMPLE");
                        container.WriteCommand("nano -w --restricted /data/config.yaml && exit");
                        task.Wait();
                    }
                }
                catch (DockerImageNotFoundException)
                {
                    ANSIConsole?.WriteLine("Docker Image cant be Found");
                    Log.Error("{name} Run Docker Image cant be Found", nameof(clsEVMBridge));
                }
                catch (DockerApiException ex)
                {
                    ANSIConsole?.WriteLine(ex.Message, "red");
                    Log.Error("{name} Run {message} ", nameof(clsDockerAccumulate), ex.Message);
                }
                catch (Exception ex)
                {
                    ANSIConsole?.WriteException(ex);
                }
                return false;
            }));


            list.Add(new SelectionFunction<bool>("<- Back", () => true));

            Root.ShowScreenHeader();

            var option = ANSIConsole.Prompt(
                new SelectionPrompt<SelectionFunction<bool>>()
                    .Title("EVM Bridge")
                    .PageSize(10)
                    .MoreChoicesText("[grey](Move up and down to reveal more options)[/]")
                    .AddChoices(list));

            if (option.MenuAction())
            {
                    exit = true;
                    break;
            }
        } while (!exit);
    }

    enum EVMStatus
    {
        Uninitialised,
        Stopped,
        Waiting,
        Running
    }

    EVMStatus GetEVMStatus()
    {
        var container = Program.DockerManager.GetContainer(_dockername).Result;
        if (container?.State.Contains("unning") ?? false) return EVMStatus.Running;
        var volume = Program.DockerManager.GetVolumeResponse(_dockername).Result;

        if (volume == null) return EVMStatus.Uninitialised;

        if (container == null || container.State.Contains("xited")) return EVMStatus.Stopped;

        return EVMStatus.Waiting;
    }

}