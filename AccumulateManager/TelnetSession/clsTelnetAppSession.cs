using System.ComponentModel.Design;
using System.Text.Json;
using System.Text.Json.Serialization;
using ConsoleServer;
using Serilog;
using Serilog.Events;
using Spectre.Console;
using AccumulateManager;
using AccumulateManager.IPTables;
using Docker.DotNet.Models;
using ConsoleServerDocker;
using ILogger = Serilog.ILogger;

namespace AccumulateManager.TelnetConsole;

public partial class clsTelnetAppSession
{
    public  TelnetSession TelnetSession { get; init; }
    public AnsiTelnetConsole AnsiConsole => TelnetSession.AnsiConsole;
    private ILogger _logger;

    public clsTelnetAppSession(TelnetSession telnetSession , ILogger logger)
    {
        TelnetSession = telnetSession;
        _logger = logger;
    }

    public void Run()
    {
        bool result;
        do
        {
          //  var volumeResponse = clsAccNetworkMap.NetworkInstalled?.GetVolumeResponse().Result;

            var status = clsAccNetworkMap.GetStatus().Result;
            
            var menulist = new List<SelectionFunction<bool>>();
           
            try
            {
                if (status == clsAccNetworkMap.Status.NotSet)
                {
                    menulist.Add(new SelectionFunction<bool>($"Accumulate - Create", () =>
                    {
                         var acc = new clsAccumulateInit(this);
                         acc.Run();
                         return true;
                        }));
                }
                else
                {
                    menulist.Add(new SelectionFunction<bool>($"Accumulate - Manage node", () =>
                    {
                         var acc = new clsAccumulate(this);
                         acc.Run(true);
                         return true;
                    }));
                }
            }
            catch (Exception ex)
            {
                AnsiConsole.WriteLine("Error communicating with Docker!");
            }

            menulist.Add(new SelectionFunction<bool>($"EVM Bridge", () =>
            {
                new clsEVMBridge(this).EVMBridge();
                return true;
            }));


            menulist.Add(new SelectionFunction<bool>($"Domain Names ({Program.Settings.DomainNames.Count})", () =>
            {
                Domain(this);
                return true;
            }));

            menulist.Add(new SelectionFunction<bool>($"Firewall", () =>
            {
                Firewall(this);
                return true;
            }));


            menulist.Add(new SelectionFunction<bool>(Program.RestartRequired ? "[yellow]AccMan Restart Required[/]" : Program.NewVersion ? "[yellow]Update AccMan[/]" : "Restart AccMan", () =>
            {
                if (Program.SaveChanges)
                {
                    clsSettings.Save();
                    Program.SaveChanges = false;
                }
                var container = Program.DockerManager.UpgradeContainer(AnsiConsole).Result;
                AnsiConsole.MarkupLine("[yellow]Waiting for restart[/]");
                Thread.Sleep(10000);
                return true;
                
            }));
            menulist.Add(new SelectionFunction<bool>("Debug", () =>
            {
                new clsDebug(this).Debug();
                return true;
            }));

            if (Program.SaveChanges && !Program.RestartRequired)
            {
                menulist.Add(new SelectionFunction<bool>($"[yellow]Save Changes[/]", () =>
                {
                    clsSettings.Save();
                    Program.SaveChanges = false;
                    return true;
                }));
            }

            if (Program.RunTimeMode != Program.EnumRuntimeMode.Normal)
            {
                menulist.Add(new SelectionFunction<bool>($"[yellow]RESET Runtime Mode[/]", () =>
                {
                    Program.RunTimeMode = Program.EnumRuntimeMode.Normal;
                    return true;
                }));
            }

            menulist.Add(new SelectionFunction<bool>("Exit", () => { return false; }));

            ShowScreenHeader(status);
            
            var option = AnsiConsole.Prompt(
                new SelectionPrompt<SelectionFunction<bool>>()
                    .Title("[magenta]Main Menu[/]")
                    .PageSize(20)
                    .MoreChoicesText("[grey](Move up and down to reveal more options)[/]")
                    .AddChoices(menulist));

            result = option.MenuAction();

        } while (result);
    }

    public void ShowScreenHeader(clsAccNetworkMap.Status? status = null)
    {
        if (status==null) status = clsAccNetworkMap.GetStatus().Result;
        AnsiConsole.Clear(true);

        String nodeType = Program.NodeStatus2?.result?.validator_info?.voting_power == "1" ? "VALIDATOR" : "FOLLOWER" ?? "";

        var rule = new Rule("[white]Accumulate Manager[/]");
        rule.RuleStyle("blue");
        rule.LeftAligned();
        AnsiConsole.Write(rule);

        AnsiConsole.WriteLine();
        var grid = new Grid();
        grid.AddColumn();
        grid.AddColumn();
        grid.AddColumn();
        grid.AddRow("Network: ", clsAccNetworkMap.NetworkInstalled?.ToString() ?? "not set", $"IPV4: [green]{Program.PublicIPV4 ?? "not found"}[/]");
        grid.AddRow("Version: ", clsAccNetworkMap.NetworkInstalled?.Tag ?? "", $"IPV6: [green]{Program.PublicIPV6 ?? "not found"}[/]");
        if (status == clsAccNetworkMap.Status.NotSet)
        {
            grid.AddRow("Volume: ", "",$"[magenta]{(Program.Settings.DomainNames.Count > 0 ? Program.Settings.DomainNames[0] : "")}[/]");
        }
        else
        {
            var volumeResponse = clsAccNetworkMap.NetworkInstalled?.GetVolumeResponse().Result;
            grid.AddRow("Volume: ", $"{volumeResponse?.Name ?? ""} {volumeResponse?.UsageData?.Size}", $"[magenta]{(Program.Settings.DomainNames.Count > 0 ? Program.Settings.DomainNames[0] : "")}[/]");
        }
        
        switch (status)
        {
            case clsAccNetworkMap.Status.Initialised:
                grid.AddRow("Accumulate: ", $"[yellow]{Enum.GetName(status.Value)}[/]", $"[magenta]{(Program.Settings.DomainNames.Count > 1 ? Program.Settings.DomainNames[1] : "")}[/]");
                break;
            case clsAccNetworkMap.Status.NotSet:
                grid.AddRow("Accumulate: ", $"[cyan]{Enum.GetName(status.Value)}[/]", $"[magenta]{(Program.Settings.DomainNames.Count > 1 ? Program.Settings.DomainNames[1] : "")}[/]");
                break;
            case clsAccNetworkMap.Status.Running:
                grid.AddRow("Accumulate: ", $"[green]{Enum.GetName(status.Value)} {nodeType}[/]", $"[magenta]{(Program.Settings.DomainNames.Count > 1 ? Program.Settings.DomainNames[1] : "")}[/]");
                break;
            case clsAccNetworkMap.Status.Stopped:
                grid.AddRow("Accumulate: ", $"[red]{Enum.GetName(status.Value)}[/]", $"[magenta]{(Program.Settings.DomainNames.Count > 1 ? Program.Settings.DomainNames[1] : "")}[/]");
                break;
        }

        
        grid.AddRow("Firewall: ", Program.Settings.DisableFirewall ? $"[red]Disabled[/]" : (Program.FirewallActive ? $"[green]Active[/]" : $"[yellow]Inactive[/]"), $"[magenta]{(Program.Settings.DomainNames.Count > 2 ? Program.Settings.DomainNames[2] : "")}[/]");
        
        AnsiConsole.Write(grid);
        var rule2 = new Rule( $"[blue]{AssemblyGitCommitTag.GetValue()}[/]");
        rule2.RuleStyle("blue");
        rule2.RightAligned();
        AnsiConsole.Write(rule2);

        if (Program.RunTimeMode != Program.EnumRuntimeMode.Normal) AnsiConsole.MarkupLine($"[RED]Runtime mode: {Program.RunTimeMode}[/]");
        if (Program.DevMode) AnsiConsole.MarkupLine($"[Yellow]Dev mode[/]");

    }

    public string MenuActiveMarkup(string input, bool active)
    {
        if (active) return input;
        return $"[grey]{input}[/]";
    }

    public bool AreYouSure(string prompt = "Confirm")
    {
        var rule = new Rule($"[white]{prompt}[/]");
        rule.RuleStyle("red");
        rule.LeftAligned();
        AnsiConsole.Write(rule);
        
        var list = new List<SelectionFunction<bool>>();
        list.Add(new SelectionFunction<bool>("No",() => false));
        list.Add(new SelectionFunction<bool>("Yes",() => true));
        
        var option = AnsiConsole.Prompt(
            new SelectionPrompt<SelectionFunction<bool>>()
                .Title("Are you sure")
                .PageSize(5)
                .AddChoices(list));

        return option.MenuAction();
    }

    public bool YesNo(string prompt = "Select")
    {
        var list = new List<SelectionFunction<bool>>();
        list.Add(new SelectionFunction<bool>("No",() => false));
        list.Add(new SelectionFunction<bool>("Yes",() => true));

        var option = AnsiConsole.Prompt(
            new SelectionPrompt<SelectionFunction<bool>>()
                .Title(prompt)
                .PageSize(5)
                .AddChoices(list));

        return option.MenuAction();
    }


    public void End()
    {
        var rule = new Rule($"[white]end[/]");
        rule.RuleStyle("blue");
        rule.LeftAligned();
        AnsiConsole.Write(rule);

        AnsiConsole.Prompt(
            new SelectionPrompt<string>()
                .Title("")
                .HighlightStyle(Style.Parse("yellow"))
                .PageSize(3)
                .AddChoices(new[] {
                    "Press Return"
                }));
    }
}