using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Grpc.Core;
using Google.Protobuf.WellKnownTypes;
using System.IO;
using Proto.Authentication;
using Google.Protobuf;
using AccumulateManager;


namespace AccumulateManager.GRPC;

public class AuthenticationService : Proto.Authentication.AccManAuthentication.AccManAuthenticationBase
{

    public override Task<AuthenticationReply> NodeAuthenticate(NodeAuthenticationRequest request, ServerCallContext context)
    {
        var authenticationReply = JwtAuthenticationManager.AuthenticateNode(request);
        if (authenticationReply == null)
            throw new RpcException(new Status(StatusCode.Unauthenticated, "Invalid user Credentials"));

        Program.DistributedProxyList.ConnectFrom(request);
        
        return Task.FromResult(authenticationReply);
    }
}