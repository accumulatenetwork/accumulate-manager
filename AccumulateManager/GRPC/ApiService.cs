using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Grpc.Core;
using Proto.API;
using Google.Protobuf.WellKnownTypes;
using System.IO;
using Google.Protobuf;
using Microsoft.AspNetCore.Authorization;


namespace AccumulateManager.GRPC;

[Authorize]
public partial class ApiService : AccManAPI.AccManAPIBase
{
    public ApiService Instance { get; init; }

    public ApiService()
    {
        Instance = this;
    }
    
}