using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Grpc.Core;
using Proto.API;
using Google.Protobuf.WellKnownTypes;
using System.IO;
using Google.Protobuf;
using Serilog;

namespace AccumulateManager.GRPC;

public partial class ApiService
{
    public override Task<AccManNodeList> GetIPList(requestCount request, ServerCallContext context)
    {
        Log.Information($"GetIPList:{context.Peer} requesting {request.Count} proxy nodes");
        return Task.FromResult(Program.DistributedProxyList.GetRandomProxies(request.Count));
    }

    public override Task<AccManNode> GetAccManNode(Empty request, ServerCallContext context)
    {
        Log.Information($"GetID:{context.Peer} request.");
        return Task.FromResult(Program.ThisAccManNode);
    }
}