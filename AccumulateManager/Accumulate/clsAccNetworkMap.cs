using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using Docker.DotNet.Models;
using ConsoleServerDocker;
using Google.Protobuf.WellKnownTypes;
using Serilog;
using SharpYaml.Serialization;
using SharpYaml.Tokens;

namespace AccumulateManager;

public static class clsAccNetworkMap
{
    
    public const uint AccumulateProjectID = 29762666;
    public const uint RegistryID = 2858961;
    static public String NetworkMapURL = "https://gitlab.com/accumulatenetwork/network-map/-/raw/main/networkmap.yaml?inline=false";
    private static DateTime? LastUpdate;
    private static string NetworkMapPath = Path.Combine(Program.DataPath, "NetworkMap.yml");
    private const int CacheTimeout = 2;
    
    public enum Status
    {
        NotSet,
        Initialised,
        Running,
        Stopped
    }

    static public List<AccNetwork> Networks;

    static public async Task<bool> UpdateNetworkMap()
    {
        string? result = null;

        //If cached recently, return true
        if (!Program.DevMode && LastUpdate != null && (DateTime.UtcNow - LastUpdate).Value.TotalMinutes < CacheTimeout) return true;

        List<AccNetwork> networksList = new List<AccNetwork>();
        AccNetwork? network = null;

        try
        {
                using (HttpClient client = new HttpClient())
                {
                    try
                    {
                        client.DefaultRequestHeaders.Add("User-Agent", "AccumulateManager");
                        result = client.GetStringAsync(NetworkMapURL).Result; //dont use await
                    }
                    catch (Exception e)
                    {
                        Log.Warning("Failed to get Network Map{url}",NetworkMapURL);
                        if (File.Exists(NetworkMapPath))
                        {
                            result = File.ReadAllText(NetworkMapPath);
                        }
                    }
                }

            if (String.IsNullOrEmpty(result)) return false;
                
            string text;
            using (var reader = new System.IO.StringReader(result))
            {
                while ((text = await reader.ReadLineAsync()) != null)
                {
                    var textTrim = text.Trim();
                    var colon = textTrim.IndexOf(':');

                    if (!textTrim.StartsWith('-') && textTrim.EndsWith(':'))
                    {
                        network = new AccNetwork(textTrim.Substring(0, textTrim.Length - 1));
                        networksList.Add(network);
                    }
                    else if (textTrim.StartsWith('-') && colon > 0)
                    {
                        var splitText = textTrim.Substring(1).Split(":", 2, StringSplitOptions.TrimEntries);
                        network.AddMetaData(splitText[0], splitText[1]);
                    }
                }
            }
            Networks = networksList;
            LastUpdate = DateTime.UtcNow;
            File.WriteAllText(NetworkMapPath,result);
            return true;
        }
        catch (Exception e)
        {
            Log.Error(e,nameof(UpdateNetworkMap));
        }

        return false;
    }

    static public async Task<Status> GetStatus()
    {

        if (NetworkInstalled is null || NetworkInstalled.DockerName is null) return Status.NotSet;

        try
        {
            var volume = await NetworkInstalled.GetVolumeResponse();
            if (volume is null) return Status.NotSet;
        }
        catch (Exception e)
        {
            return Status.NotSet;
        }

        var container = Program.DockerManager.GetContainer(NetworkInstalled.DockerName).Result;
        if (container == null) return Status.Initialised;
        if (container.State.Contains("unning")) return Status.Running;
        return Status.Stopped;
    }
    
    static public async Task<List<string>> GetDockerVersions(string? wildcard = null)
    {
        await clsAccNetworkMap.UpdateNetworkMap();
        var gl = new clsGitLab(AccumulateProjectID,RegistryID);
        
        var data = await gl.GetDockerImages(RegistryID);

        List<string> list = new List<string>();
        
        if (!String.IsNullOrEmpty(wildcard))
        {
            var regex = new Regex(wildcard.WildCardToRegEx());
            list = data.Where(x => regex.IsMatch(x.name)).Select(x=>x.name).ToList();
        }
        return list;
    }

    private static AccNetworkInstalled? _networkInstalled;
    static public AccNetworkInstalled? NetworkInstalled
    {
        get
        {
            if (_networkInstalled is null)
            {
                var path = Path.Combine(Program.DataPath, "network.yaml");
                if (File.Exists(path))
                {
                    //Read from file
                    try
                    {
                        Log.Information("Loading {path}", path);
                        using (var readFile = new StreamReader(path))
                        {
                            var serializer = new Serializer();
                            _networkInstalled = serializer.Deserialize<AccNetworkInstalled>(readFile);
                        }
                        Program.ThisAccManNode.Network = _networkInstalled.ToString();
                        Program.ThisAccManNode.NodeTag = _networkInstalled.Tag;
                        Log.Information("Network: {network}",Program.ThisAccManNode.Network);
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex, "Get {network}", nameof(AccNetworkInstalled));
                    }
                }
                else
                {
                    Program.ThisAccManNode.Network = "not set";
                }
            }
            return _networkInstalled;
        }
        
        set
        {
           _networkInstalled = value;
           SaveNetworkInstalled();
        }
    }

    static public void SaveNetworkInstalled()
    {
        var path = Path.Combine(Program.DataPath, "network.yaml");
        if (_networkInstalled is not null)
        {
            using (var writefile = new StreamWriter(path))
            {
                Log.Information("Saving {path}", path);
                var serializer = new Serializer();
                serializer.Serialize(writefile, _networkInstalled, typeof(AccNetworkInstalled));
            }
            Program.ThisAccManNode.Network = _networkInstalled.ToString();
            Program.ThisAccManNode.NodeTag = _networkInstalled.Tag;
        }
        else
        {
            if (File.Exists(path))
            {
                File.Delete(path);
                Program.ThisAccManNode.Network = "not set";
            }
        }
    }


}

public class AccNetworkInstalled
{
    public string NetworkName { get; set; }
    public string Tag { get; set; }
    public string Subnet { get; set; }
    public string Genisis { get; set; }
    public string BaseURL { get; set; }

    public string ToString()
    {
        return $"{NetworkName ?? "<not set>"}.{Subnet ?? "--"}";
    }
    
    
    [YamlIgnore]
    public string? DockerName
    {
        get
        {
            if (String.IsNullOrEmpty(NetworkName) || String.IsNullOrEmpty(Subnet)) return null;
            return $"acc_{NetworkName.ToLower().Replace("-","_")}_{Subnet.ToLower()}";
        }
    }
   
    public AccNetwork? GetAccNetwork()
    {
        return clsAccNetworkMap.Networks?.Find(x => x.NetworkName == NetworkName);
    }

    public Task<VolumeResponse> GetVolumeResponse()
    {
        var dockerName = DockerName;
        if (dockerName is null) return null;
        return Program.DockerManager.GetVolumeResponse(dockerName);
    }
}


public class AccNetwork
{
    public string NetworkName { get; init; }
    public string Tags { get; set; }
    public string DevTags { get; set; }
    public string BaseURL { get; set; }
    public record SubnetInfo(string Subnet, string genisis);
    
    public List<SubnetInfo> SubNets = new List<SubnetInfo>();

    public AccNetwork(string name)
    {
        NetworkName = name;
    }

    public void AddMetaData(string key, string value)
    {
        if (key.ToLower() == "tags")
        {
            Tags = value;
        }
        else if (key.ToLower() == "baseurl")
        {
            BaseURL = value;
        }
        else if (key.ToLower() == "devtags")
        {
            DevTags = value;
        }
        else
        {
            SubNets.Add(new SubnetInfo(key, value));
        }
    }
}