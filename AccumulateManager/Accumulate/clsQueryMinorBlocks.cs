using System.Text.Json.Serialization;

public class clsQueryMinorBlocks
{

    // Root myDeserializedClass = JsonSerializer.Deserialize<Root>(myJsonResponse);
    public class Body
    {
        [JsonPropertyName("type")]
        public string type { get; set; }

        [JsonPropertyName("source")]
        public string source { get; set; }

        [JsonPropertyName("minorBlockIndex")]
        public int? minorBlockIndex { get; set; }

        [JsonPropertyName("rootChainIndex")]
        public int? rootChainIndex { get; set; }

        [JsonPropertyName("rootChainAnchor")]
        public string rootChainAnchor { get; set; }

        [JsonPropertyName("stateTreeAnchor")]
        public string stateTreeAnchor { get; set; }

        [JsonPropertyName("acmeBurnt")]
        public string acmeBurnt { get; set; }
    }

    public class Data
    {
        [JsonPropertyName("acmeBurnt")]
        public string acmeBurnt { get; set; }

        [JsonPropertyName("minorBlockIndex")]
        public int? minorBlockIndex { get; set; }

        [JsonPropertyName("rootChainAnchor")]
        public string rootChainAnchor { get; set; }

        [JsonPropertyName("rootChainIndex")]
        public int? rootChainIndex { get; set; }

        [JsonPropertyName("source")]
        public string source { get; set; }

        [JsonPropertyName("stateTreeAnchor")]
        public string stateTreeAnchor { get; set; }

        [JsonPropertyName("type")]
        public string type { get; set; }
    }

    public class Entry
    {
        [JsonPropertyName("hash")]
        public string hash { get; set; }

        [JsonPropertyName("right")]
        public bool? right { get; set; }
    }

    public class Header
    {
        [JsonPropertyName("principal")]
        public string principal { get; set; }

        [JsonPropertyName("initiator")]
        public string initiator { get; set; }
    }

    public class Page
    {
        [JsonPropertyName("signer")]
        public Signer signer { get; set; }

        [JsonPropertyName("signatures")]
        public List<Signature> signatures { get; set; }
    }

    public class Proof
    {
        [JsonPropertyName("start")]
        public string start { get; set; }

        [JsonPropertyName("startIndex")]
        public int? startIndex { get; set; }

        [JsonPropertyName("end")]
        public string end { get; set; }

        [JsonPropertyName("endIndex")]
        public int? endIndex { get; set; }

        [JsonPropertyName("anchor")]
        public string anchor { get; set; }

        [JsonPropertyName("entries")]
        public List<Entry> entries { get; set; }
    }

    public class Receipt
    {
        [JsonPropertyName("localBlock")]
        public int? localBlock { get; set; }

        [JsonPropertyName("localBlockTime")]
        public DateTime? localBlockTime { get; set; }

        [JsonPropertyName("proof")]
        public Proof proof { get; set; }

        [JsonPropertyName("receipt")]
        public Receipt receipt { get; set; }

        [JsonPropertyName("account")]
        public string account { get; set; }

        [JsonPropertyName("chain")]
        public string chain { get; set; }
    }

    public class Receipt2
    {
        [JsonPropertyName("start")]
        public string start { get; set; }

        [JsonPropertyName("startIndex")]
        public int? startIndex { get; set; }

        [JsonPropertyName("end")]
        public string end { get; set; }

        [JsonPropertyName("endIndex")]
        public int? endIndex { get; set; }

        [JsonPropertyName("anchor")]
        public string anchor { get; set; }

        [JsonPropertyName("entries")]
        public List<Entry> entries { get; set; }
    }

    public class Result
    {
        [JsonPropertyName("type")]
        public string type { get; set; }

        [JsonPropertyName("data")]
        public Data data { get; set; }

        [JsonPropertyName("origin")]
        public string origin { get; set; }

        [JsonPropertyName("sponsor")]
        public string sponsor { get; set; }

        [JsonPropertyName("transactionHash")]
        public string transactionHash { get; set; }

        [JsonPropertyName("txid")]
        public string txid { get; set; }

        [JsonPropertyName("transaction")]
        public Transaction transaction { get; set; }

        [JsonPropertyName("signatures")]
        public List<Signature> signatures { get; set; }

        [JsonPropertyName("status")]
        public Status status { get; set; }

        [JsonPropertyName("receipts")]
        public List<Receipt> receipts { get; set; }

        [JsonPropertyName("signatureBooks")]
        public List<SignatureBook> signatureBooks { get; set; }
    }

    public class Root
    {
        [JsonPropertyName("jsonrpc")]
        public string jsonrpc { get; set; }

        [JsonPropertyName("result")]
        public Result result { get; set; }

        [JsonPropertyName("id")]
        public int? id { get; set; }
    }

    public class Signature
    {
        [JsonPropertyName("type")]
        public string type { get; set; }

        [JsonPropertyName("sourceNetwork")]
        public string sourceNetwork { get; set; }

        [JsonPropertyName("destinationNetwork")]
        public string destinationNetwork { get; set; }

        [JsonPropertyName("sequenceNumber")]
        public int? sequenceNumber { get; set; }

        [JsonPropertyName("transactionHash")]
        public string transactionHash { get; set; }

        [JsonPropertyName("publicKey")]
        public string publicKey { get; set; }

        [JsonPropertyName("signature")]
        public string signature { get; set; }

        [JsonPropertyName("signer")]
        public string signer { get; set; }

        [JsonPropertyName("signerVersion")]
        public int? signerVersion { get; set; }

        [JsonPropertyName("timestamp")]
        public int? timestamp { get; set; }
    }

    public class SignatureBook
    {
        [JsonPropertyName("authority")]
        public string authority { get; set; }

        [JsonPropertyName("pages")]
        public List<Page> pages { get; set; }
    }

    public class Signer
    {
        [JsonPropertyName("type")]
        public string type { get; set; }

        [JsonPropertyName("url")]
        public string url { get; set; }

        [JsonPropertyName("version")]
        public int? version { get; set; }
    }

    public class Signer2
    {
        [JsonPropertyName("type")]
        public string type { get; set; }

        [JsonPropertyName("url")]
        public string url { get; set; }
    }

    public class Status
    {
        [JsonPropertyName("txID")]
        public string txID { get; set; }

        [JsonPropertyName("code")]
        public string code { get; set; }

        [JsonPropertyName("delivered")]
        public bool? delivered { get; set; }

        [JsonPropertyName("codeNum")]
        public int? codeNum { get; set; }

        [JsonPropertyName("result")]
        public Result result { get; set; }

        [JsonPropertyName("received")]
        public int? received { get; set; }

        [JsonPropertyName("initiator")]
        public string initiator { get; set; }

        [JsonPropertyName("signers")]
        public List<Signer> signers { get; set; }

        [JsonPropertyName("sourceNetwork")]
        public string sourceNetwork { get; set; }

        [JsonPropertyName("destinationNetwork")]
        public string destinationNetwork { get; set; }

        [JsonPropertyName("sequenceNumber")]
        public int? sequenceNumber { get; set; }

        [JsonPropertyName("anchorSigners")]
        public List<string> anchorSigners { get; set; }
    }

    public class Transaction
    {
        [JsonPropertyName("header")]
        public Header header { get; set; }

        [JsonPropertyName("body")]
        public Body body { get; set; }
    }



}