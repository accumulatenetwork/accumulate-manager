namespace AccumulateManager;

public class clsNewNetworkJSON
{

    public string id { get; set; }
    public List<Bvn> bvns { get; set; }

    public class Bvn
    {
        public string id { get; set; }
        public List<Node> nodes { get; set; }
    }

    public class Node
    {
        public string dnnType { get; set; }
        public string bvnnType { get; set; }
        public int basePort { get; set; }
        public string advertizeAddress { get; set; }
        public string peerAddress { get; set; }
        public string listenAddress { get; set; }
    }

}