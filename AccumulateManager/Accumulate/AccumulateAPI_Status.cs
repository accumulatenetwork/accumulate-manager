namespace AccumulateManager;

// http://testnet.accumulatenetwork.io:16695/status

public class AccumulateAPI_Status
{
    public bool ok { get; set; }
    public int bvnHeight { get; set; }
    public string bvnTime { get; set; }
    public DateTime dnTime { get; set; }
    public int lastDirectoryAnchorHeight { get; set; }
    public string bvnRootHash { get; set; }
    public string dnRootHash { get; set; }
    public string bvnBptHash { get; set; }
    public string dnBptHash { get; set; }
}