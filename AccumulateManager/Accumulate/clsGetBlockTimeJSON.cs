namespace AccumulateManager;

public class clsGetBlockTimeJSON
{
   public string txhash { get; set; }
   public bool delivered { get; set; }
   public List<resultItem> result { get; set; }

   public class resultItem
   {
       public string account { get; set; }
       public string chain{ get; set; }
       public long localBlock{ get; set; }
       public string localBlockTime{ get; set; }
   }

}

