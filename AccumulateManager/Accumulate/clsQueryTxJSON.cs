namespace AccumulateManager;

public class clsQueryTxJSON
{
    public string jsonrpc { get; set; }
    public Result result { get; set; }
    public int id { get; set; }


// Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    public class Body
    {
        public string type { get; set; }
        public string source { get; set; }
        public long minorBlockIndex { get; set; }
        public long rootChainIndex { get; set; }
        public string rootChainAnchor { get; set; }
        public string stateTreeAnchor { get; set; }
        public string acmeBurnt { get; set; }
    }

    public class Data
    {
        public string acmeBurnt { get; set; }
        public long minorBlockIndex { get; set; }
        public string rootChainAnchor { get; set; }
        public long rootChainIndex { get; set; }
        public string source { get; set; }
        public string stateTreeAnchor { get; set; }
        public string type { get; set; }
    }

    public class Entry
    {
        public string hash { get; set; }
        public bool? right { get; set; }
    }

    public class Header
    {
        public string principal { get; set; }
        public string initiator { get; set; }
    }

    public class Page
    {
        public Signer signer { get; set; }
        public List<Signature> signatures { get; set; }
    }

    public class Proof
    {
        public string start { get; set; }
        public long startIndex { get; set; }
        public string end { get; set; }
        public long endIndex { get; set; }
        public string anchor { get; set; }
        public List<Entry> entries { get; set; }
    }

    public class Receipt
    {
        public long localBlock { get; set; }
        public string localBlockTime { get; set; }
        public Proof proof { get; set; }
        public Receipt receipt { get; set; }
        public string account { get; set; }
        public string chain { get; set; }
    }

    public class Receipt2
    {
        public string start { get; set; }
        public long startIndex { get; set; }
        public string end { get; set; }
        public long endIndex { get; set; }
        public string anchor { get; set; }
        public List<Entry> entries { get; set; }
    }

    public class Result
    {
        public string type { get; set; }
        public Data data { get; set; }
        public string origin { get; set; }
        public string sponsor { get; set; }
        public string transactionHash { get; set; }
        public string txid { get; set; }
        public Transaction transaction { get; set; }
        public List<Signature> signatures { get; set; }
        public Status status { get; set; }
        public List<Receipt> receipts { get; set; }
        public List<SignatureBook> signatureBooks { get; set; }
    }


    public class Signature
    {
        public string type { get; set; }
        public string sourceNetwork { get; set; }
        public string destinationNetwork { get; set; }
        public long sequenceNumber { get; set; }
        public string transactionHash { get; set; }
        public string publicKey { get; set; }
        public string signature { get; set; }
        public string signer { get; set; }
        public int? signerVersion { get; set; }
        public long? timestamp { get; set; }
    }

    public class SignatureBook
    {
        public string authority { get; set; }
        public List<Page> pages { get; set; }
    }

    public class Signer
    {
        public string type { get; set; }
        public string url { get; set; }
        public int version { get; set; }
    }

    public class Signer2
    {
        public string type { get; set; }
        public string url { get; set; }
    }

    public class Status
    {
        public string txID { get; set; }
        public string code { get; set; }
        public bool delivered { get; set; }
        public long codeNum { get; set; }
        public Result result { get; set; }
        public long received { get; set; }
        public string initiator { get; set; }
        public List<Signer> signers { get; set; }
        public string sourceNetwork { get; set; }
        public string destinationNetwork { get; set; }
        public long sequenceNumber { get; set; }
        public List<string> anchorSigners { get; set; }
    }

    public class Transaction
    {
        public Header header { get; set; }
        public Body body { get; set; }
    }

}