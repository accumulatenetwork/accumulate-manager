using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using AccumulateManager.Cache;
using AccumulateManager.GRPC;
using AccumulateManager.Prometheus;
using AccumulateManager.REST;
using Google.Protobuf;
using Google.Protobuf.Reflection;
using LettuceEncrypt;
using LettuceEncrypt.Acme;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Serilog;
using Serilog.Events;
using Yarp.ReverseProxy.Configuration;
using AuthenticationService = AccumulateManager.GRPC.AuthenticationService;
using JsonSerializer = System.Text.Json.JsonSerializer;
using Prometheus;

namespace AccumulateManager
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = false;
                options.SaveToken = true;
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey =
                        new SymmetricSecurityKey(Encoding.ASCII.GetBytes(JwtAuthenticationManager.JWT_TOKEN_KEY)),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                };
            });
            services.AddAuthorization();
            services.AddGrpc();
            /*services.AddRazorPages()
                .AddRazorPagesOptions(options =>
                {
                    //Use the below line to change the default directory
                    //for your Razor Pages.
                    options.RootDirectory = Path.Combine("/",Program.DataPath, "www");
                
                    //Use the below line to change the default
                    //"landing page" of the application.
                    //options.Conventions.AddPageRoute(Path.Combine(Program.DataPath, "www","index.razor") , "");
              
                  
                });*/

            services.AddCors(o => o.AddPolicy("AllowAll", builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
                //  .WithExposedHeaders("Grpc-Status", "Grpc-Message", "Grpc-Encoding", "Grpc-Accept-Encoding");
            }));

            services.AddCors(o => o.AddPolicy(Program.CORSpolicy, builder =>
            {
                builder.AllowAnyMethod()
                       .AllowAnyHeader()
                       .SetIsOriginAllowedToAllowWildcardSubdomains()
                       .WithExposedHeaders("Grpc-Status", "Grpc-Message", "Grpc-Encoding", "Grpc-Accept-Encoding");

                var corsList = new List<string>();
                foreach (var domain in Program.Settings.DomainNames)
                {
                    corsList.Add($"https://{domain}");
                }
                corsList.AddRange(Program.Settings.CorsURL);
                Log.Information("CORS: {@list}", corsList);

                if (corsList.Contains("*") || corsList.Contains("https://*"))
                {
                    Log.Information("CORS: AllowAnyOrigin ACTIVE");
                    builder.AllowAnyOrigin();
                }
                else
                {
                    builder.WithOrigins(corsList.Distinct().ToArray());
                }
            }));
#if !DEBUG
            if (Program.Settings.DomainNames?.Count > 0 && Program.SSLCert == null)
            {
                if (String.IsNullOrEmpty(Program.Settings.DomainEmail))
                {
                    Log.Error("Domain Name e-mail missing.  Skipping Lets Encrypt.");
                }
                else
                {
                       services.AddLettuceEncrypt(c =>
                        {
                            /*c.EabCredentials = new EabCredentials
                            {
                                EabKeyId = null,
                                EabKey = null,
                                EabKeyAlg = null
                            }*/
                            c.DomainNames = Program.Settings.DomainNames.ToArray();
                            c.EmailAddress = Program.Settings.DomainEmail;
                            c.AcceptTermsOfService = true;
                            c.RenewDaysInAdvance = TimeSpan.FromDays(5);
                            c.AllowedChallengeTypes = ChallengeType.TlsAlpn01;

                            //c.UseStagingServer = true;

                        })
                        .PersistDataToDirectory(new DirectoryInfo(Program.DataPathCerts), null);
                }
            }
#endif
            //    services.AddControllers();
            //   services.AddRouting();


            services.AddSingleton<IProxyConfigProvider>(Program.ProxyConfigProvider);
            services.AddReverseProxy();
            services.AddAllPrometheusMetrics();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseSerilogRequestLogging(options =>
            {
                // Customize the message template
                options.MessageTemplate = "Handled {RequestPath}";
    
                // Emit debug-level events instead of the defaults
                options.GetLevel = (httpContext, elapsed, ex) => LogEventLevel.Debug;
    
                // Attach additional properties to the request completion event
                options.EnrichDiagnosticContext = (diagnosticContext, httpContext) =>
                {
                    diagnosticContext.Set("RequestHost", httpContext.Request.Host.Value);
                    diagnosticContext.Set("RequestScheme", httpContext.Request.Scheme);
                };
            });

            app.UseRouting();
            app.UseGrpcWeb();
            app.UseCors();

            if (!String.IsNullOrEmpty(Program.DataPathWWW) &&  Directory.Exists(Program.DataPathWWW))
            {
                app.UseStaticFiles(new StaticFileOptions
                {
                    FileProvider = new PhysicalFileProvider(Program.DataPathWWW),
                    RequestPath = new PathString("/www")
                });
            }

            app.UseAuthentication();
            app.UseAuthorization();
            app.UsePerRequestMetricCollection();

            app.UseEndpoints(endpoints =>
            {
               // endpoints.MapControllers().RequireCors(Program.CORSpolicy);
                endpoints.MapGet("/circulatingsupply",
                    async context =>
                    {
                        using (var httpClient = new HttpClient())
                        {
                            var response = await httpClient.PostAsync("http://localhost:16695/v2", new StringContent("{\"jsonrpc\":\"2.0\",\"method\":\"query\",\"params\":{\"url\":\"acc://ACME\"},\"id\":1}") );
                            var json = JsonSerializer.Deserialize<clsTokenStats>(await response.Content.ReadAsStringAsync());

                            if (double.TryParse(json.result.data.issued, out var issued))
                            {
                                issued /= 100000000;
                                issued -= 154588859.85772869;
                                await context.Response.WriteAsync($"{issued}");
                            }
                        }
                    }).RequireCors(Program.CORSpolicy);

                endpoints.MapGet("/totalsupply",
                    async context =>
                    {
                        using (var httpClient = new HttpClient())
                        {
                            var response = await httpClient.PostAsync("http://localhost:16695/v2", new StringContent("{\"jsonrpc\":\"2.0\",\"method\":\"query\",\"params\":{\"url\":\"acc://ACME\"},\"id\":1}") );
                            var json = JsonSerializer.Deserialize<clsTokenStats>(await response.Content.ReadAsStringAsync());

                            if (double.TryParse(json.result.data.issued, out var supplyLimit))
                            {
                                supplyLimit /= 100000000;
                                await context.Response.WriteAsync($"{supplyLimit}");
                            }
                        }
                    }).RequireCors(Program.CORSpolicy);;

                endpoints.MapGet("/maxsupply",
                    async context =>
                    {
                        using (var httpClient = new HttpClient())
                        {
                            var response = await httpClient.PostAsync("http://localhost:16695/v2", new StringContent("{\"jsonrpc\":\"2.0\",\"method\":\"query\",\"params\":{\"url\":\"acc://ACME\"},\"id\":1}") );
                            var json = JsonSerializer.Deserialize<clsTokenStats>(await response.Content.ReadAsStringAsync());

                            if (double.TryParse(json.result.data.supplyLimit, out var supplyLimit))
                            {
                                supplyLimit /= 100000000;
                                await context.Response.WriteAsync($"{supplyLimit}");
                            }
                        }
                    }).RequireCors(Program.CORSpolicy);;


                endpoints.MapGet("/getblocktime", async (string hash) =>
                {
                    var receipts = clsGetHashTime.GetHashReceipt(hash);
                    bool deliveredFlag = false;
                    bool getGenesis = false;

                    if (receipts == null)
                    {
                        try
                        {
                            using (var httpClient = new HttpClient())
                            {
                                var stringRequest = string.Concat(new string[] { "{\"jsonrpc\":\"2.0\",\"id\":0,\"method\":\"query-tx\",\"params\":{\"txid\":\"", hash, "\",\"prove\":true}}" });
#if DEBUG
                                var response = await httpClient.PostAsync("https://mainnet.accumulatenetwork.io/v2", new StringContent(stringRequest));
#else
                                var response = await httpClient.PostAsync("http://localhost:16695/v2", new StringContent(stringRequest));
#endif
                                var jsonText = await response.Content.ReadAsStringAsync();

                                try
                                {
                                    var queryResult = JsonSerializer.Deserialize<clsQueryTxJSON>(jsonText);
                                    if (queryResult?.result?.receipts != null)
                                    {
                                        receipts = queryResult.result?.receipts;
                                        deliveredFlag = queryResult.result.status?.delivered ?? false;
                                        if (deliveredFlag)
                                        {
                                            clsGetHashTime.SetHash(hash, receipts);
                                        }
                                    }
                                    else if (queryResult?.result?.type.Contains("enesis") ?? false)
                                    {
                                        getGenesis = true;
                                    }
                                }
                                catch (Exception e)
                                {
                                    if (jsonText?.Contains("enesis") ?? false) getGenesis = true;
                                }

                                if (getGenesis)
                                {
#if DEBUG
                                    var response2 = await httpClient .GetAsync("http://bvn0-seed.mainnet.accumulatenetwork.io:16592/genesis");
#else
                                    var response2 = await httpClient .GetAsync("http://localhost:16592/genesis");
#endif
                                    var text = response2.Content.ReadAsStringAsync().Result;
                                    var pt1 =text.IndexOf("\"genesis_time\""); //+17
                                    if (pt1 > 0)
                                    {
                                        pt1 +=17;
                                        var pt2 = text.IndexOf("\"",pt1);
                                        if (pt2 > 0)
                                        {
                                            receipts = new List<clsQueryTxJSON.Receipt>();
                                            receipts.Add(new clsQueryTxJSON.Receipt
                                            {
                                                localBlock = 0,
                                                localBlockTime = text.Substring(pt1,pt2-pt1),
                                                account = "",
                                                chain = ""
                                            });
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            Log.Error(e ,"gettime");
                        }
                    }
                    else
                    {
                        deliveredFlag = true;
                    }

                    var json = new clsGetBlockTimeJSON()
                    {
                        txhash = hash,
                        delivered = deliveredFlag,
                        result = new List<clsGetBlockTimeJSON.resultItem>(),
                    };

                    if (receipts != null)
                    {
                        foreach (var receipt in receipts)
                        {
                            json.result.Add(new clsGetBlockTimeJSON.resultItem
                            {
                                account = receipt.account,
                                chain = receipt.chain,
                                localBlock = receipt.localBlock,
                                localBlockTime = receipt.localBlockTime
                            });
                        }
                    }
                    return Microsoft.AspNetCore.Http.Results.Json(json,null,"application/json");

                }).RequireCors(Program.CORSpolicy);


                endpoints.MapGet("/getblocknotime", async (string block) =>
                {
                    if (block.All(char.IsNumber))
                    {
                        using (var httpClient = new HttpClient())
                        {
                            var stringRequest = string.Concat(new string[] { "{\"jsonrpc\":\"2.0\",\"id\":0,\"method\":\"query-minor-blocks\",\"params\":{\"start\":", block, ",\"count\":1,\"url\": \"acc://dn.acme\"}}" });
#if DEBUG
                            var response = await httpClient.PostAsync("https://mainnet.accumulatenetwork.io/v2", new StringContent(stringRequest));
#else
                            var response = await httpClient.PostAsync("http://localhost:16695/v2", new StringContent(stringRequest));
#endif

                            var text = response.Content.ReadAsStringAsync().Result;
                            var pt1 =text.IndexOf("\"blockTime\""); //+13
                            if (pt1 > 0)
                            {
                                pt1 +=13;
                                var pt2 = text.IndexOf("\"",pt1);
                                if (pt2 > 0)
                                {
                                    return Microsoft.AspNetCore.Http.Results.Text(text.Substring(pt1, pt2 - pt1));
                                }
                            }
                        }
                    }
                    return Microsoft.AspNetCore.Http.Results.NotFound();

                }).RequireCors(Program.CORSpolicy);

                //   endpoints.MapRazorPages();
             //   endpoints.MapControllers();
                endpoints.MapGrpcService<AuthenticationService>().EnableGrpcWeb().RequireCors("AllowAll");
                endpoints.MapGrpcService<ApiService>().EnableGrpcWeb().RequireCors("AllowAll");
              //  endpoints.MapReverseProxy();  //https://github.com/microsoft/reverse-proxy/blob/main/samples/ReverseProxy.Code.Sample/Startup.cs

                 endpoints.MapReverseProxy(proxyPipeline =>
                 {
                     proxyPipeline.Use((context, next) =>
                     {
                          if (context.Request.Headers.Origin.ToString() != null)
                          {
                              string org = context.Request.Headers.Origin.ToString();
                              Log.Information("ORIGIN: {org}",org);
                              if (!Program.OriginList.Contains(org)) Program.OriginList.Add(org);
                          }

                         return next();
                     });
                 });

                 // Add the /Metrics endpoint for prometheus to query on
                 endpoints.MapMetrics();

                // endpoints.MapFallbackToPage("/Index.razor");

                /*endpoints.MapGet("/",
                    async context =>
                    {
                        await context.Response.WriteAsync(
                            "Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");
                    });*/

                endpoints.MapGet("/rpc",
                    async context =>
                    {
                        using (var sr = new System.IO.StreamReader(context.Request.Body))
                        {
                            string content = await sr.ReadToEndAsync();
                            var ipRequest = JsonSerializer.Deserialize<REST.GetIP>(content);

                            var nodes = Program.DistributedProxyList.GetRandomAccumulateNodes(ipRequest.@params.network,ipRequest.@params.count);
                            
                            var ipReply = new GetIPReply
                            {
                                jsonrpc = ipRequest.jsonrpc,
                                result = new GetIPReply.Result
                                {
                                    network = null,
                                    addresses = default
                                },
                                id = ipRequest.id
                            };

                            await context.Response.WriteAsync(JsonSerializer.Serialize<REST.GetIPReply>(ipReply));
                        }
                    });
            });
            
        }
    }
}