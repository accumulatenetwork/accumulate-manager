namespace AccumulateManager.REST;


public class GetIPReply
{
    public string jsonrpc { get; set; }
    public Result result { get; set; }
    public int id { get; set; }
    
    public class Result
    {
        public string network { get; set; }
        public List<string> addresses { get; set; }
    }
}


public class GetIP
{
    public string jsonrpc { get; set; }
    public string method { get; set; }
    public Params @params { get; set; }
    public int id { get; set; }
    
    public class Params
    {
        public string network { get; set; }
        public int count { get; set; }
    }
}
