// Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);

public class clsTokenStats
{
    public string jsonrpc { get; set; }
    public Result result { get; set; }
    public int id { get; set; }


public class Authority
{
    public string url { get; set; }
}

public class Chain
{
    public int count { get; set; }
    public int height { get; set; }
    public string name { get; set; }
    public List<string> roots { get; set; }
    public string type { get; set; }
}

public class Data
{
    public List<Authority> authorities { get; set; }
    public string issued { get; set; }
    public int precision { get; set; }
    public string supplyLimit { get; set; }
    public string symbol { get; set; }
    public string type { get; set; }
    public string url { get; set; }
}

public class MainChain
{
    public int count { get; set; }
    public int height { get; set; }
    public List<string> roots { get; set; }
}

public class MerkleState
{
    public int count { get; set; }
    public int height { get; set; }
    public List<string> roots { get; set; }
}

public class Result
{
    public string chainId { get; set; }
    public List<Chain> chains { get; set; }
    public Data data { get; set; }
    public MainChain mainChain { get; set; }
    public MerkleState merkleState { get; set; }
    public string type { get; set; }
}

}