#!/bin/sh
#AccMan iptables firewall script


ClearTable()
{
        echo "Clearing INPUT chain"
        iptables --flush INPUT
        iptables --policy INPUT ACCEPT
}

SetupPre()
{
        iptables --flush INPUT
        iptables --policy INPUT DROP
        iptables -A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
        iptables -A INPUT -i lo -p all -j ACCEPT
        iptables -A INPUT -p tcp -m tcp --tcp-flags ACK ACK -j ACCEPT
        
        # --- RATE-LIMIT chain ---
        (iptables --flush RATE-LIMIT || iptables --new-chain RATE-LIMIT) 2> /dev/null
        iptables --policy RATE-LIMIT DROP
        iptables --append RATE-LIMIT \
            --match hashlimit \
            --hashlimit-upto 2/sec \
            --hashlimit-burst 20 \
            --hashlimit-name conn_rate_limit \
            --jump ACCEPT
        iptables --append RATE-LIMIT -j DROP
        

        # --- P2P chain ---
        (iptables --flush AccMan || iptables --new-chain AccMan) 2> /dev/null
        iptables --policy AccMan DROP
}

SetupPost()
{
        iptables -A INPUT --jump AccMan
}

PrivilegedAccess()
{
      #iptables -A INPUT -p tcp -s 123.123.123.123 -j ACCEPT   # Your office IP or paying customer
}


RegularPorts()
{
        iptables -A INPUT -p icmp -m limit --limit 10/second -j ACCEPT          #ICMP packets
        iptables -A INPUT -p tcp --dport 22 -j ACCEPT                           # ssh
}


LogDropped()
{
        iptables -A INPUT -m limit --limit 15/minute -j LOG   --log-level 7 --log-prefix "Dropped by firewall: "
        iptables -A OUTPUT -m limit --limit 15/minute -j LOG  --log-level 7 --log-prefix "Dropped by firewall: "
        iptables -A DOCKER-USER -m limit --limit 15/minute -j LOG  --log-level 7 --log-prefix "Dropped by firewall: "   
}


case "$1" in
  start)
        SetupPre
        PrivilegedAccess
        RegularPorts
        SetupPost
    ;;
  stop)
        ClearTable                                        
    ;;
  *)
    echo "Usage: iptables.sh {start|stop} $1"
    exit 1
    ;;
esac

exit 0
