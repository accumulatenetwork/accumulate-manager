using System.Net;
using System.Text;
using ConsoleServer;
using Docker.DotNet;
using Docker.DotNet.Models;
using Proto.API;
using Proto.Settings;
using Serilog;
using Spectre.Console;
using ILogger = Serilog.ILogger;

namespace AccumulateManager.IPTables;

static public class clsIPTables
{
    static private ILogger _logger;
    private const string _chain = "ACCUMULATE";
    const string DEFAULT_TARGET = "RATE-LIMIT";

    static public  readonly (uint port,IpTablesArgs.IPProtocol protocol)[] FixedRPCPortsTCP = {
        (16591,IpTablesArgs.IPProtocol.tcp),  //DN TM P2P
        (16592,IpTablesArgs.IPProtocol.tcp),  //DN TM RPC
        (16593,IpTablesArgs.IPProtocol.udp),  //DN ACC P2P (UDP)
        (16593,IpTablesArgs.IPProtocol.tcp),  //DN ACC P2P (TCP)
        (16594,IpTablesArgs.IPProtocol.tcp),  //Prometheus - TODO Allow requests from local docker containers but not from the network
        (16595,IpTablesArgs.IPProtocol.tcp),  //DN ACC RPC
        (16691,IpTablesArgs.IPProtocol.tcp),  //BVN TM P2P
        (16692,IpTablesArgs.IPProtocol.tcp),  //BVN TM RPC
        (16693,IpTablesArgs.IPProtocol.udp),  //BVN ACC P2P (UDP)
        (16693,IpTablesArgs.IPProtocol.tcp),  //BVN ACC P2P (TCP)
        (16695,IpTablesArgs.IPProtocol.tcp),  //BVN ACC RPC
        (16666,IpTablesArgs.IPProtocol.tcp)}; //AccMan


    public static string iptables_path { get; private set; }

//    private const string IPtablesFile = "https://gitlab.com/accumulatenetwork/AccMan/-/raw/develop/AccumulateManager/IPTables/iptables.sh?inline=false";


    static clsIPTables()
    {
        _logger = Log.Logger.ForContext(typeof(clsIPTables));
        iptables_path = Environment.GetEnvironmentVariable("IPTABLES", EnvironmentVariableTarget.Process);
    }


    static public async Task<bool> UpdateIPTables(bool rebuild, AnsiTelnetConsole? console = null, IEnumerable<IpTablesArgs>? iptablesArgs = null)
    {
        _logger.Information("{method}: Start. Rebuild={reset}",nameof(UpdateIPTables),rebuild);

        try
        {
            var cancel = new CancellationTokenSource();
            using (var run = Program.DockerManager.CreateRunOnHostInstance(console,cancel.Token))
            {
                if (!run.AttatchToHost("/bin/sh"))
                {
                    _logger.Information("{name} Failed to start.", nameof(UpdateIPTables));
                    console?.MarkupLine("[red]Failed to start[/]");
                    return false;
                }

                var startReaderTask = run.StartReaderTask();

                if (rebuild)
                {
                    _logger.Information("{method}: Reset chain",nameof(UpdateIPTables));

                    if (!String.IsNullOrEmpty(iptables_path))
                    {
                        _logger.Information("{method}: Run iptables.sh",nameof(UpdateIPTables));
                        run.WriteCommand($"sh {iptables_path} start");
                    }

                    //Flush Chain.
                    run.WriteCommand($"iptables --flush {_chain}");
                    //Add path for Proxy listener ports
                    foreach (var proxyNetwork in Program.Settings.Proxy.Network)
                    {
                        var ipTablesArgs = new IpTablesArgs()
                        {
                            Protocol = IpTablesArgs.IPProtocol.tcp,
                            Port = proxyNetwork.UrlJsonAPI,
                            Target = DEFAULT_TARGET,
                        };
                        run.WriteCommand($"iptables -A {_chain} {ipTablesArgs}");
                    }

                    //Add FixedRPCPortsTCP ports
                    foreach (var fixPort in FixedRPCPortsTCP)
                    {
                        var ipTablesArgs2 = new IpTablesArgs()
                        {
                            Protocol = fixPort.protocol,
                            Port = fixPort.port,
                            Target = DEFAULT_TARGET,
                        };
                        run.WriteCommand($"iptables -A {_chain} {ipTablesArgs2}");
                    }


                    //WWW port
                    if (Program.Settings.WWWPort > 0)
                    {
                        var ipTablesArgs3 = new IpTablesArgs()
                        {
                            Protocol = IpTablesArgs.IPProtocol.tcp,
                            Port = Program.Settings.WWWPort,
                            Target = DEFAULT_TARGET,
                        };
                        run.WriteCommand($"iptables -A {_chain} {ipTablesArgs3}");
                    }

                    run.WriteCommand($"iptables -A {_chain} -j DROP");
                }

                if (iptablesArgs != null)
                {
                    foreach (var ipTablesArgs in iptablesArgs)
                    {
                       run.WriteCommand($"iptables -I {_chain} {ipTablesArgs}");
                    }
                }
                run.WriteCommand($"exit");
                startReaderTask.WaitAsync(TimeSpan.FromSeconds(10));
            }

            Program.FirewallActive = true;
            _logger.Information("{name} complete.", nameof(UpdateIPTables));
            cancel.Cancel(true);
            return true;
        } catch (Exception ex)
        {
            _logger.Error(ex,nameof(UpdateIPTables));
        }

        return false;
    }

    static public async Task Clear(AnsiTelnetConsole console = null)
    {

        _logger.Information("{method}: Start.",nameof(Clear));

        try
        {
            var cancel = new CancellationTokenSource();
            using (var run = Program.DockerManager.CreateRunOnHostInstance(console, cancel.Token))
            {
                if (run.AttatchToHost("/bin/sh"))
                {
                    var readerTask = run.StartReaderTask();

                    _logger.Information("{method}: Reset chain", nameof(UpdateIPTables));

                    if (!String.IsNullOrEmpty(iptables_path))
                    {
                        _logger.Information("{method}: Run iptables.sh", nameof(UpdateIPTables));
                        run.WriteCommand($"sh {iptables_path} clear");
                    }

                    run.WriteCommand($"exit");
                    readerTask.Wait(10000);
                }
            }
            Program.FirewallActive = false;
            _logger.Information("{name} complete.", nameof(Clear));
            cancel.Cancel(true);

        } catch (Exception ex)
        {
            _logger.Error(ex,nameof(UpdateIPTables));
        }
    }



    public record IpTablesArgs
    {
        public enum IPProtocol
        {
            udp,
            tcp
        }

        public IPProtocol Protocol { get; init; } = IPProtocol.tcp;
        public string? IP { get; init; } = null;
        public uint? Port { get; init; } = null;
        public string? Target { get; init; } = "RATE-LIMIT";

        public override string ToString()
        {
                var sb = new StringBuilder();
                sb.Append($"-p {Protocol} ");
                if (IP != null) sb.Append($"-s {IP} ");
                if (Port != null) sb.Append($"--dport {Port} ");
                sb.Append("-j ");
                sb.Append(Target);
                return sb.ToString();
        }
    }
}