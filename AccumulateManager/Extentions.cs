using System.CodeDom.Compiler;
using System.Collections;
using System.Net;
using System.Reflection;
using System.Text.RegularExpressions;
using Google.Protobuf.Collections;
using Serilog;
using Spectre.Console;
using DateTime = System.DateTime;

namespace AccumulateManager;

static public class Extentions
{
    static public async void FireAndForget(this Task task)
    {
        try
        {
            await task;
        }
        catch (Exception e)
        {
            Log.Error($"FireAndForget: {e.Message}");
        }
    }
    
    public static String WildCardToRegEx(this String value) {
        return $"^{Regex.Escape(value).Replace("\\?", ".").Replace("\\*", ".*")}$"; 
    }
    
    public static async Task<string?> GetPublicIpAddress()
    {
        try
        {
            using (var client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromSeconds(4);
                var result = await client.GetStringAsync("https://ipv4.seeip.org");
                return result.Trim();
            }
        }
        catch (Exception e)
        {
            Log.Error($"GetPublicIpAddress: {e.Message}");
        }

        return null;
    }

    public static async Task<string?> GetPublicIp6Address()
    {
        try
        {
            using (var client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromSeconds(4);
                var result = await client.GetStringAsync("https://ipv6.seeip.org");
                if (!String.IsNullOrEmpty(result) && result.Contains(":"))
                {
                    return result.Trim();
                }
                return null;
            }
        }
        catch (Exception e)
        {
            Log.Error($"GetPublicIpAddress: {e.Message}");
        }

        return null;
    }


    static public string StripBraketedPaste(this string text)
    {
        if (text.StartsWith('-') && text.EndsWith('~'))
        {
            var pt1 = text.IndexOf('~');
            var pt2 = text.LastIndexOf('-');
            if (pt1 < pt2)
            {
                return text.Substring(pt1 + 1,pt2-pt1-1);
            }
        }

        return text;
    }

    static public string BytesToHumanString(this long bytes)
    {
        string[] sizes = { "B", "KB", "MB", "GB", "TB" };
        
        int order = 0;
        while (bytes >= 1024 && order < sizes.Length - 1) {
            order++;
            bytes = bytes/1024;
        }
        return String.Format($"{bytes:0.##} {sizes[order]}");
    }
}