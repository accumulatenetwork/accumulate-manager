using System.Net.Sockets;

namespace AccumulateManagerCommon;

static public class Extentions
{

/// <summary>
/// Gets a random 64-bit number
/// https://stackoverflow.com/a/13095144/115425
/// </summary>
/// <param name="min"></param>
/// <param name="max"></param>
/// <returns></returns>
    static public UInt64 GetRandomID(UInt64 min = 0, UInt64 max = UInt64.MaxValue)
    {
        UInt64 uRange = max - min;
        var random = new Random();
        UInt64 ulongRand;
        do
        {
            byte[] buf = new byte[8];
            random.NextBytes(buf);
            ulongRand = (UInt64)BitConverter.ToInt64(buf, 0);
        } while (ulongRand > UInt64.MaxValue - ((UInt64.MaxValue % uRange) + 1) % uRange);

        return (ulongRand % uRange) + min;
    }
    
    
}