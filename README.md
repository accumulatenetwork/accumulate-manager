![](AccumulateManager.png)



# Accumulate Manager (AccMan backend)

 - Manage Accumulate Node
 - Reverse Proxy
 - Accumulate Bridge
 - LetsEncrypt SSL certificate
 - Firewall & Rate Limiter
 - Prometheus Metrics

## Install

Please use AccMan frontend https://gitlab.com/accumulatenetwork/accman